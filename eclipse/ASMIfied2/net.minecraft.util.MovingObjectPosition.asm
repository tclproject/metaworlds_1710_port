package asm.net.minecraft.util;
import java.util.*;
import org.objectweb.asm.*;
public class MovingObjectPositionDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/util/MovingObjectPosition", null, "java/lang/Object", null);

cw.visitInnerClass("net/minecraft/util/MovingObjectPosition$MovingObjectType", "net/minecraft/util/MovingObjectPosition", "MovingObjectType", ACC_PUBLIC + ACC_FINAL + ACC_STATIC + ACC_ENUM);

{
fv = cw.visitField(ACC_PUBLIC, "typeOfHit", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "blockX", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "blockY", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "blockZ", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "sideHit", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "hitVec", "Lnet/minecraft/util/Vec3;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "entityHit", "Lnet/minecraft/entity/Entity;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00000610");
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "subHit", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "hitInfo", "Ljava/lang/Object;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "worldObj", "Lnet/minecraft/world/World;", null, null);
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIIILnet/minecraft/util/Vec3;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ALOAD, 5);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/MovingObjectPosition", "<init>", "(IIIILnet/minecraft/util/Vec3;Z)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(7, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIIILnet/minecraft/util/Vec3;Lnet/minecraft/world/World;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ALOAD, 5);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/MovingObjectPosition", "<init>", "(IIIILnet/minecraft/util/Vec3;ZLnet/minecraft/world/World;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(8, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIIILnet/minecraft/util/Vec3;Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ALOAD, 5);
mv.visitVarInsn(ILOAD, 6);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/client/Minecraft", "getMinecraft", "()Lnet/minecraft/client/Minecraft;", false);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/Minecraft", "theWorld", "Lnet/minecraft/client/multiplayer/WorldClient;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/MovingObjectPosition", "<init>", "(IIIILnet/minecraft/util/Vec3;ZLnet/minecraft/world/World;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(8, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIIILnet/minecraft/util/Vec3;ZLnet/minecraft/world/World;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "subHit", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "hitInfo", "Ljava/lang/Object;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 6);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/util/MovingObjectPosition$MovingObjectType", "BLOCK", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_FULL, 8, new Object[] {"net/minecraft/util/MovingObjectPosition", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "net/minecraft/util/Vec3", Opcodes.INTEGER, "net/minecraft/world/World"}, 1, new Object[] {"net/minecraft/util/MovingObjectPosition"});
mv.visitFieldInsn(GETSTATIC, "net/minecraft/util/MovingObjectPosition$MovingObjectType", "MISS", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 8, new Object[] {"net/minecraft/util/MovingObjectPosition", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "net/minecraft/util/Vec3", Opcodes.INTEGER, "net/minecraft/world/World"}, 2, new Object[] {"net/minecraft/util/MovingObjectPosition", "net/minecraft/util/MovingObjectPosition$MovingObjectType"});
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "typeOfHit", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "blockX", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "blockY", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "blockZ", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 4);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "sideHit", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "xCoord", "D");
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "yCoord", "D");
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "zCoord", "D");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/Vec3", "createVectorHelper", "(DDD)Lnet/minecraft/util/Vec3;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "hitVec", "Lnet/minecraft/util/Vec3;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 7);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "worldObj", "Lnet/minecraft/world/World;");
mv.visitInsn(RETURN);
mv.visitMaxs(7, 8);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/util/MovingObjectPosition;Lnet/minecraft/world/World;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "subHit", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "hitInfo", "Ljava/lang/Object;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "typeOfHit", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "typeOfHit", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "blockX", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "blockX", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "blockY", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "blockY", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "blockZ", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "blockZ", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "sideHit", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "sideHit", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "hitVec", "Lnet/minecraft/util/Vec3;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "hitVec", "Lnet/minecraft/util/Vec3;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "worldObj", "Lnet/minecraft/world/World;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/entity/Entity;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posX", "D");
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posY", "D");
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posZ", "D");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/Vec3", "createVectorHelper", "(DDD)Lnet/minecraft/util/Vec3;", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/MovingObjectPosition", "<init>", "(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/Vec3;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(8, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/Vec3;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "subHit", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "hitInfo", "Ljava/lang/Object;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/util/MovingObjectPosition$MovingObjectType", "ENTITY", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "typeOfHit", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "entityHit", "Lnet/minecraft/entity/Entity;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "hitVec", "Lnet/minecraft/util/Vec3;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/util/MovingObjectPosition", "worldObj", "Lnet/minecraft/world/World;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "toString", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("HitResult{type=");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "typeOfHit", "Lnet/minecraft/util/MovingObjectPosition$MovingObjectType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/Object;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", x=");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "blockX", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", y=");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "blockY", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", z=");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "blockZ", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", f=");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "sideHit", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", pos=");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "hitVec", "Lnet/minecraft/util/Vec3;");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/Object;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", entity=");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/MovingObjectPosition", "entityHit", "Lnet/minecraft/entity/Entity;");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/Object;)Ljava/lang/StringBuilder;", false);
mv.visitIntInsn(BIPUSH, 125);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(C)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
