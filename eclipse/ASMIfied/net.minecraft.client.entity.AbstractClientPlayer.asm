package asm.net.minecraft.client.entity;
import java.util.*;
import org.objectweb.asm.*;
public class AbstractClientPlayerDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER + ACC_ABSTRACT, "net/minecraft/client/entity/AbstractClientPlayer", null, "net/tclproject/metaworlds/patcher/EntityPlayerIntermediateClass", new String[] { "net/minecraft/client/resources/SkinManager$SkinAvailableCallback" });

{
av0 = cw.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
cw.visitInnerClass("com/mojang/authlib/minecraft/MinecraftProfileTexture$Type", "com/mojang/authlib/minecraft/MinecraftProfileTexture", "Type", ACC_PUBLIC + ACC_FINAL + ACC_STATIC + ACC_ENUM);

cw.visitInnerClass("net/minecraft/client/entity/AbstractClientPlayer$SwitchType", "net/minecraft/client/entity/AbstractClientPlayer", "SwitchType", ACC_FINAL + ACC_STATIC);

cw.visitInnerClass("net/minecraft/client/resources/SkinManager$SkinAvailableCallback", "net/minecraft/client/resources/SkinManager", "SkinAvailableCallback", ACC_PUBLIC + ACC_STATIC + ACC_ABSTRACT + ACC_INTERFACE);

{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "locationStevePng", "Lnet/minecraft/util/ResourceLocation;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "locationSkin", "Lnet/minecraft/util/ResourceLocation;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "locationCape", "Lnet/minecraft/util/ResourceLocation;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00000935");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/util/ResourceLocation");
mv.visitInsn(DUP);
mv.visitLdcInsn("textures/entity/steve.png");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ResourceLocation", "<init>", "(Ljava/lang/String;)V", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/client/entity/AbstractClientPlayer", "locationStevePng", "Lnet/minecraft/util/ResourceLocation;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/world/World;Lcom/mojang/authlib/GameProfile;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/EntityPlayerIntermediateClass", "<init>", "(Lnet/minecraft/world/World;Lcom/mojang/authlib/GameProfile;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/entity/AbstractClientPlayer", "getCommandSenderName", "()Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "isEmpty", "()Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFNE, l0);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/client/Minecraft", "getMinecraft", "()Lnet/minecraft/client/Minecraft;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/Minecraft", "func_152342_ad", "()Lnet/minecraft/client/resources/SkinManager;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/resources/SkinManager", "func_152790_a", "(Lcom/mojang/authlib/GameProfile;Lnet/minecraft/client/resources/SkinManager$SkinAvailableCallback;Z)V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_FULL, 4, new Object[] {"net/minecraft/client/entity/AbstractClientPlayer", "net/minecraft/world/World", "com/mojang/authlib/GameProfile", "java/lang/String"}, 0, new Object[] {});
mv.visitInsn(RETURN);
mv.visitMaxs(4, 5);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152122_n", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/entity/AbstractClientPlayer", "locationCape", "Lnet/minecraft/util/ResourceLocation;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152123_o", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/entity/AbstractClientPlayer", "locationSkin", "Lnet/minecraft/util/ResourceLocation;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getLocationSkin", "()Lnet/minecraft/util/ResourceLocation;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/entity/AbstractClientPlayer", "locationSkin", "Lnet/minecraft/util/ResourceLocation;");
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/client/entity/AbstractClientPlayer", "locationStevePng", "Lnet/minecraft/util/ResourceLocation;");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/entity/AbstractClientPlayer", "locationSkin", "Lnet/minecraft/util/ResourceLocation;");
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/util/ResourceLocation"});
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getLocationCape", "()Lnet/minecraft/util/ResourceLocation;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/entity/AbstractClientPlayer", "locationCape", "Lnet/minecraft/util/ResourceLocation;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "getDownloadImageSkin", "(Lnet/minecraft/util/ResourceLocation;Ljava/lang/String;)Lnet/minecraft/client/renderer/ThreadDownloadImageData;", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/client/Minecraft", "getMinecraft", "()Lnet/minecraft/client/Minecraft;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/Minecraft", "getTextureManager", "()Lnet/minecraft/client/renderer/texture/TextureManager;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/renderer/texture/TextureManager", "getTexture", "(Lnet/minecraft/util/ResourceLocation;)Lnet/minecraft/client/renderer/texture/ITextureObject;", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 3);
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitTypeInsn(NEW, "net/minecraft/client/renderer/ThreadDownloadImageData");
mv.visitInsn(DUP);
mv.visitInsn(ACONST_NULL);
mv.visitLdcInsn("http://skins.minecraft.net/MinecraftSkins/%s.png");
mv.visitInsn(ICONST_1);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/StringUtils", "stripControlCodes", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "format", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", false);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/client/entity/AbstractClientPlayer", "locationStevePng", "Lnet/minecraft/util/ResourceLocation;");
mv.visitTypeInsn(NEW, "net/minecraft/client/renderer/ImageBufferDownload");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/client/renderer/ImageBufferDownload", "<init>", "()V", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/client/renderer/ThreadDownloadImageData", "<init>", "(Ljava/io/File;Ljava/lang/String;Lnet/minecraft/util/ResourceLocation;Lnet/minecraft/client/renderer/IImageBuffer;)V", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 3);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/client/renderer/texture/ITextureObject");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/renderer/texture/TextureManager", "loadTexture", "(Lnet/minecraft/util/ResourceLocation;Lnet/minecraft/client/renderer/texture/ITextureObject;)Z", false);
mv.visitInsn(POP);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/client/renderer/texture/TextureManager", "java/lang/Object"}, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/client/renderer/ThreadDownloadImageData");
mv.visitInsn(ARETURN);
mv.visitMaxs(8, 4);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "getLocationSkin", "(Ljava/lang/String;)Lnet/minecraft/util/ResourceLocation;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/util/ResourceLocation");
mv.visitInsn(DUP);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("skins/");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/StringUtils", "stripControlCodes", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ResourceLocation", "<init>", "(Ljava/lang/String;)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(5, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152121_a", "(Lcom/mojang/authlib/minecraft/MinecraftProfileTexture$Type;Lnet/minecraft/util/ResourceLocation;)V", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/client/entity/AbstractClientPlayer$SwitchType", "field_152630_a", "[I");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "com/mojang/authlib/minecraft/MinecraftProfileTexture$Type", "ordinal", "()I", false);
mv.visitInsn(IALOAD);
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTableSwitchInsn(1, 2, l2, new Label[] { l0, l1 });
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/entity/AbstractClientPlayer", "locationSkin", "Lnet/minecraft/util/ResourceLocation;");
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/entity/AbstractClientPlayer", "locationCape", "Lnet/minecraft/util/ResourceLocation;");
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 3);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
