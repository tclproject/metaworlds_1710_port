package asm.net.minecraft.server;
import java.util.*;
import org.objectweb.asm.*;
public class MinecraftServerDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER + ACC_ABSTRACT, "net/minecraft/server/MinecraftServer", null, "java/lang/Object", new String[] { "net/minecraft/command/ICommandSender", "java/lang/Runnable", "net/minecraft/profiler/IPlayerUsage" });

cw.visitInnerClass("cpw/mods/fml/common/StartupQuery$AbortedException", "cpw/mods/fml/common/StartupQuery", "AbortedException", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier", "net/minecraft/network/ServerStatusResponse", "MinecraftProtocolVersionIdentifier", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/network/ServerStatusResponse$PlayerCountData", "net/minecraft/network/ServerStatusResponse", "PlayerCountData", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$1", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$2", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$3", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$4", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$5", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$6", null, null, 0);

cw.visitInnerClass("net/minecraft/world/WorldSettings$GameType", "net/minecraft/world/WorldSettings", "GameType", ACC_PUBLIC + ACC_FINAL + ACC_STATIC + ACC_ENUM);

cw.visitInnerClass("net/minecraftforge/event/world/WorldEvent$Load", "net/minecraftforge/event/world/WorldEvent", "Load", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraftforge/event/world/WorldEvent$Unload", "net/minecraftforge/event/world/WorldEvent", "Unload", ACC_PUBLIC + ACC_STATIC);

{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "logger", "Lorg/apache/logging/log4j/Logger;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "field_152367_a", "Ljava/io/File;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_STATIC, "mcServer", "Lnet/minecraft/server/MinecraftServer;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "anvilConverterForAnvilFile", "Lnet/minecraft/world/storage/ISaveFormat;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "anvilFile", "Ljava/io/File;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "tickables", "Ljava/util/List;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "commandManager", "Lnet/minecraft/command/ICommandManager;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL, "theProfiler", "Lnet/minecraft/profiler/Profiler;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147144_o", "Lnet/minecraft/network/NetworkSystem;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147146_q", "Ljava/util/Random;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "hostname", "Ljava/lang/String;", null, null);
{
av0 = fv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "serverPort", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "worldServers", "[Lnet/minecraft/world/WorldServer;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "serverRunning", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "serverStopped", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "tickCounter", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED + ACC_FINAL, "serverProxy", "Ljava/net/Proxy;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "currentTask", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "percentDone", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "onlineMode", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "canSpawnAnimals", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "canSpawnNPCs", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "pvpEnabled", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "allowFlight", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "motd", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "buildLimit", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_143008_E", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL, "tickTimeArray", "[J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "worldTickTimes", "Ljava/util/Hashtable;", "Ljava/util/Hashtable<Ljava/lang/Integer;[J>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "serverKeyPair", "Ljava/security/KeyPair;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "serverOwner", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "folderName", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "worldName", "Ljava/lang/String;", null, null);
{
av0 = fv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "isDemo", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "enableBonusChest", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "worldIsBeingDeleted", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_147141_M", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "serverIsRunning", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "timeOfLastWarning", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "userMessage", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "startProfiling", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "isGamemodeForced", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_147142_T", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001462");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "org/apache/logging/log4j/LogManager", "getLogger", "()Lorg/apache/logging/log4j/Logger;", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitLdcInsn("usercache.json");
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/lang/String;)V", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/server/MinecraftServer", "field_152367_a", "Ljava/io/File;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Ljava/io/File;Ljava/net/Proxy;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/PlayerUsageSnooper");
mv.visitInsn(DUP);
mv.visitLdcInsn("server");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/PlayerUsageSnooper", "<init>", "(Ljava/lang/String;Lnet/minecraft/profiler/IPlayerUsage;J)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/ArrayList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "tickables", "Ljava/util/List;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/Profiler");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/Profiler", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Random");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Random", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147146_q", "Ljava/util/Random;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverPort", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "net/minecraft/world/WorldServer");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverRunning", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitIntInsn(BIPUSH, 100);
mv.visitIntInsn(NEWARRAY, T_LONG);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "tickTimeArray", "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Hashtable");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Hashtable", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(LCONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/server/management/PlayerProfileCache");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_152367_a", "Ljava/io/File;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/management/PlayerProfileCache", "<init>", "(Lnet/minecraft/server/MinecraftServer;Ljava/io/File;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/server/MinecraftServer", "mcServer", "Lnet/minecraft/server/MinecraftServer;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverProxy", "Ljava/net/Proxy;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "anvilFile", "Ljava/io/File;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/network/NetworkSystem");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/NetworkSystem", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147144_o", "Lnet/minecraft/network/NetworkSystem;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/command/ServerCommandManager");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/command/ServerCommandManager", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "commandManager", "Lnet/minecraft/command/ICommandManager;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/chunk/storage/AnvilSaveConverter");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/chunk/storage/AnvilSaveConverter", "<init>", "(Ljava/io/File;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "anvilConverterForAnvilFile", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "java/util/UUID", "randomUUID", "()Ljava/util/UUID;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/UUID", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService", "<init>", "(Ljava/net/Proxy;Ljava/lang/String;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitMethodInsn(INVOKEVIRTUAL, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService", "createMinecraftSessionService", "()Lcom/mojang/authlib/minecraft/MinecraftSessionService;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitMethodInsn(INVOKEVIRTUAL, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService", "createProfileRepository", "()Lcom/mojang/authlib/GameProfileRepository;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitInsn(RETURN);
mv.visitMaxs(7, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/PlayerUsageSnooper");
mv.visitInsn(DUP);
mv.visitLdcInsn("server");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/PlayerUsageSnooper", "<init>", "(Ljava/lang/String;Lnet/minecraft/profiler/IPlayerUsage;J)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/ArrayList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "tickables", "Ljava/util/List;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/Profiler");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/Profiler", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Random");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Random", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147146_q", "Ljava/util/Random;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverPort", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "net/minecraft/world/WorldServer");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverRunning", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitIntInsn(BIPUSH, 100);
mv.visitIntInsn(NEWARRAY, T_LONG);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "tickTimeArray", "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Hashtable");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Hashtable", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(LCONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverProxy", "Ljava/net/Proxy;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "anvilFile", "Ljava/io/File;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147144_o", "Lnet/minecraft/network/NetworkSystem;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "commandManager", "Lnet/minecraft/command/ICommandManager;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "anvilConverterForAnvilFile", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitInsn(RETURN);
mv.visitMaxs(7, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED + ACC_ABSTRACT, "startServer", "()Z", null, new String[] { "java/io/IOException" });
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "convertMapIfNeeded", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getActiveAnvilConverter", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "isOldMapFormat", "(Ljava/lang/String;)Z", true);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Converting map!");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("menu.convertingLevel");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "setUserMessage", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getActiveAnvilConverter", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$1");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$1", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "convertMapFormat", "(Ljava/lang/String;Lnet/minecraft/util/IProgressUpdate;)Z", true);
mv.visitInsn(POP);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED + ACC_SYNCHRONIZED, "setUserMessage", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "userMessage", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_SYNCHRONIZED, "getUserMessage", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "userMessage", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "loadAllWorlds", "(Ljava/lang/String;Ljava/lang/String;JLnet/minecraft/world/WorldType;Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "convertMapIfNeeded", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("menu.loadingLevel");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "setUserMessage", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "anvilConverterForAnvilFile", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "getSaveLoader", "(Ljava/lang/String;Z)Lnet/minecraft/world/storage/ISaveHandler;", true);
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveHandler", "loadWorldInfo", "()Lnet/minecraft/world/storage/WorldInfo;", true);
mv.visitVarInsn(ASTORE, 8);
mv.visitVarInsn(ALOAD, 8);
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldSettings");
mv.visitInsn(DUP);
mv.visitVarInsn(LLOAD, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getGameType", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "canStructuresSpawn", "()Z", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isHardcore", "()Z", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldSettings", "<init>", "(JLnet/minecraft/world/WorldSettings$GameType;ZZLnet/minecraft/world/WorldType;)V", false);
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_82750_a", "(Ljava/lang/String;)Lnet/minecraft/world/WorldSettings;", false);
mv.visitInsn(POP);
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/world/storage/ISaveHandler", "net/minecraft/world/storage/WorldInfo"}, 0, null);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldSettings");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldSettings", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitVarInsn(ASTORE, 9);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldSettings"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "enableBonusChest", "Z");
Label l2 = new Label();
mv.visitJumpInsn(IFEQ, l2);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "enableBonusChest", "()Lnet/minecraft/world/WorldSettings;", false);
mv.visitInsn(POP);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isDemo", "()Z", false);
Label l3 = new Label();
mv.visitJumpInsn(IFEQ, l3);
mv.visitTypeInsn(NEW, "net/minecraft/world/demo/DemoWorldServer");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 7);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/demo/DemoWorldServer", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/storage/ISaveHandler;Ljava/lang/String;ILnet/minecraft/profiler/Profiler;)V", false);
Label l4 = new Label();
mv.visitJumpInsn(GOTO, l4);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldServer");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 7);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldServer", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/storage/ISaveHandler;Ljava/lang/String;ILnet/minecraft/world/WorldSettings;Lnet/minecraft/profiler/Profiler;)V", false);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/WorldServer"});
mv.visitVarInsn(ASTORE, 10);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getStaticDimensionIDs", "()[Ljava/lang/Integer;", false);
mv.visitInsn(DUP);
mv.visitVarInsn(ASTORE, 14);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 13);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 12);
Label l5 = new Label();
mv.visitJumpInsn(GOTO, l5);
Label l6 = new Label();
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_FULL, 14, new Object[] {"net/minecraft/server/MinecraftServer", "java/lang/String", "java/lang/String", Opcodes.LONG, "net/minecraft/world/WorldType", "java/lang/String", "net/minecraft/world/storage/ISaveHandler", "net/minecraft/world/storage/WorldInfo", "net/minecraft/world/WorldSettings", "net/minecraft/world/WorldServer", Opcodes.TOP, Opcodes.INTEGER, Opcodes.INTEGER, "[Ljava/lang/Integer;"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 14);
mv.visitVarInsn(ILOAD, 12);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ILOAD, 11);
Label l7 = new Label();
mv.visitJumpInsn(IFNE, l7);
mv.visitVarInsn(ALOAD, 10);
Label l8 = new Label();
mv.visitJumpInsn(GOTO, l8);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_FULL, 14, new Object[] {"net/minecraft/server/MinecraftServer", "java/lang/String", "java/lang/String", Opcodes.LONG, "net/minecraft/world/WorldType", "java/lang/String", "net/minecraft/world/storage/ISaveHandler", "net/minecraft/world/storage/WorldInfo", "net/minecraft/world/WorldSettings", "net/minecraft/world/WorldServer", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "[Ljava/lang/Integer;"}, 0, new Object[] {});
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldServerMulti");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 7);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 11);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldServerMulti", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/storage/ISaveHandler;Ljava/lang/String;ILnet/minecraft/world/WorldSettings;Lnet/minecraft/world/WorldServer;Lnet/minecraft/profiler/Profiler;)V", false);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/WorldServer"});
mv.visitVarInsn(ASTORE, 15);
mv.visitVarInsn(ALOAD, 15);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldManager");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 15);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldManager", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/WorldServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "addWorldAccess", "(Lnet/minecraft/world/IWorldAccess;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isSinglePlayer", "()Z", false);
Label l9 = new Label();
mv.visitJumpInsn(IFNE, l9);
mv.visitVarInsn(ALOAD, 15);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorldInfo", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getGameType", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "setGameType", "(Lnet/minecraft/world/WorldSettings$GameType;)V", false);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitFieldInsn(GETSTATIC, "net/minecraftforge/common/MinecraftForge", "EVENT_BUS", "Lcpw/mods/fml/common/eventhandler/EventBus;");
mv.visitTypeInsn(NEW, "net/minecraftforge/event/world/WorldEvent$Load");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 15);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraftforge/event/world/WorldEvent$Load", "<init>", "(Lnet/minecraft/world/World;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/eventhandler/EventBus", "post", "(Lcpw/mods/fml/common/eventhandler/Event;)Z", false);
mv.visitInsn(POP);
mv.visitIincInsn(12, 1);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_FULL, 14, new Object[] {"net/minecraft/server/MinecraftServer", "java/lang/String", "java/lang/String", Opcodes.LONG, "net/minecraft/world/WorldType", "java/lang/String", "net/minecraft/world/storage/ISaveHandler", "net/minecraft/world/storage/WorldInfo", "net/minecraft/world/WorldSettings", "net/minecraft/world/WorldServer", Opcodes.TOP, Opcodes.INTEGER, Opcodes.INTEGER, "[Ljava/lang/Integer;"}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 12);
mv.visitVarInsn(ILOAD, 13);
mv.visitJumpInsn(IF_ICMPLT, l6);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(ICONST_1);
mv.visitTypeInsn(ANEWARRAY, "net/minecraft/world/WorldServer");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 10);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "setPlayerManager", "([Lnet/minecraft/world/WorldServer;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147135_j", "()Lnet/minecraft/world/EnumDifficulty;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147139_a", "(Lnet/minecraft/world/EnumDifficulty;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "initialWorldChunkLoad", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(9, 16);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "initialWorldChunkLoad", "()V", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 1);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 2);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 3);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 4);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("menu.generatingTerrain");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "setUserMessage", "(Ljava/lang/String;)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 6);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("Preparing start region for level ");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 6);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 6);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getSpawnPoint", "()Lnet/minecraft/util/ChunkCoordinates;", false);
mv.visitVarInsn(ASTORE, 8);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitVarInsn(LSTORE, 9);
mv.visitIntInsn(SIPUSH, -192);
mv.visitVarInsn(ISTORE, 11);
Label l0 = new Label();
mv.visitJumpInsn(GOTO, l0);
Label l1 = new Label();
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 11, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "net/minecraft/world/WorldServer", "net/minecraft/util/ChunkCoordinates", Opcodes.LONG, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitIntInsn(SIPUSH, -192);
mv.visitVarInsn(ISTORE, 12);
Label l2 = new Label();
mv.visitJumpInsn(GOTO, l2);
Label l3 = new Label();
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitVarInsn(LSTORE, 13);
mv.visitVarInsn(LLOAD, 13);
mv.visitVarInsn(LLOAD, 9);
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(1000L));
mv.visitInsn(LCMP);
Label l4 = new Label();
mv.visitJumpInsn(IFLE, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("Preparing spawn area");
mv.visitVarInsn(ILOAD, 5);
mv.visitIntInsn(BIPUSH, 100);
mv.visitInsn(IMUL);
mv.visitIntInsn(SIPUSH, 625);
mv.visitInsn(IDIV);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "outputPercentRemaining", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(LLOAD, 13);
mv.visitVarInsn(LSTORE, 9);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.LONG}, 0, null);
mv.visitIincInsn(5, 1);
mv.visitVarInsn(ALOAD, 7);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "theChunkProviderServer", "Lnet/minecraft/world/gen/ChunkProviderServer;");
mv.visitVarInsn(ALOAD, 8);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/ChunkCoordinates", "posX", "I");
mv.visitVarInsn(ILOAD, 11);
mv.visitInsn(IADD);
mv.visitInsn(ICONST_4);
mv.visitInsn(ISHR);
mv.visitVarInsn(ALOAD, 8);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/ChunkCoordinates", "posZ", "I");
mv.visitVarInsn(ILOAD, 12);
mv.visitInsn(IADD);
mv.visitInsn(ICONST_4);
mv.visitInsn(ISHR);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/gen/ChunkProviderServer", "loadChunk", "(II)Lnet/minecraft/world/chunk/Chunk;", false);
mv.visitInsn(POP);
mv.visitIincInsn(12, 16);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ILOAD, 12);
mv.visitIntInsn(SIPUSH, 192);
Label l5 = new Label();
mv.visitJumpInsn(IF_ICMPGT, l5);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isServerRunning", "()Z", false);
mv.visitJumpInsn(IFNE, l3);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitIincInsn(11, 16);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 11);
mv.visitIntInsn(SIPUSH, 192);
Label l6 = new Label();
mv.visitJumpInsn(IF_ICMPGT, l6);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isServerRunning", "()Z", false);
mv.visitJumpInsn(IFNE, l1);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "clearCurrentTask", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 15);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "canStructuresSpawn", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "getGameType", "()Lnet/minecraft/world/WorldSettings$GameType;", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_147135_j", "()Lnet/minecraft/world/EnumDifficulty;", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "isHardcore", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "getOpPermissionLevel", "()I", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_152363_m", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "outputPercentRemaining", "(Ljava/lang/String;I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "currentTask", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "percentDone", "I");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "valueOf", "(Ljava/lang/Object;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitLdcInsn(": ");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("%");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "clearCurrentTask", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "currentTask", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "percentDone", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "saveAllWorlds", "(Z)V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "net/minecraft/world/MinecraftException");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldIsBeingDeleted", "Z");
Label l3 = new Label();
mv.visitJumpInsn(IFNE, l3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l4 = new Label();
mv.visitJumpInsn(IFNONNULL, l4);
mv.visitInsn(RETURN);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"[Lnet/minecraft/world/WorldServer;"}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 3);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 4);
Label l5 = new Label();
mv.visitJumpInsn(GOTO, l5);
Label l6 = new Label();
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 4);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 5);
Label l7 = new Label();
mv.visitJumpInsn(IFNULL, l7);
mv.visitVarInsn(ILOAD, 1);
mv.visitJumpInsn(IFNE, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("Saving chunks for level '");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorldInfo", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "getWorldName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("'/");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "provider", "Lnet/minecraft/world/WorldProvider;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldProvider", "getDimensionName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitVarInsn(ALOAD, 5);
mv.visitInsn(ICONST_1);
mv.visitInsn(ACONST_NULL);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "saveAllChunks", "(ZLnet/minecraft/util/IProgressUpdate;)V", false);
mv.visitLabel(l1);
mv.visitJumpInsn(GOTO, l7);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/MinecraftException"});
mv.visitVarInsn(ASTORE, 6);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/MinecraftException", "getMessage", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;)V", true);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitIincInsn(4, 1);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ILOAD, 3);
mv.visitJumpInsn(IF_ICMPLT, l6);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "stopServer", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldIsBeingDeleted", "Z");
Label l0 = new Label();
mv.visitJumpInsn(IFNE, l0);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/Loader", "instance", "()Lcpw/mods/fml/common/Loader;", false);
mv.visitFieldInsn(GETSTATIC, "cpw/mods/fml/common/LoaderState", "SERVER_STARTED", "Lcpw/mods/fml/common/LoaderState;");
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/Loader", "hasReachedState", "(Lcpw/mods/fml/common/LoaderState;)Z", false);
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitJumpInsn(IFNE, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Stopping server");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", false);
Label l1 = new Label();
mv.visitJumpInsn(IFNULL, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/NetworkSystem", "terminateEndpoints", "()V", false);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
Label l2 = new Label();
mv.visitJumpInsn(IFNULL, l2);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Saving players");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "saveAllPlayerData", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "removeAllPlayers", "()V", false);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
Label l3 = new Label();
mv.visitJumpInsn(IFNULL, l3);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Saving worlds");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "saveAllWorlds", "(Z)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
Label l4 = new Label();
mv.visitJumpInsn(GOTO, l4);
Label l5 = new Label();
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 1);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 2);
mv.visitFieldInsn(GETSTATIC, "net/minecraftforge/common/MinecraftForge", "EVENT_BUS", "Lcpw/mods/fml/common/eventhandler/EventBus;");
mv.visitTypeInsn(NEW, "net/minecraftforge/event/world/WorldEvent$Unload");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraftforge/event/world/WorldEvent$Unload", "<init>", "(Lnet/minecraft/world/World;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/eventhandler/EventBus", "post", "(Lcpw/mods/fml/common/eventhandler/Event;)Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "flush", "()V", false);
mv.visitIincInsn(1, 1);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ASTORE, 1);
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(DUP);
mv.visitVarInsn(ASTORE, 5);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 4);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l6 = new Label();
mv.visitJumpInsn(GOTO, l6);
Label l7 = new Label();
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_FULL, 6, new Object[] {"net/minecraft/server/MinecraftServer", "[Lnet/minecraft/world/WorldServer;", Opcodes.TOP, Opcodes.INTEGER, Opcodes.INTEGER, "[Lnet/minecraft/world/WorldServer;"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 5);
mv.visitVarInsn(ILOAD, 3);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "provider", "Lnet/minecraft/world/WorldProvider;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldProvider", "dimensionId", "I");
mv.visitInsn(ACONST_NULL);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "setWorld", "(ILnet/minecraft/world/WorldServer;)V", false);
mv.visitIincInsn(3, 1);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ILOAD, 4);
mv.visitJumpInsn(IF_ICMPLT, l7);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"net/minecraft/server/MinecraftServer"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "isSnooperRunning", "()Z", false);
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "stopSnooper", "()V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isServerRunning", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverRunning", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "initiateShutdown", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverRunning", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "run", "()V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "cpw/mods/fml/common/StartupQuery$AbortedException");
Label l3 = new Label();
Label l4 = new Label();
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l4, l5, "java/lang/Throwable");
Label l6 = new Label();
Label l7 = new Label();
mv.visitTryCatchBlock(l3, l6, l7, null);
Label l8 = new Label();
mv.visitTryCatchBlock(l0, l1, l8, "java/lang/Throwable");
Label l9 = new Label();
Label l10 = new Label();
Label l11 = new Label();
mv.visitTryCatchBlock(l9, l10, l11, "java/lang/Throwable");
Label l12 = new Label();
Label l13 = new Label();
mv.visitTryCatchBlock(l9, l12, l13, null);
Label l14 = new Label();
mv.visitTryCatchBlock(l0, l3, l14, null);
mv.visitTryCatchBlock(l8, l9, l14, null);
Label l15 = new Label();
Label l16 = new Label();
Label l17 = new Label();
mv.visitTryCatchBlock(l15, l16, l17, "java/lang/Throwable");
Label l18 = new Label();
Label l19 = new Label();
mv.visitTryCatchBlock(l15, l18, l19, null);
Label l20 = new Label();
Label l21 = new Label();
Label l22 = new Label();
mv.visitTryCatchBlock(l20, l21, l22, "java/lang/Throwable");
Label l23 = new Label();
Label l24 = new Label();
mv.visitTryCatchBlock(l20, l23, l24, null);
mv.visitLabel(l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "startServer", "()Z", false);
Label l25 = new Label();
mv.visitJumpInsn(IFEQ, l25);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStarted", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitVarInsn(LSTORE, 1);
mv.visitInsn(LCONST_0);
mv.visitVarInsn(LSTORE, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitTypeInsn(NEW, "net/minecraft/util/ChatComponentText");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "motd", "Ljava/lang/String;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ChatComponentText", "<init>", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151315_a", "(Lnet/minecraft/util/IChatComponent;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier");
mv.visitInsn(DUP);
mv.visitLdcInsn("1.7.10");
mv.visitInsn(ICONST_5);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier", "<init>", "(Ljava/lang/String;I)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151321_a", "(Lnet/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer", "func_147138_a", "(Lnet/minecraft/network/ServerStatusResponse;)V", false);
Label l26 = new Label();
mv.visitJumpInsn(GOTO, l26);
Label l27 = new Label();
mv.visitLabel(l27);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.LONG, Opcodes.LONG}, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitVarInsn(LSTORE, 5);
mv.visitVarInsn(LLOAD, 5);
mv.visitVarInsn(LLOAD, 1);
mv.visitInsn(LSUB);
mv.visitVarInsn(LSTORE, 7);
mv.visitVarInsn(LLOAD, 7);
mv.visitLdcInsn(new Long(2000L));
mv.visitInsn(LCMP);
Label l28 = new Label();
mv.visitJumpInsn(IFLE, l28);
mv.visitVarInsn(LLOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "timeOfLastWarning", "J");
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(15000L));
mv.visitInsn(LCMP);
mv.visitJumpInsn(IFLT, l28);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Can't keep up! Did the system time change, or is the server overloaded? Running {}ms behind, skipping {} tick(s)");
mv.visitInsn(ICONST_2);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(LLOAD, 7);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;", false);
mv.visitInsn(AASTORE);
mv.visitInsn(DUP);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(LLOAD, 7);
mv.visitLdcInsn(new Long(50L));
mv.visitInsn(LDIV);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;", false);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;[Ljava/lang/Object;)V", true);
mv.visitLdcInsn(new Long(2000L));
mv.visitVarInsn(LSTORE, 7);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "timeOfLastWarning", "J");
mv.visitLabel(l28);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.LONG, Opcodes.LONG}, 0, null);
mv.visitVarInsn(LLOAD, 7);
mv.visitInsn(LCONST_0);
mv.visitInsn(LCMP);
Label l29 = new Label();
mv.visitJumpInsn(IFGE, l29);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Time ran backwards! Did the system time change?");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;)V", true);
mv.visitInsn(LCONST_0);
mv.visitVarInsn(LSTORE, 7);
mv.visitLabel(l29);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(LLOAD, 3);
mv.visitVarInsn(LLOAD, 7);
mv.visitInsn(LADD);
mv.visitVarInsn(LSTORE, 3);
mv.visitVarInsn(LLOAD, 5);
mv.visitVarInsn(LSTORE, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "areAllPlayersAsleep", "()Z", false);
Label l30 = new Label();
mv.visitJumpInsn(IFEQ, l30);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "tick", "()V", false);
mv.visitInsn(LCONST_0);
mv.visitVarInsn(LSTORE, 3);
Label l31 = new Label();
mv.visitJumpInsn(GOTO, l31);
Label l32 = new Label();
mv.visitLabel(l32);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(LLOAD, 3);
mv.visitLdcInsn(new Long(50L));
mv.visitInsn(LSUB);
mv.visitVarInsn(LSTORE, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "tick", "()V", false);
mv.visitLabel(l30);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(LLOAD, 3);
mv.visitLdcInsn(new Long(50L));
mv.visitInsn(LCMP);
mv.visitJumpInsn(IFGT, l32);
mv.visitLabel(l31);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(LCONST_1);
mv.visitLdcInsn(new Long(50L));
mv.visitVarInsn(LLOAD, 3);
mv.visitInsn(LSUB);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "max", "(JJ)J", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "sleep", "(J)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverIsRunning", "Z");
mv.visitLabel(l26);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverRunning", "Z");
mv.visitJumpInsn(IFNE, l27);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopping", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitJumpInsn(GOTO, l20);
mv.visitLabel(l25);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "finalTick", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitLabel(l1);
mv.visitJumpInsn(GOTO, l20);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"cpw/mods/fml/common/StartupQuery$AbortedException"});
mv.visitVarInsn(ASTORE, 1);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitLabel(l3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "stopServer", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitLabel(l4);
Label l33 = new Label();
mv.visitJumpInsn(GOTO, l33);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 10);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l6);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
Label l34 = new Label();
mv.visitJumpInsn(GOTO, l34);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitVarInsn(ALOAD, 11);
mv.visitInsn(ATHROW);
mv.visitLabel(l33);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitJumpInsn(GOTO, l34);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 1);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Encountered an unexpected exception");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(INSTANCEOF, "net/minecraft/util/ReportedException");
Label l35 = new Label();
mv.visitJumpInsn(IFEQ, l35);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/ReportedException");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/ReportedException", "getCrashReport", "()Lnet/minecraft/crash/CrashReport;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "addServerInfoToCrashReport", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 2);
Label l36 = new Label();
mv.visitJumpInsn(GOTO, l36);
mv.visitLabel(l35);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"java/lang/Throwable", "net/minecraft/crash/CrashReport"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/crash/CrashReport");
mv.visitInsn(DUP);
mv.visitLdcInsn("Exception in server tick loop");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/crash/CrashReport", "<init>", "(Ljava/lang/String;Ljava/lang/Throwable;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "addServerInfoToCrashReport", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitLabel(l36);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getDataDirectory", "()Ljava/io/File;", false);
mv.visitLdcInsn("crash-reports");
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/io/File;Ljava/lang/String;)V", false);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("crash-");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitTypeInsn(NEW, "java/text/SimpleDateFormat");
mv.visitInsn(DUP);
mv.visitLdcInsn("yyyy-MM-dd_HH.mm.ss");
mv.visitMethodInsn(INVOKESPECIAL, "java/text/SimpleDateFormat", "<init>", "(Ljava/lang/String;)V", false);
mv.visitTypeInsn(NEW, "java/util/Date");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Date", "<init>", "()V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/text/SimpleDateFormat", "format", "(Ljava/util/Date;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("-server.txt");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/io/File;Ljava/lang/String;)V", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "saveToFile", "(Ljava/io/File;)Z", false);
Label l37 = new Label();
mv.visitJumpInsn(IFEQ, l37);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("This crash report has been saved to: ");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/File", "getAbsolutePath", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;)V", true);
Label l38 = new Label();
mv.visitJumpInsn(GOTO, l38);
mv.visitLabel(l37);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/io/File"}, 0, null);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("We were unable to save this crash report to disk.");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;)V", true);
mv.visitLabel(l38);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "finalTick", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitLabel(l9);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "stopServer", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitLabel(l10);
Label l39 = new Label();
mv.visitJumpInsn(GOTO, l39);
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"net/minecraft/server/MinecraftServer"}, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 10);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l12);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitJumpInsn(GOTO, l34);
mv.visitLabel(l13);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitVarInsn(ALOAD, 11);
mv.visitInsn(ATHROW);
mv.visitLabel(l39);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitJumpInsn(GOTO, l34);
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 9);
mv.visitLabel(l15);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "stopServer", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitLabel(l16);
Label l40 = new Label();
mv.visitJumpInsn(GOTO, l40);
mv.visitLabel(l17);
mv.visitFrame(Opcodes.F_FULL, 10, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, "java/lang/Throwable"}, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 10);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l18);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
Label l41 = new Label();
mv.visitJumpInsn(GOTO, l41);
mv.visitLabel(l19);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitVarInsn(ALOAD, 11);
mv.visitInsn(ATHROW);
mv.visitLabel(l40);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitLabel(l41);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitInsn(ATHROW);
mv.visitLabel(l20);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"net/minecraft/server/MinecraftServer"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "stopServer", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitLabel(l21);
Label l42 = new Label();
mv.visitJumpInsn(GOTO, l42);
mv.visitLabel(l22);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 10);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l23);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitJumpInsn(GOTO, l34);
mv.visitLabel(l24);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitVarInsn(ALOAD, 11);
mv.visitInsn(ATHROW);
mv.visitLabel(l42);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "systemExitNow", "()V", false);
mv.visitLabel(l34);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(9, 12);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PRIVATE, "func_147138_a", "(Lnet/minecraft/network/ServerStatusResponse;)V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
Label l3 = new Label();
Label l4 = new Label();
mv.visitTryCatchBlock(l0, l3, l4, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("server-icon.png");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getFile", "(Ljava/lang/String;)Ljava/io/File;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/File", "isFile", "()Z", false);
Label l5 = new Label();
mv.visitJumpInsn(IFEQ, l5);
mv.visitMethodInsn(INVOKESTATIC, "io/netty/buffer/Unpooled", "buffer", "()Lio/netty/buffer/ByteBuf;", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitLabel(l0);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "javax/imageio/ImageIO", "read", "(Ljava/io/File;)Ljava/awt/image/BufferedImage;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/awt/image/BufferedImage", "getWidth", "()I", false);
mv.visitIntInsn(BIPUSH, 64);
Label l6 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l6);
mv.visitInsn(ICONST_1);
Label l7 = new Label();
mv.visitJumpInsn(GOTO, l7);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {"java/io/File", "io/netty/buffer/ByteBuf", "java/awt/image/BufferedImage"}, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitLdcInsn("Must be 64 pixels wide");
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitMethodInsn(INVOKESTATIC, "org/apache/commons/lang3/Validate", "validState", "(ZLjava/lang/String;[Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/awt/image/BufferedImage", "getHeight", "()I", false);
mv.visitIntInsn(BIPUSH, 64);
Label l8 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l8);
mv.visitInsn(ICONST_1);
Label l9 = new Label();
mv.visitJumpInsn(GOTO, l9);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitLdcInsn("Must be 64 pixels high");
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitMethodInsn(INVOKESTATIC, "org/apache/commons/lang3/Validate", "validState", "(ZLjava/lang/String;[Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitLdcInsn("PNG");
mv.visitTypeInsn(NEW, "io/netty/buffer/ByteBufOutputStream");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESPECIAL, "io/netty/buffer/ByteBufOutputStream", "<init>", "(Lio/netty/buffer/ByteBuf;)V", false);
mv.visitMethodInsn(INVOKESTATIC, "javax/imageio/ImageIO", "write", "(Ljava/awt/image/RenderedImage;Ljava/lang/String;Ljava/io/OutputStream;)Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESTATIC, "io/netty/handler/codec/base64/Base64", "encode", "(Lio/netty/buffer/ByteBuf;)Lio/netty/buffer/ByteBuf;", false);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("data:image/png;base64,");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETSTATIC, "com/google/common/base/Charsets", "UTF_8", "Ljava/nio/charset/Charset;");
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "toString", "(Ljava/nio/charset/Charset;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151320_a", "(Ljava/lang/String;)V", false);
mv.visitLabel(l1);
Label l10 = new Label();
mv.visitJumpInsn(GOTO, l10);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_FULL, 4, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/network/ServerStatusResponse", "java/io/File", "io/netty/buffer/ByteBuf"}, 1, new Object[] {"java/lang/Exception"});
mv.visitVarInsn(ASTORE, 4);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Couldn't load server icon");
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l3);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "release", "()Z", false);
mv.visitInsn(POP);
mv.visitJumpInsn(GOTO, l5);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 6);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "release", "()Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 6);
mv.visitInsn(ATHROW);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "release", "()Z", false);
mv.visitInsn(POP);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "getDataDirectory", "()Ljava/io/File;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitLdcInsn(".");
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/lang/String;)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "finalTick", "(Lnet/minecraft/crash/CrashReport;)V", null, null);
mv.visitCode();
mv.visitInsn(RETURN);
mv.visitMaxs(0, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "systemExitNow", "()V", null, null);
mv.visitCode();
mv.visitInsn(RETURN);
mv.visitMaxs(0, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "tick", "()V", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LSTORE, 1);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPreServerTick", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(DUP);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "startProfiling", "Z");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "startProfiling", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/profiler/Profiler", "profilingEnabled", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "clearProfiling", "()V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.LONG}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("root");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "updateTimeLightAndEntities", "()V", false);
mv.visitVarInsn(LLOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(5000000000L));
mv.visitInsn(LCMP);
Label l1 = new Label();
mv.visitJumpInsn(IFLT, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse$PlayerCountData");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getMaxPlayers", "()I", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getCurrentPlayerCount", "()I", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse$PlayerCountData", "<init>", "(II)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151319_a", "(Lnet/minecraft/network/ServerStatusResponse$PlayerCountData;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getCurrentPlayerCount", "()I", false);
mv.visitIntInsn(BIPUSH, 12);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "min", "(II)I", false);
mv.visitTypeInsn(ANEWARRAY, "com/mojang/authlib/GameProfile");
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147146_q", "Ljava/util/Random;");
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getCurrentPlayerCount", "()I", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARRAYLENGTH);
mv.visitInsn(ISUB);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/MathHelper", "getRandomIntegerInRange", "(Ljava/util/Random;II)I", false);
mv.visitVarInsn(ISTORE, 4);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 5);
Label l2 = new Label();
mv.visitJumpInsn(GOTO, l2);
Label l3 = new Label();
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {"[Lcom/mojang/authlib/GameProfile;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ILOAD, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/management/ServerConfigurationManager", "playerEntityList", "Ljava/util/List;");
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ILOAD, 5);
mv.visitInsn(IADD);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/entity/player/EntityPlayerMP");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/player/EntityPlayerMP", "getGameProfile", "()Lcom/mojang/authlib/GameProfile;", false);
mv.visitInsn(AASTORE);
mv.visitIincInsn(5, 1);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 5);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l3);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESTATIC, "java/util/Arrays", "asList", "([Ljava/lang/Object;)Ljava/util/List;", false);
mv.visitMethodInsn(INVOKESTATIC, "java/util/Collections", "shuffle", "(Ljava/util/List;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151318_b", "()Lnet/minecraft/network/ServerStatusResponse$PlayerCountData;", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse$PlayerCountData", "func_151330_a", "([Lcom/mojang/authlib/GameProfile;)V", false);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitIntInsn(SIPUSH, 900);
mv.visitInsn(IREM);
Label l4 = new Label();
mv.visitJumpInsn(IFNE, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("save");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "saveAllPlayerData", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "saveAllWorlds", "(Z)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tallying");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickTimeArray", "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitIntInsn(BIPUSH, 100);
mv.visitInsn(IREM);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LLOAD, 1);
mv.visitInsn(LSUB);
mv.visitInsn(LASTORE);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("snooper");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "isSnooperRunning", "()Z", false);
Label l5 = new Label();
mv.visitJumpInsn(IFNE, l5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitIntInsn(BIPUSH, 100);
mv.visitJumpInsn(IF_ICMPLE, l5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "startSnooper", "()V", false);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitIntInsn(SIPUSH, 6000);
mv.visitInsn(IREM);
Label l6 = new Label();
mv.visitJumpInsn(IFNE, l6);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "addMemoryStatsToSnooper", "()V", false);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPostServerTick", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(6, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "updateTimeLightAndEntities", "()V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Throwable");
Label l3 = new Label();
Label l4 = new Label();
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l4, l5, "java/lang/Throwable");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("levels");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/chunkio/ChunkIOExecutor", "tick", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitIntInsn(SIPUSH, 200);
mv.visitInsn(IREM);
Label l6 = new Label();
mv.visitJumpInsn(IFNE, l6);
mv.visitInsn(ICONST_1);
Label l7 = new Label();
mv.visitJumpInsn(GOTO, l7);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getIDs", "(Z)[Ljava/lang/Integer;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l8 = new Label();
mv.visitJumpInsn(GOTO, l8);
Label l9 = new Label();
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_FULL, 4, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.TOP, "[Ljava/lang/Integer;", Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
mv.visitVarInsn(ISTORE, 4);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LSTORE, 5);
mv.visitVarInsn(ILOAD, 4);
Label l10 = new Label();
mv.visitJumpInsn(IFEQ, l10);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getAllowNether", "()Z", false);
Label l11 = new Label();
mv.visitJumpInsn(IFEQ, l11);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.LONG}, 0, null);
mv.visitVarInsn(ILOAD, 4);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorld", "(I)Lnet/minecraft/world/WorldServer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorlds", "()Ljava/util/Collection;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Collection", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 8);
Label l12 = new Label();
mv.visitJumpInsn(GOTO, l12);
Label l13 = new Label();
mv.visitLabel(l13);
mv.visitFrame(Opcodes.F_FULL, 8, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.TOP, "[Ljava/lang/Integer;", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.LONG, Opcodes.TOP, "java/util/Iterator"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/world/World");
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ALOAD, 7);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/world/WorldServer");
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorldInfo", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "getWorldName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("pools");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitIntInsn(BIPUSH, 20);
mv.visitInsn(IREM);
Label l14 = new Label();
mv.visitJumpInsn(IFNE, l14);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("timeSync");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitTypeInsn(NEW, "net/minecraft/network/play/server/S03PacketTimeUpdate");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getTotalWorldTime", "()J", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorldTime", "()J", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getGameRules", "()Lnet/minecraft/world/GameRules;", false);
mv.visitLdcInsn("doDaylightCycle");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/GameRules", "getGameRuleBooleanValue", "(Ljava/lang/String;)Z", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S03PacketTimeUpdate", "<init>", "(JJZ)V", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "provider", "Lnet/minecraft/world/WorldProvider;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldProvider", "dimensionId", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "sendPacketToAllPlayersInDimension", "(Lnet/minecraft/network/Packet;I)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_FULL, 9, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.TOP, "[Ljava/lang/Integer;", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.LONG, "net/minecraft/world/World", "java/util/Iterator", "net/minecraft/world/WorldServer"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tick");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPreWorldTick", "(Lnet/minecraft/world/World;)V", false);
mv.visitLabel(l0);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "tick", "()V", false);
mv.visitLabel(l1);
mv.visitJumpInsn(GOTO, l3);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitVarInsn(ALOAD, 11);
mv.visitLdcInsn("Exception ticking world");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/crash/CrashReport", "makeCrashReport", "(Ljava/lang/Throwable;Ljava/lang/String;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 10);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "addWorldInfoToCrashReport", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitInsn(POP);
mv.visitTypeInsn(NEW, "net/minecraft/util/ReportedException");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ReportedException", "<init>", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitInsn(ATHROW);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "updateEntities", "()V", false);
mv.visitLabel(l4);
Label l15 = new Label();
mv.visitJumpInsn(GOTO, l15);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitVarInsn(ALOAD, 11);
mv.visitLdcInsn("Exception ticking world entities");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/crash/CrashReport", "makeCrashReport", "(Ljava/lang/Throwable;Ljava/lang/String;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 10);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "addWorldInfoToCrashReport", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitInsn(POP);
mv.visitTypeInsn(NEW, "net/minecraft/util/ReportedException");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ReportedException", "<init>", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitInsn(ATHROW);
mv.visitLabel(l15);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPostWorldTick", "(Lnet/minecraft/world/World;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tracker");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getEntityTracker", "()Lnet/minecraft/entity/EntityTracker;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/EntityTracker", "updateTrackedEntities", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitLabel(l12);
mv.visitFrame(Opcodes.F_FULL, 8, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.TOP, "[Ljava/lang/Integer;", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.LONG, Opcodes.TOP, "java/util/Iterator"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
mv.visitJumpInsn(IFNE, l13);
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_FULL, 6, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.TOP, "[Ljava/lang/Integer;", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.LONG}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitVarInsn(ILOAD, 4);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", false);
mv.visitTypeInsn(CHECKCAST, "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitIntInsn(BIPUSH, 100);
mv.visitInsn(IREM);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LLOAD, 5);
mv.visitInsn(LSUB);
mv.visitInsn(LASTORE);
mv.visitIincInsn(3, 1);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l9);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("dim_unloading");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endStartSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "unloadWorlds", "(Ljava/util/Hashtable;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("connection");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endStartSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/NetworkSystem", "networkTick", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("players");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endStartSection", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "sendPlayerInfoToAllPlayers", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tickables");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endStartSection", "(Ljava/lang/String;)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
Label l16 = new Label();
mv.visitJumpInsn(GOTO, l16);
Label l17 = new Label();
mv.visitLabel(l17);
mv.visitFrame(Opcodes.F_FULL, 3, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.INTEGER, "[Ljava/lang/Integer;"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickables", "Ljava/util/List;");
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/server/gui/IUpdatePlayerListBox");
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/server/gui/IUpdatePlayerListBox", "update", "()V", true);
mv.visitIincInsn(1, 1);
mv.visitLabel(l16);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickables", "Ljava/util/List;");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
mv.visitJumpInsn(IF_ICMPLT, l17);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "theProfiler", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(9, 12);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getAllowNether", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "startServerThread", "()V", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/StartupQuery", "reset", "()V", false);
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$2");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("Server thread");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$2", "<init>", "(Lnet/minecraft/server/MinecraftServer;Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer$2", "start", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getFile", "(Ljava/lang/String;)Ljava/io/File;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getDataDirectory", "()Ljava/io/File;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/io/File;Ljava/lang/String;)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(4, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "logWarning", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "worldServerForDimension", "(I)Lnet/minecraft/world/WorldServer;", null, null);
mv.visitCode();
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorld", "(I)Lnet/minecraft/world/WorldServer;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "initDimension", "(I)V", false);
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorld", "(I)Lnet/minecraft/world/WorldServer;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getMinecraftVersion", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitLdcInsn("1.7.10");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getCurrentPlayerCount", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "getCurrentPlayerCount", "()I", false);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getMaxPlayers", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "getMaxPlayers", "()I", false);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getAllUsernames", "()[Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "getAllUsernames", "()[Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152357_F", "()[Lcom/mojang/authlib/GameProfile;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_152600_g", "()[Lcom/mojang/authlib/GameProfile;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getServerModName", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "getModName", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "addServerInfoToCrashReport", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReport;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "getCategory", "()Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitLdcInsn("Profiler Position");
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$3");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$3", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IFLE, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "getCategory", "()Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitLdcInsn("Vec3 Pool Size");
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$4");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$4", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
Label l1 = new Label();
mv.visitJumpInsn(IFNULL, l1);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "getCategory", "()Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitLdcInsn("Player Count");
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$5");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$5", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ARETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getPossibleCompletions", "(Lnet/minecraft/command/ICommandSender;Ljava/lang/String;)Ljava/util/List;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/util/ArrayList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitLdcInsn("/");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "startsWith", "(Ljava/lang/String;)Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "substring", "(I)Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitLdcInsn(" ");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "contains", "(Ljava/lang/CharSequence;)Z", false);
Label l1 = new Label();
mv.visitJumpInsn(IFEQ, l1);
mv.visitInsn(ICONST_0);
Label l2 = new Label();
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/util/ArrayList"}, 0, null);
mv.visitInsn(ICONST_1);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitVarInsn(ISTORE, 4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "commandManager", "Lnet/minecraft/command/ICommandManager;");
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/command/ICommandManager", "getPossibleCommands", "(Lnet/minecraft/command/ICommandSender;Ljava/lang/String;)Ljava/util/List;", true);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 5);
Label l3 = new Label();
mv.visitJumpInsn(IFNULL, l3);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 6);
Label l4 = new Label();
mv.visitJumpInsn(GOTO, l4);
Label l5 = new Label();
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {Opcodes.INTEGER, "java/util/List", "java/util/Iterator"}, 0, null);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "java/lang/String");
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ILOAD, 4);
Label l6 = new Label();
mv.visitJumpInsn(IFEQ, l6);
mv.visitVarInsn(ALOAD, 3);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("/");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/ArrayList", "add", "(Ljava/lang/Object;)Z", false);
mv.visitInsn(POP);
mv.visitJumpInsn(GOTO, l4);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/lang/String"}, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/ArrayList", "add", "(Ljava/lang/Object;)Z", false);
mv.visitInsn(POP);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
mv.visitJumpInsn(IFNE, l5);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitLdcInsn(" ");
mv.visitInsn(ICONST_M1);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "split", "(Ljava/lang/String;I)[Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitInsn(ARRAYLENGTH);
mv.visitInsn(ICONST_1);
mv.visitInsn(ISUB);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "getAllUsernames", "()[Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 6);
mv.visitVarInsn(ALOAD, 6);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 7);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 8);
Label l7 = new Label();
mv.visitJumpInsn(GOTO, l7);
Label l8 = new Label();
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_FULL, 9, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/command/ICommandSender", "java/lang/String", "java/util/ArrayList", "[Ljava/lang/String;", "java/lang/String", "[Ljava/lang/String;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 6);
mv.visitVarInsn(ILOAD, 8);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ALOAD, 5);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/command/CommandBase", "doesStringStartWith", "(Ljava/lang/String;Ljava/lang/String;)Z", false);
Label l9 = new Label();
mv.visitJumpInsn(IFEQ, l9);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/ArrayList", "add", "(Ljava/lang/Object;)Z", false);
mv.visitInsn(POP);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitIincInsn(8, 1);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 8);
mv.visitVarInsn(ILOAD, 7);
mv.visitJumpInsn(IF_ICMPLT, l8);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARETURN);
mv.visitMaxs(4, 10);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "getServer", "()Lnet/minecraft/server/MinecraftServer;", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "mcServer", "Lnet/minecraft/server/MinecraftServer;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getCommandSenderName", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitLdcInsn("Server");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "addChatMessage", "(Lnet/minecraft/util/IChatComponent;)V", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/util/IChatComponent", "getUnformattedText", "()Ljava/lang/String;", true);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "canCommandSenderUseCommand", "(ILjava/lang/String;)Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getCommandManager", "()Lnet/minecraft/command/ICommandManager;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "commandManager", "Lnet/minecraft/command/ICommandManager;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getKeyPair", "()Ljava/security/KeyPair;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverKeyPair", "Ljava/security/KeyPair;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getServerOwner", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverOwner", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setServerOwner", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverOwner", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isSinglePlayer", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverOwner", "Ljava/lang/String;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getFolderName", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "folderName", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setFolderName", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "folderName", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setWorldName", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldName", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getWorldName", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldName", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setKeyPair", "(Ljava/security/KeyPair;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverKeyPair", "Ljava/security/KeyPair;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147139_a", "(Lnet/minecraft/world/EnumDifficulty;)V", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 2);
Label l0 = new Label();
mv.visitJumpInsn(GOTO, l0);
Label l1 = new Label();
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 2);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 3);
Label l2 = new Label();
mv.visitJumpInsn(IFNULL, l2);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorldInfo", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "isHardcoreModeEnabled", "()Z", false);
Label l3 = new Label();
mv.visitJumpInsn(IFEQ, l3);
mv.visitVarInsn(ALOAD, 3);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/EnumDifficulty", "HARD", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/WorldServer", "difficultySetting", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ICONST_1);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "setAllowedSpawnTypes", "(ZZ)V", false);
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isSinglePlayer", "()Z", false);
Label l4 = new Label();
mv.visitJumpInsn(IFEQ, l4);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/WorldServer", "difficultySetting", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 3);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "difficultySetting", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/EnumDifficulty", "PEACEFUL", "Lnet/minecraft/world/EnumDifficulty;");
Label l5 = new Label();
mv.visitJumpInsn(IF_ACMPEQ, l5);
mv.visitInsn(ICONST_1);
Label l6 = new Label();
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/WorldServer"});
mv.visitInsn(ICONST_0);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_FULL, 4, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/world/EnumDifficulty", Opcodes.INTEGER, "net/minecraft/world/WorldServer"}, 2, new Object[] {"net/minecraft/world/WorldServer", Opcodes.INTEGER});
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "setAllowedSpawnTypes", "(ZZ)V", false);
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/WorldServer", "difficultySetting", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "allowSpawnMonsters", "()Z", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "canSpawnAnimals", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "setAllowedSpawnTypes", "(ZZ)V", false);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitIincInsn(2, 1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l1);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 4);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "allowSpawnMonsters", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isDemo", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "isDemo", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setDemo", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "isDemo", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "canCreateBonusChest", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "enableBonusChest", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getActiveAnvilConverter", "()Lnet/minecraft/world/storage/ISaveFormat;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "anvilConverterForAnvilFile", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "deleteWorldAndStopServer", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldIsBeingDeleted", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getActiveAnvilConverter", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "flushCache", "()V", true);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
Label l0 = new Label();
mv.visitJumpInsn(GOTO, l0);
Label l1 = new Label();
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 1);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l2 = new Label();
mv.visitJumpInsn(IFNULL, l2);
mv.visitFieldInsn(GETSTATIC, "net/minecraftforge/common/MinecraftForge", "EVENT_BUS", "Lcpw/mods/fml/common/eventhandler/EventBus;");
mv.visitTypeInsn(NEW, "net/minecraftforge/event/world/WorldEvent$Unload");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraftforge/event/world/WorldEvent$Unload", "<init>", "(Lnet/minecraft/world/World;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/eventhandler/EventBus", "post", "(Lcpw/mods/fml/common/eventhandler/Event;)Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "flush", "()V", false);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitIincInsn(1, 1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getActiveAnvilConverter", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getSaveHandler", "()Lnet/minecraft/world/storage/ISaveHandler;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveHandler", "getWorldDirectoryName", "()Ljava/lang/String;", true);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "deleteWorldDirectory", "(Ljava/lang/String;)Z", true);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "initiateShutdown", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getTexturePack", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "addServerStatsToSnooper", "(Lnet/minecraft/profiler/PlayerUsageSnooper;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("whitelist_enabled");
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("whitelist_count");
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("players_current");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getCurrentPlayerCount", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("players_max");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getMaxPlayers", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("players_seen");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "getAvailablePlayerDat", "()[Ljava/lang/String;", false);
mv.visitInsn(ARRAYLENGTH);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("uses_auth");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "onlineMode", "Z");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("gui_state");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getGuiEnabled", "()Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitLdcInsn("enabled");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 2, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String"});
mv.visitLdcInsn("disabled");
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 3, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String", "java/lang/String"});
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("run_time");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "getMinecraftStartTimeMillis", "()J", false);
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(60L));
mv.visitInsn(LDIV);
mv.visitLdcInsn(new Long(1000L));
mv.visitInsn(LMUL);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("avg_tick_ms");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickTimeArray", "[J");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/MathHelper", "average", "([J)D", false);
mv.visitLdcInsn(new Double("1.0E-6"));
mv.visitInsn(DMUL);
mv.visitInsn(D2I);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l2 = new Label();
mv.visitJumpInsn(GOTO, l2);
Label l3 = new Label();
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 3);
mv.visitInsn(AALOAD);
Label l4 = new Label();
mv.visitJumpInsn(IFNULL, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 3);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorldInfo", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][dimension]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "provider", "Lnet/minecraft/world/WorldProvider;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldProvider", "dimensionId", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][mode]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "getGameType", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][difficulty]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "difficultySetting", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][hardcore]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "isHardcoreModeEnabled", "()Z", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][generator_name]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "getTerrainType", "()Lnet/minecraft/world/WorldType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "getWorldTypeName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][generator_version]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "getTerrainType", "()Lnet/minecraft/world/WorldType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "getGeneratorVersion", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][height]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "buildLimit", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][chunks_loaded]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getChunkProvider", "()Lnet/minecraft/world/chunk/IChunkProvider;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/chunk/IChunkProvider", "getLoadedChunkCount", "()I", true);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitIincInsn(2, 1);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitIincInsn(3, 1);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l3);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("worlds");
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(6, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "addServerTypeToSnooper", "(Lnet/minecraft/profiler/PlayerUsageSnooper;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("singleplayer");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isSinglePlayer", "()Z", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("server_brand");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getServerModName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("gui_supported");
mv.visitMethodInsn(INVOKESTATIC, "java/awt/GraphicsEnvironment", "isHeadless", "()Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitLdcInsn("headless");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 2, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String"});
mv.visitLdcInsn("supported");
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 3, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String", "java/lang/String"});
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("dedicated");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isDedicatedServer", "()Z", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isSnooperEnabled", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "isDedicatedServer", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isServerInOnlineMode", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "onlineMode", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setOnlineMode", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "onlineMode", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getCanSpawnAnimals", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "canSpawnAnimals", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setCanSpawnAnimals", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "canSpawnAnimals", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getCanSpawnNPCs", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "canSpawnNPCs", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setCanSpawnNPCs", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "canSpawnNPCs", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isPVPEnabled", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "pvpEnabled", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setAllowPvp", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "pvpEnabled", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isFlightAllowed", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "allowFlight", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setAllowFlight", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "allowFlight", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "isCommandBlockEnabled", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getMOTD", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "motd", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setMOTD", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "motd", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getBuildLimit", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "buildLimit", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setBuildLimit", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "buildLimit", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getConfigurationManager", "()Lnet/minecraft/server/management/ServerConfigurationManager;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152361_a", "(Lnet/minecraft/server/management/ServerConfigurationManager;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setGameType", "(Lnet/minecraft/world/WorldSettings$GameType;)V", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 2);
Label l0 = new Label();
mv.visitJumpInsn(GOTO, l0);
Label l1 = new Label();
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getServer", "()Lnet/minecraft/server/MinecraftServer;", false);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 2);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorldInfo", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "setGameType", "(Lnet/minecraft/world/WorldSettings$GameType;)V", false);
mv.visitIincInsn(2, 1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l1);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147144_o", "Lnet/minecraft/network/NetworkSystem;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "serverIsInRunLoop", "()Z", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverIsRunning", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getGuiEnabled", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "shareToLAN", "(Lnet/minecraft/world/WorldSettings$GameType;Z)Ljava/lang/String;", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getTickCounter", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickCounter", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "enableProfiling", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "startProfiling", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getPlayerUsageSnooper", "()Lnet/minecraft/profiler/PlayerUsageSnooper;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "usageSnooper", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getPlayerCoordinates", "()Lnet/minecraft/util/ChunkCoordinates;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/util/ChunkCoordinates");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitInsn(ICONST_0);
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ChunkCoordinates", "<init>", "(III)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(5, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getEntityWorld", "()Lnet/minecraft/world/World;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldServers", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSpawnProtectionSize", "()I", null, null);
mv.visitCode();
mv.visitIntInsn(BIPUSH, 16);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isBlockProtected", "(Lnet/minecraft/world/World;IIILnet/minecraft/entity/player/EntityPlayer;)Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getForceGamemode", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "isGamemodeForced", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getServerProxy", "()Ljava/net/Proxy;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverProxy", "Ljava/net/Proxy;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "getSystemTimeMillis", "()J", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_143007_ar", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_143006_e", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_145748_c_", "()Lnet/minecraft/util/IChatComponent;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/util/ChatComponentText");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getCommandSenderName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ChatComponentText", "<init>", "(Ljava/lang/String;)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147136_ar", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147130_as", "()Lcom/mojang/authlib/minecraft/MinecraftSessionService;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152359_aw", "()Lcom/mojang/authlib/GameProfileRepository;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152358_ax", "()Lnet/minecraft/server/management/PlayerProfileCache;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147134_at", "()Lnet/minecraft/network/ServerStatusResponse;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147132_au", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(LCONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getServerHostname", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "hostname", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setHostname", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "hostname", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82010_a", "(Lnet/minecraft/server/gui/IUpdatePlayerListBox;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "tickables", "Ljava/util/List;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "add", "(Ljava/lang/Object;)Z", true);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/NumberFormatException");
Label l3 = new Label();
Label l4 = new Label();
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l4, l5, "java/lang/Exception");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/init/Bootstrap", "func_151354_b", "()V", false);
mv.visitLabel(l3);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 1);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 2);
mv.visitLdcInsn(".");
mv.visitVarInsn(ASTORE, 3);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 4);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 5);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 6);
mv.visitInsn(ICONST_M1);
mv.visitVarInsn(ISTORE, 7);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 8);
Label l6 = new Label();
mv.visitJumpInsn(GOTO, l6);
Label l7 = new Label();
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_FULL, 9, new Object[] {"[Ljava/lang/String;", Opcodes.INTEGER, "java/lang/String", "java/lang/String", "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 8);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ILOAD, 8);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ARRAYLENGTH);
mv.visitInsn(ICONST_1);
mv.visitInsn(ISUB);
Label l8 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l8);
mv.visitInsn(ACONST_NULL);
Label l9 = new Label();
mv.visitJumpInsn(GOTO, l9);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/lang/String"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 8);
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitInsn(AALOAD);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/String"});
mv.visitVarInsn(ASTORE, 10);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("nogui");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l10 = new Label();
mv.visitJumpInsn(IFNE, l10);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--nogui");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
mv.visitJumpInsn(IFNE, l10);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--port");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l11 = new Label();
mv.visitJumpInsn(IFEQ, l11);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l11);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitLabel(l0);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "parseInt", "(Ljava/lang/String;)I", false);
mv.visitVarInsn(ISTORE, 7);
mv.visitLabel(l1);
Label l12 = new Label();
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_FULL, 12, new Object[] {"[Ljava/lang/String;", Opcodes.INTEGER, "java/lang/String", "java/lang/String", "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "java/lang/String", "java/lang/String", Opcodes.INTEGER}, 1, new Object[] {"java/lang/NumberFormatException"});
mv.visitVarInsn(ASTORE, 12);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--singleplayer");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l13 = new Label();
mv.visitJumpInsn(IFEQ, l13);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l13);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ASTORE, 2);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l13);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--universe");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l14 = new Label();
mv.visitJumpInsn(IFEQ, l14);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l14);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ASTORE, 3);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--world");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l15 = new Label();
mv.visitJumpInsn(IFEQ, l15);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l15);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ASTORE, 4);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l15);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--demo");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l16 = new Label();
mv.visitJumpInsn(IFEQ, l16);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 5);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l16);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--bonusChest");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
mv.visitJumpInsn(IFEQ, l12);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 6);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
mv.visitLabel(l12);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 11);
Label l17 = new Label();
mv.visitJumpInsn(IFEQ, l17);
mv.visitIincInsn(8, 1);
mv.visitLabel(l17);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitIincInsn(8, 1);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 8);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IF_ICMPLT, l7);
mv.visitTypeInsn(NEW, "net/minecraft/server/dedicated/DedicatedServer");
mv.visitInsn(DUP);
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/dedicated/DedicatedServer", "<init>", "(Ljava/io/File;)V", false);
mv.visitVarInsn(ASTORE, 8);
mv.visitVarInsn(ALOAD, 2);
Label l18 = new Label();
mv.visitJumpInsn(IFNULL, l18);
mv.visitVarInsn(ALOAD, 8);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "setServerOwner", "(Ljava/lang/String;)V", false);
mv.visitLabel(l18);
mv.visitFrame(Opcodes.F_FULL, 9, new Object[] {"[Ljava/lang/String;", Opcodes.INTEGER, "java/lang/String", "java/lang/String", "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "net/minecraft/server/dedicated/DedicatedServer"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 4);
Label l19 = new Label();
mv.visitJumpInsn(IFNULL, l19);
mv.visitVarInsn(ALOAD, 8);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "setFolderName", "(Ljava/lang/String;)V", false);
mv.visitLabel(l19);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 7);
Label l20 = new Label();
mv.visitJumpInsn(IFLT, l20);
mv.visitVarInsn(ALOAD, 8);
mv.visitVarInsn(ILOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "setServerPort", "(I)V", false);
mv.visitLabel(l20);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 5);
Label l21 = new Label();
mv.visitJumpInsn(IFEQ, l21);
mv.visitVarInsn(ALOAD, 8);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "setDemo", "(Z)V", false);
mv.visitLabel(l21);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 6);
Label l22 = new Label();
mv.visitJumpInsn(IFEQ, l22);
mv.visitVarInsn(ALOAD, 8);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "canCreateBonusChest", "(Z)V", false);
mv.visitLabel(l22);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 1);
Label l23 = new Label();
mv.visitJumpInsn(IFEQ, l23);
mv.visitMethodInsn(INVOKESTATIC, "java/awt/GraphicsEnvironment", "isHeadless", "()Z", false);
mv.visitJumpInsn(IFNE, l23);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "setGuiEnabled", "()V", false);
mv.visitLabel(l23);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "startServerThread", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Runtime", "getRuntime", "()Ljava/lang/Runtime;", false);
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$6");
mv.visitInsn(DUP);
mv.visitLdcInsn("Server Shutdown Thread");
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$6", "<init>", "(Ljava/lang/String;Lnet/minecraft/server/dedicated/DedicatedServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Runtime", "addShutdownHook", "(Ljava/lang/Thread;)V", false);
mv.visitLabel(l4);
Label l24 = new Label();
mv.visitJumpInsn(GOTO, l24);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"[Ljava/lang/String;"}, 1, new Object[] {"java/lang/Exception"});
mv.visitVarInsn(ASTORE, 1);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Failed to start the minecraft server");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "fatal", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l24);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 13);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "logInfo", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getHostname", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "hostname", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getPort", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverPort", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getMotd", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "motd", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getPlugins", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitLdcInsn("");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "handleRConCommand", "(Ljava/lang/String;)Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/network/rcon/RConConsoleSource", "instance", "Lnet/minecraft/network/rcon/RConConsoleSource;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/rcon/RConConsoleSource", "resetLog", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "commandManager", "Lnet/minecraft/command/ICommandManager;");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/network/rcon/RConConsoleSource", "instance", "Lnet/minecraft/network/rcon/RConConsoleSource;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/command/ICommandManager", "executeCommand", "(Lnet/minecraft/command/ICommandSender;Ljava/lang/String;)I", true);
mv.visitInsn(POP);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/network/rcon/RConConsoleSource", "instance", "Lnet/minecraft/network/rcon/RConConsoleSource;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/rcon/RConConsoleSource", "getLogContents", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isDebuggingEnabled", "()Z", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "logSevere", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "logDebug", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "isDebuggingEnabled", "()Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getServerPort", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverPort", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setServerPort", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "serverPort", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_155759_m", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isServerStopped", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverStopped", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setForceGamemode", "(Z)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "isGamemodeForced", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$0", "()Lorg/apache/logging/log4j/Logger;", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1", "(Lnet/minecraft/server/MinecraftServer;)Lnet/minecraft/server/management/ServerConfigurationManager;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "serverConfigManager", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
