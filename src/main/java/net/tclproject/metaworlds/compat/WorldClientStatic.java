package net.tclproject.metaworlds.compat;

import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.profiler.Profiler;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.WorldSettings;
import net.tclproject.metaworlds.patcher.SubWorldFactory;

public class WorldClientStatic extends WorldClient {
	public WorldClientStatic(NetHandlerPlayClient p_i45063_1_, WorldSettings p_i45063_2_, int p_i45063_3_,
			EnumDifficulty p_i45063_4_, Profiler p_i45063_5_) {
		super(p_i45063_1_, p_i45063_2_, p_i45063_3_, p_i45063_4_, p_i45063_5_);
	}

	public static SubWorldFactory subWorldFactory = null;
}
