package net.tclproject.metaworlds.compat;

import net.minecraft.profiler.Profiler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.storage.ISaveHandler;
import net.tclproject.metaworlds.patcher.SubWorldFactory;

public class WorldServerStatic extends WorldServer {
	public WorldServerStatic(MinecraftServer p_i45284_1_, ISaveHandler p_i45284_2_, String p_i45284_3_, int p_i45284_4_,
			WorldSettings p_i45284_5_, Profiler p_i45284_6_) {
		super(p_i45284_1_, p_i45284_2_, p_i45284_3_, p_i45284_4_, p_i45284_5_, p_i45284_6_);
	}

	public static SubWorldFactory subWorldFactory = null;
}
