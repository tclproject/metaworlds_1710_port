package net.tclproject.metaworlds.compat.dumper;
import java.util.*;
import org.objectweb.asm.*;
public class TileEntityDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/tileentity/TileEntity", null, "net/tclproject/metaworlds/patcher/TileEntityBaseSubWorlds", null);

cw.visitInnerClass("net/minecraft/block/BlockJukebox$TileEntityJukebox", "net/minecraft/block/BlockJukebox", "TileEntityJukebox", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/tileentity/TileEntity$1", null, null, 0);

cw.visitInnerClass("net/minecraft/tileentity/TileEntity$2", null, null, 0);

cw.visitInnerClass("net/minecraft/tileentity/TileEntity$3", null, null, 0);

{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "logger", "Lorg/apache/logging/log4j/Logger;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_STATIC, "nameToClassMap", "Ljava/util/Map;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_STATIC, "classToNameMap", "Ljava/util/Map;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "worldObj", "Lnet/minecraft/world/World;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "xCoord", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "yCoord", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "zCoord", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "tileEntityInvalid", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "blockMetadata", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "blockType", "Lnet/minecraft/block/Block;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00000340");
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "isVanilla", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "INFINITE_EXTENT_AABB", "Lnet/minecraft/util/AxisAlignedBB;", null, null);
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "org/apache/logging/log4j/LogManager", "getLogger", "()Lorg/apache/logging/log4j/Logger;", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/tileentity/TileEntity", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/tileentity/TileEntity", "nameToClassMap", "Ljava/util/Map;");
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/tileentity/TileEntity", "classToNameMap", "Ljava/util/Map;");
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityFurnace;"));
mv.visitLdcInsn("Furnace");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityChest;"));
mv.visitLdcInsn("Chest");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityEnderChest;"));
mv.visitLdcInsn("EnderChest");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/block/BlockJukebox$TileEntityJukebox;"));
mv.visitLdcInsn("RecordPlayer");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityDispenser;"));
mv.visitLdcInsn("Trap");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityDropper;"));
mv.visitLdcInsn("Dropper");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntitySign;"));
mv.visitLdcInsn("Sign");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityMobSpawner;"));
mv.visitLdcInsn("MobSpawner");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityNote;"));
mv.visitLdcInsn("Music");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityPiston;"));
mv.visitLdcInsn("Piston");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityBrewingStand;"));
mv.visitLdcInsn("Cauldron");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityEnchantmentTable;"));
mv.visitLdcInsn("EnchantTable");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityEndPortal;"));
mv.visitLdcInsn("Airportal");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityCommandBlock;"));
mv.visitLdcInsn("Control");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityBeacon;"));
mv.visitLdcInsn("Beacon");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntitySkull;"));
mv.visitLdcInsn("Skull");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityDaylightDetector;"));
mv.visitLdcInsn("DLDetector");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityHopper;"));
mv.visitLdcInsn("Hopper");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityComparator;"));
mv.visitLdcInsn("Comparator");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(Type.getType("Lnet/minecraft/tileentity/TileEntityFlowerPot;"));
mv.visitLdcInsn("FlowerPot");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/tileentity/TileEntity", "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", false);
mv.visitLdcInsn(new Double("-Infinity"));
mv.visitLdcInsn(new Double("-Infinity"));
mv.visitLdcInsn(new Double("-Infinity"));
mv.visitLdcInsn(new Double("Infinity"));
mv.visitLdcInsn(new Double("Infinity"));
mv.visitLdcInsn(new Double("Infinity"));
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/AxisAlignedBB", "getBoundingBox", "(DDDDDD)Lnet/minecraft/util/AxisAlignedBB;", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/tileentity/TileEntity", "INFINITE_EXTENT_AABB", "Lnet/minecraft/util/AxisAlignedBB;");
mv.visitInsn(RETURN);
mv.visitMaxs(12, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/TileEntityBaseSubWorlds", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "blockMetadata", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);
mv.visitLdcInsn("net.minecraft.tileentity");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "startsWith", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "isVanilla", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "addMapping", "(Ljava/lang/Class;Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "nameToClassMap", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "containsKey", "(Ljava/lang/Object;)Z", true);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitTypeInsn(NEW, "java/lang/IllegalArgumentException");
mv.visitInsn(DUP);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("Duplicate id: ");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V", false);
mv.visitInsn(ATHROW);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "nameToClassMap", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "classToNameMap", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getWorldObj", "()Lnet/minecraft/world/World;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setWorldObj", "(Lnet/minecraft/world/World;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "hasWorldObj", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "readFromNBT", "(Lnet/minecraft/nbt/NBTTagCompound;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("x");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("y");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "writeToNBT", "(Lnet/minecraft/nbt/NBTTagCompound;)V", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "classToNameMap", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "java/lang/String");
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitTypeInsn(NEW, "java/lang/RuntimeException");
mv.visitInsn(DUP);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/Object;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(" is missing a mapping! This is a bug!");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/RuntimeException", "<init>", "(Ljava/lang/String;)V", false);
mv.visitInsn(ATHROW);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/lang/String"}, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("id");
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setString", "(Ljava/lang/String;Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("x");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("y");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("z");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "updateEntity", "()V", null, null);
mv.visitCode();
mv.visitInsn(RETURN);
mv.visitMaxs(0, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "createAndLoadEntity", "(Lnet/minecraft/nbt/NBTTagCompound;)Lnet/minecraft/tileentity/TileEntity;", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
Label l3 = new Label();
Label l4 = new Label();
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l4, l5, "java/lang/Exception");
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 1);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 2);
mv.visitLabel(l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "nameToClassMap", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("id");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getString", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "java/lang/Class");
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l6 = new Label();
mv.visitJumpInsn(IFNULL, l6);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "newInstance", "()Ljava/lang/Object;", false);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/tileentity/TileEntity");
mv.visitVarInsn(ASTORE, 1);
mv.visitLabel(l1);
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_FULL, 3, new Object[] {"net/minecraft/nbt/NBTTagCompound", "net/minecraft/tileentity/TileEntity", "java/lang/Class"}, 1, new Object[] {"java/lang/Exception"});
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Exception", "printStackTrace", "()V", false);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
Label l7 = new Label();
mv.visitJumpInsn(IFNULL, l7);
mv.visitLabel(l3);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/tileentity/TileEntity", "readFromNBT", "(Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitLabel(l4);
Label l8 = new Label();
mv.visitJumpInsn(GOTO, l8);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Exception"});
mv.visitVarInsn(ASTORE, 3);
mv.visitFieldInsn(GETSTATIC, "org/apache/logging/log4j/Level", "ERROR", "Lorg/apache/logging/log4j/Level;");
mv.visitVarInsn(ALOAD, 3);
mv.visitLdcInsn("A TileEntity %s(%s) has thrown an exception during loading, its state cannot be restored. Report this to the mod author");
mv.visitInsn(ICONST_2);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("id");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getString", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitInsn(AASTORE);
mv.visitInsn(DUP);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLLog", "log", "(Lorg/apache/logging/log4j/Level;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V", false);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 1);
mv.visitJumpInsn(GOTO, l8);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "logger", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitLdcInsn("Skipping BlockEntity with id ");
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("id");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getString", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;)V", true);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ARETURN);
mv.visitMaxs(8, 4);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getBlockMetadata", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "blockMetadata", "I");
mv.visitInsn(ICONST_M1);
Label l0 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getBlockMetadata", "(III)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "blockMetadata", "I");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "blockMetadata", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(5, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "markDirty", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getBlockMetadata", "(III)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "blockMetadata", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "markTileEntityChunkModified", "(IIILnet/minecraft/tileentity/TileEntity;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/tileentity/TileEntity", "getBlockType", "()Lnet/minecraft/block/Block;", false);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/init/Blocks", "air", "Lnet/minecraft/block/Block;");
mv.visitJumpInsn(IF_ACMPEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/tileentity/TileEntity", "getBlockType", "()Lnet/minecraft/block/Block;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "func_147453_f", "(IIILnet/minecraft/block/Block;)V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getDistanceFrom", "(DDD)D", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitInsn(I2D);
mv.visitLdcInsn(new Double("0.5"));
mv.visitInsn(DADD);
mv.visitVarInsn(DLOAD, 1);
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 7);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitInsn(I2D);
mv.visitLdcInsn(new Double("0.5"));
mv.visitInsn(DADD);
mv.visitVarInsn(DLOAD, 3);
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 9);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitInsn(I2D);
mv.visitLdcInsn(new Double("0.5"));
mv.visitInsn(DADD);
mv.visitVarInsn(DLOAD, 5);
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 11);
mv.visitVarInsn(DLOAD, 7);
mv.visitVarInsn(DLOAD, 7);
mv.visitInsn(DMUL);
mv.visitVarInsn(DLOAD, 9);
mv.visitVarInsn(DLOAD, 9);
mv.visitInsn(DMUL);
mv.visitInsn(DADD);
mv.visitVarInsn(DLOAD, 11);
mv.visitVarInsn(DLOAD, 11);
mv.visitInsn(DMUL);
mv.visitInsn(DADD);
mv.visitInsn(DRETURN);
mv.visitMaxs(6, 13);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getMaxRenderDistanceSquared", "()D", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitLdcInsn(new Double("4096.0"));
mv.visitInsn(DRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getBlockType", "()Lnet/minecraft/block/Block;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "blockType", "Lnet/minecraft/block/Block;");
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getBlock", "(III)Lnet/minecraft/block/Block;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "blockType", "Lnet/minecraft/block/Block;");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "blockType", "Lnet/minecraft/block/Block;");
mv.visitInsn(ARETURN);
mv.visitMaxs(5, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getDescriptionPacket", "()Lnet/minecraft/network/Packet;", null, null);
mv.visitCode();
mv.visitInsn(ACONST_NULL);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isInvalid", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "tileEntityInvalid", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "invalidate", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "tileEntityInvalid", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "validate", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "tileEntityInvalid", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "receiveClientEvent", "(II)Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "updateContainingBlockInfo", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "blockType", "Lnet/minecraft/block/Block;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/tileentity/TileEntity", "blockMetadata", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_145828_a", "(Lnet/minecraft/crash/CrashReportCategory;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Name");
mv.visitTypeInsn(NEW, "net/minecraft/tileentity/TileEntity$1");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/tileentity/TileEntity$1", "<init>", "(Lnet/minecraft/tileentity/TileEntity;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/tileentity/TileEntity", "getBlockType", "()Lnet/minecraft/block/Block;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/tileentity/TileEntity", "getBlockMetadata", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/crash/CrashReportCategory", "func_147153_a", "(Lnet/minecraft/crash/CrashReportCategory;IIILnet/minecraft/block/Block;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Actual block type");
mv.visitTypeInsn(NEW, "net/minecraft/tileentity/TileEntity$2");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/tileentity/TileEntity$2", "<init>", "(Lnet/minecraft/tileentity/TileEntity;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Actual block data value");
mv.visitTypeInsn(NEW, "net/minecraft/tileentity/TileEntity$3");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/tileentity/TileEntity$3", "<init>", "(Lnet/minecraft/tileentity/TileEntity;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(6, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "canUpdate", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "onDataPacket", "(Lnet/minecraft/network/NetworkManager;Lnet/minecraft/network/play/server/S35PacketUpdateTileEntity;)V", null, null);
mv.visitCode();
mv.visitInsn(RETURN);
mv.visitMaxs(0, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "onChunkUnload", "()V", null, null);
mv.visitCode();
mv.visitInsn(RETURN);
mv.visitMaxs(0, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "shouldRefresh", "(Lnet/minecraft/block/Block;Lnet/minecraft/block/Block;IILnet/minecraft/world/World;III)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "isVanilla", "Z");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 2);
mv.visitJumpInsn(IF_ACMPNE, l0);
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(2, 9);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "shouldRenderInPass", "(I)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ILOAD, 1);
Label l0 = new Label();
mv.visitJumpInsn(IFNE, l0);
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getRenderBoundingBox", "()Lnet/minecraft/util/AxisAlignedBB;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "INFINITE_EXTENT_AABB", "Lnet/minecraft/util/AxisAlignedBB;");
mv.visitVarInsn(ASTORE, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/tileentity/TileEntity", "getBlockType", "()Lnet/minecraft/block/Block;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/init/Blocks", "enchanting_table", "Lnet/minecraft/block/Block;");
Label l0 = new Label();
mv.visitJumpInsn(IF_ACMPNE, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitInsn(I2D);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/AxisAlignedBB", "getBoundingBox", "(DDDDDD)Lnet/minecraft/util/AxisAlignedBB;", false);
mv.visitVarInsn(ASTORE, 1);
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/util/AxisAlignedBB", "net/minecraft/block/Block"}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/init/Blocks", "chest", "Lnet/minecraft/block/BlockChest;");
Label l2 = new Label();
mv.visitJumpInsn(IF_ACMPEQ, l2);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/init/Blocks", "trapped_chest", "Lnet/minecraft/block/Block;");
Label l3 = new Label();
mv.visitJumpInsn(IF_ACMPNE, l3);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitInsn(ICONST_1);
mv.visitInsn(ISUB);
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitInsn(ICONST_1);
mv.visitInsn(ISUB);
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitInsn(ICONST_2);
mv.visitInsn(IADD);
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitInsn(ICONST_2);
mv.visitInsn(IADD);
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitInsn(ICONST_2);
mv.visitInsn(IADD);
mv.visitInsn(I2D);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/AxisAlignedBB", "getBoundingBox", "(DDDDDD)Lnet/minecraft/util/AxisAlignedBB;", false);
mv.visitVarInsn(ASTORE, 1);
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitJumpInsn(IFNULL, l1);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/init/Blocks", "beacon", "Lnet/minecraft/block/BlockBeacon;");
mv.visitJumpInsn(IF_ACMPEQ, l1);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "xCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "yCoord", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/tileentity/TileEntity", "zCoord", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/block/Block", "getCollisionBoundingBoxFromPool", "(Lnet/minecraft/world/World;III)Lnet/minecraft/util/AxisAlignedBB;", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 3);
mv.visitJumpInsn(IFNULL, l1);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ASTORE, 1);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ARETURN);
mv.visitMaxs(12, 4);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$0", "()Ljava/util/Map;", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/tileentity/TileEntity", "classToNameMap", "Ljava/util/Map;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 0);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
