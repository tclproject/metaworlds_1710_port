package net.tclproject.metaworlds.compat.dumper;
import java.util.*;
import org.objectweb.asm.*;
public class S08PacketPlayerPosLookDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/network/play/server/S08PacketPlayerPosLook", null, "net/minecraft/network/Packet", null);

{
fv = cw.visitField(ACC_PRIVATE, "subWorldBelowFeetID", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_148940_a", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_148938_b", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_148939_c", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_148936_d", "F", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_148937_e", "F", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_148935_f", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001273");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "<init>", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(DDDFFZI)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148940_a", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148938_b", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 5);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148939_c", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(FLOAD, 7);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148936_d", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(FLOAD, 8);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148937_e", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 9);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148935_f", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 10);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "subWorldBelowFeetID", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 11);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "readPacketData", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readDouble", "()D", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148940_a", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readDouble", "()D", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148938_b", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readDouble", "()D", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148939_c", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readFloat", "()F", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148936_d", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readFloat", "()F", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148937_e", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readBoolean", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148935_f", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "subWorldBelowFeetID", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "writePacketData", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148940_a", "D");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeDouble", "(D)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148938_b", "D");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeDouble", "(D)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148939_c", "D");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeDouble", "(D)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148936_d", "F");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeFloat", "(F)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148937_e", "F");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeFloat", "(F)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148935_f", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeBoolean", "(Z)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "subWorldBelowFeetID", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "processPacket", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/network/play/INetHandlerPlayClient", "handlePlayerPosLook", "(Lnet/minecraft/network/play/server/S08PacketPlayerPosLook;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "processPacket", "(Lnet/minecraft/network/INetHandler;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/network/play/INetHandlerPlayClient");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "processPacket", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148932_c", "()D", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148940_a", "D");
mv.visitInsn(DRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148928_d", "()D", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148938_b", "D");
mv.visitInsn(DRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148933_e", "()D", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148939_c", "D");
mv.visitInsn(DRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148931_f", "()F", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148936_d", "F");
mv.visitInsn(FRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148930_g", "()F", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148937_e", "F");
mv.visitInsn(FRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148929_h", "()Z", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S08PacketPlayerPosLook", "field_148935_f", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
