package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class RenderListDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/client/renderer/RenderList", null, "java/lang/Object", null);

{
av0 = cw.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "field_78429_a", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "field_78427_b", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "field_78428_c", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_78425_d", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_78426_e", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_78423_f", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "subWorldID", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "customTransformation", "Ljava/nio/DoubleBuffer;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_78424_g", "Ljava/nio/IntBuffer;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_78430_h", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_78431_i", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00000957");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn(new Integer(65536));
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/client/renderer/GLAllocation", "func_74527_f", "(I)Ljava/nio/IntBuffer;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78424_g", "Ljava/nio/IntBuffer;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setupRenderList", "(IIIDDDILjava/nio/DoubleBuffer;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78430_h", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78424_g", "Ljava/nio/IntBuffer;");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/nio/IntBuffer", "clear", "()Ljava/nio/Buffer;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78429_a", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78427_b", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78428_c", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 4);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78425_d", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 6);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78426_e", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 8);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78423_f", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 10);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "subWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 11);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "customTransformation", "Ljava/nio/DoubleBuffer;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 12);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "rendersChunk", "(IIII)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78430_h", "Z");
Label l0 = new Label();
mv.visitJumpInsn(IFNE, l0);
mv.visitInsn(ICONST_0);
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78429_a", "I");
Label l2 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l2);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78427_b", "I");
mv.visitJumpInsn(IF_ICMPNE, l2);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78428_c", "I");
mv.visitJumpInsn(IF_ICMPNE, l2);
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "subWorldID", "I");
mv.visitJumpInsn(IF_ICMPNE, l2);
mv.visitInsn(ICONST_1);
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitInsn(IRETURN);
mv.visitMaxs(2, 5);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_78420_a", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78424_g", "Ljava/nio/IntBuffer;");
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/nio/IntBuffer", "put", "(I)Ljava/nio/IntBuffer;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78424_g", "Ljava/nio/IntBuffer;");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/nio/IntBuffer", "remaining", "()I", false);
Label l0 = new Label();
mv.visitJumpInsn(IFNE, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/renderer/RenderList", "func_78419_a", "()V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_78419_a", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78430_h", "Z");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78431_i", "Z");
Label l1 = new Label();
mv.visitJumpInsn(IFNE, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78424_g", "Ljava/nio/IntBuffer;");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/nio/IntBuffer", "flip", "()Ljava/nio/Buffer;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78431_i", "Z");
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78424_g", "Ljava/nio/IntBuffer;");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/nio/IntBuffer", "remaining", "()I", false);
mv.visitJumpInsn(IFLE, l0);
mv.visitMethodInsn(INVOKESTATIC, "org/lwjgl/opengl/GL11", "glPushMatrix", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78425_d", "D");
mv.visitInsn(DNEG);
mv.visitInsn(D2F);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78426_e", "D");
mv.visitInsn(DNEG);
mv.visitInsn(D2F);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78423_f", "D");
mv.visitInsn(DNEG);
mv.visitInsn(D2F);
mv.visitMethodInsn(INVOKESTATIC, "org/lwjgl/opengl/GL11", "glTranslatef", "(FFF)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "customTransformation", "Ljava/nio/DoubleBuffer;");
Label l2 = new Label();
mv.visitJumpInsn(IFNULL, l2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "customTransformation", "Ljava/nio/DoubleBuffer;");
mv.visitMethodInsn(INVOKESTATIC, "org/lwjgl/opengl/GL11", "glMultMatrix", "(Ljava/nio/DoubleBuffer;)V", false);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78429_a", "I");
mv.visitInsn(I2F);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78427_b", "I");
mv.visitInsn(I2F);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78428_c", "I");
mv.visitInsn(I2F);
mv.visitMethodInsn(INVOKESTATIC, "org/lwjgl/opengl/GL11", "glTranslatef", "(FFF)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/RenderList", "field_78424_g", "Ljava/nio/IntBuffer;");
mv.visitMethodInsn(INVOKESTATIC, "org/lwjgl/opengl/GL11", "glCallLists", "(Ljava/nio/IntBuffer;)V", false);
mv.visitMethodInsn(INVOKESTATIC, "org/lwjgl/opengl/GL11", "glPopMatrix", "()V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_78421_b", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78430_h", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/RenderList", "field_78431_i", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
