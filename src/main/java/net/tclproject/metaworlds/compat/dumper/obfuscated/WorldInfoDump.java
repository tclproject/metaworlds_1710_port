package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class WorldInfoDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/world/storage/WorldInfo", null, "java/lang/Object", new String[] { "net/tclproject/metaworlds/api/WorldInfoSuperClass" });

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$9", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$8", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$7", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$6", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$5", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$4", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$3", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$2", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$1", null, null, 0);

cw.visitInnerClass("net/minecraft/world/WorldSettings$GameType", "net/minecraft/world/WorldSettings", "GameType", ACC_PUBLIC + ACC_STATIC + ACC_ENUM);

{
fv = cw.visitField(ACC_PRIVATE, "field_76100_a", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76098_b", "Lnet/minecraft/world/WorldType;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_82576_c", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76099_c", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76096_d", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76097_e", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_82575_g", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76094_f", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76095_g", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76107_h", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76108_i", "Lnet/minecraft/nbt/NBTTagCompound;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76105_j", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76106_k", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76103_l", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76104_m", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76101_n", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76102_o", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76114_p", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76112_r", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76111_s", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76110_t", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_76109_u", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_82577_x", "Lnet/minecraft/world/GameRules;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "additionalProperties", "Ljava/util/Map;", "Ljava/util/Map<Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00000587");
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "subWorldIDsByDimension", "Ljava/util/Map;", "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Ljava/lang/Integer;>;>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "subWorldInfoByID", "Ljava/util/Map;", "Ljava/util/Map<Ljava/lang/Integer;Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "nextSubWorldID", "I", null, null);
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "field_77137_b", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/nbt/NBTTagCompound;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "field_77137_b", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("RandomSeed");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74763_f", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76100_a", "J");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorName");
mv.visitIntInsn(BIPUSH, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorName");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74779_i", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/world/WorldType", "func_77130_a", "(Ljava/lang/String;)Lnet/minecraft/world/WorldType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
Label l1 = new Label();
mv.visitJumpInsn(IFNONNULL, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "field_77137_b", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
Label l2 = new Label();
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 3, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound", "java/lang/String"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "func_77125_e", "()Z", false);
mv.visitJumpInsn(IFEQ, l2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorVersion");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l3 = new Label();
mv.visitJumpInsn(IFEQ, l3);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorVersion");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitVarInsn(ISTORE, 3);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ILOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "func_77132_a", "(I)Lnet/minecraft/world/WorldType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorOptions");
mv.visitIntInsn(BIPUSH, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorOptions");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74779_i", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameType");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/world/WorldSettings$GameType", "func_77146_a", "(I)Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("MapFeatures");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l4 = new Label();
mv.visitJumpInsn(IFEQ, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("MapFeatures");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74767_n", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
Label l5 = new Label();
mv.visitJumpInsn(GOTO, l5);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnX");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnY");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnZ");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Time");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74763_f", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("DayTime");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l6 = new Label();
mv.visitJumpInsn(IFEQ, l6);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("DayTime");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74763_f", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
Label l7 = new Label();
mv.visitJumpInsn(GOTO, l7);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LastPlayed");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74763_f", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76095_g", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SizeOnDisk");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74763_f", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76107_h", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LevelName");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74779_i", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76106_k", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("version");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76103_l", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("rainTime");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76101_n", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("raining");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74767_n", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76104_m", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thunderTime");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76114_p", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thundering");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74767_n", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76102_o", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("hardcore");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74767_n", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76111_s", "Z");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("initialized");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l8 = new Label();
mv.visitJumpInsn(IFEQ, l8);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("initialized");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74767_n", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
Label l9 = new Label();
mv.visitJumpInsn(GOTO, l9);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("allowCommands");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l10 = new Label();
mv.visitJumpInsn(IFEQ, l10);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("allowCommands");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74767_n", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
Label l11 = new Label();
mv.visitJumpInsn(GOTO, l11);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldSettings$GameType", "CREATIVE", "Lnet/minecraft/world/WorldSettings$GameType;");
Label l12 = new Label();
mv.visitJumpInsn(IF_ACMPNE, l12);
mv.visitInsn(ICONST_1);
Label l13 = new Label();
mv.visitJumpInsn(GOTO, l13);
mv.visitLabel(l12);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/storage/WorldInfo"});
mv.visitInsn(ICONST_0);
mv.visitLabel(l13);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound"}, 2, new Object[] {"net/minecraft/world/storage/WorldInfo", Opcodes.INTEGER});
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Player");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l14 = new Label();
mv.visitJumpInsn(IFEQ, l14);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Player");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74775_l", "(Ljava/lang/String;)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76108_i", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76108_i", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitLdcInsn("Dimension");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76105_j", "I");
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameRules");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150297_b", "(Ljava/lang/String;I)Z", false);
Label l15 = new Label();
mv.visitJumpInsn(IFEQ, l15);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameRules");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74775_l", "(Ljava/lang/String;)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/GameRules", "func_82768_a", "(Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitLabel(l15);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("NextSubWorldID");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74764_b", "(Ljava/lang/String;)Z", false);
Label l16 = new Label();
mv.visitJumpInsn(IFEQ, l16);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("NextSubWorldID");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitLabel(l16);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldIDsByDimension");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74764_b", "(Ljava/lang/String;)Z", false);
Label l17 = new Label();
mv.visitJumpInsn(IFEQ, l17);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldIDsByDimension");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150295_c", "(Ljava/lang/String;I)Lnet/minecraft/nbt/NBTTagList;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l18 = new Label();
mv.visitLabel(l18);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/nbt/NBTTagList", Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "func_74745_c", "()I", false);
mv.visitJumpInsn(IF_ICMPGE, l17);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "func_150305_b", "(I)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitLdcInsn("DimID");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74762_e", "(Ljava/lang/String;)I", false);
mv.visitVarInsn(ISTORE, 5);
mv.visitVarInsn(ALOAD, 4);
mv.visitLdcInsn("SubWorldIDs");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74759_k", "(Ljava/lang/String;)[I", false);
mv.visitVarInsn(ASTORE, 6);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ILOAD, 5);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitTypeInsn(NEW, "java/util/HashSet");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKESTATIC, "com/google/common/primitives/Ints", "asList", "([I)Ljava/util/List;", false);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashSet", "<init>", "(Ljava/util/Collection;)V", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitIincInsn(3, 1);
mv.visitJumpInsn(GOTO, l18);
mv.visitLabel(l17);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldInfos");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74764_b", "(Ljava/lang/String;)Z", false);
Label l19 = new Label();
mv.visitJumpInsn(IFEQ, l19);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldInfos");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_150295_c", "(Ljava/lang/String;I)Lnet/minecraft/nbt/NBTTagList;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l20 = new Label();
mv.visitLabel(l20);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/nbt/NBTTagList", Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "func_74745_c", "()I", false);
mv.visitJumpInsn(IF_ICMPGE, l19);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "func_150305_b", "(I)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitTypeInsn(NEW, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "<init>", "(Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "subWorldId", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitIincInsn(3, 1);
mv.visitJumpInsn(GOTO, l20);
mv.visitLabel(l19);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/world/WorldSettings;Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "field_77137_b", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_77160_d", "()J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76100_a", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_77162_e", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_77164_g", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76106_k", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_77158_f", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76111_s", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_77165_h", "()Lnet/minecraft/world/WorldType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_82749_j", "()Ljava/lang/String;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_77163_i", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "field_77137_b", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76100_a", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76100_a", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76095_g", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76095_g", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76107_h", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76107_h", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76108_i", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76108_i", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76105_j", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76105_j", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76106_k", "Ljava/lang/String;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76106_k", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76103_l", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76103_l", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76101_n", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76101_n", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76104_m", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76104_m", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76114_p", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76114_p", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76102_o", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76102_o", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76111_s", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76111_s", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76066_a", "()Lnet/minecraft/nbt/NBTTagCompound;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76108_i", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo", "func_76064_a", "(Lnet/minecraft/nbt/NBTTagCompound;Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76082_a", "(Lnet/minecraft/nbt/NBTTagCompound;)Lnet/minecraft/nbt/NBTTagCompound;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo", "func_76064_a", "(Lnet/minecraft/nbt/NBTTagCompound;Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PRIVATE, "func_76064_a", "(Lnet/minecraft/nbt/NBTTagCompound;Lnet/minecraft/nbt/NBTTagCompound;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("RandomSeed");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76100_a", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74772_a", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorName");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "func_77127_a", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74778_a", "(Ljava/lang/String;Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorVersion");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "func_77131_c", "()I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorOptions");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74778_a", "(Ljava/lang/String;Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameType");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings$GameType", "func_77148_a", "()I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("MapFeatures");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74757_a", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnX");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnY");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnZ");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Time");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74772_a", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("DayTime");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74772_a", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SizeOnDisk");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76107_h", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74772_a", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LastPlayed");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74772_a", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LevelName");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76106_k", "Ljava/lang/String;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74778_a", "(Ljava/lang/String;Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("version");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76103_l", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("rainTime");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76101_n", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("raining");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76104_m", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74757_a", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thunderTime");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76114_p", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thundering");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76102_o", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74757_a", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("hardcore");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76111_s", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74757_a", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("allowCommands");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74757_a", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("initialized");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74757_a", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameRules");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/GameRules", "func_82770_a", "()Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74782_a", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitVarInsn(ALOAD, 2);
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Player");
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74782_a", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("NextSubWorldID");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagList", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorlds", "()[Lnet/minecraft/world/WorldServer;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 5);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 6);
Label l1 = new Label();
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 7, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagList", "[Lnet/minecraft/world/WorldServer;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 6);
mv.visitVarInsn(ILOAD, 5);
Label l2 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l2);
mv.visitVarInsn(ALOAD, 4);
mv.visitVarInsn(ILOAD, 6);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 7);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 8);
mv.visitVarInsn(ALOAD, 8);
mv.visitLdcInsn("DimID");
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getDimension", "()I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74768_a", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 8);
mv.visitLdcInsn("SubWorldIDs");
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getSubWorldsMap", "()Ljava/util/Map;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "keySet", "()Ljava/util/Set;", true);
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Integer");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Set", "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "[Ljava/lang/Integer;");
mv.visitMethodInsn(INVOKESTATIC, "org/apache/commons/lang3/ArrayUtils", "toPrimitive", "([Ljava/lang/Integer;)[I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74783_a", "(Ljava/lang/String;[I)V", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "func_74742_a", "(Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getSubWorlds", "()Ljava/util/Collection;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Collection", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 9);
Label l3 = new Label();
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {"net/minecraft/world/WorldServer", "net/minecraft/nbt/NBTTagCompound", "java/util/Iterator"}, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
Label l4 = new Label();
mv.visitJumpInsn(IFEQ, l4);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/world/World");
mv.visitVarInsn(ASTORE, 10);
mv.visitTypeInsn(NEW, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 10);
mv.visitTypeInsn(CHECKCAST, "net/tclproject/metaworlds/api/SubWorld");
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "<init>", "(Lnet/tclproject/metaworlds/api/SubWorld;)V", false);
mv.visitVarInsn(ASTORE, 11);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getSubWorldID", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitVarInsn(ALOAD, 11);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitJumpInsn(GOTO, l3);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitIincInsn(6, 1);
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldIDsByDimension");
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74782_a", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagList", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "values", "()Ljava/util/Collection;", true);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Collection", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 5);
Label l5 = new Label();
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/nbt/NBTTagList", "java/util/Iterator"}, 0, null);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
Label l6 = new Label();
mv.visitJumpInsn(IFEQ, l6);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitVarInsn(ASTORE, 6);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ALOAD, 6);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "writeToNBT", "(Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "func_74742_a", "(Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitJumpInsn(GOTO, l5);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldInfos");
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "func_74782_a", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 12);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getNextSubWorldID", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(DUP);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitInsn(DUP_X1);
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(4, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldIDs", "(I)Ljava/util/Collection;", "(I)Ljava/util/Collection<Ljava/lang/Integer;>;", null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "java/util/Collection");
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "updateSubWorldInfo", "(Lnet/tclproject/metaworlds/api/SubWorld;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/tclproject/metaworlds/api/SubWorld", "getSubWorldID", "()I", true);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitTypeInsn(NEW, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "<init>", "(Lnet/tclproject/metaworlds/api/SubWorld;)V", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "updateSubWorldInfo", "(Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "subWorldId", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldInfo", "(I)Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldInfos", "()Ljava/util/Collection;", "()Ljava/util/Collection<Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;>;", null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "values", "()Ljava/util/Collection;", true);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76063_b", "()J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76100_a", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76079_c", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76075_d", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76074_e", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82573_f", "()J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76073_f", "()J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76092_g", "()J", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76107_h", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76072_h", "()Lnet/minecraft/nbt/NBTTagCompound;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76108_i", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76076_i", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76105_j", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76058_a", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76056_b", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82572_b", "(J)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76087_c", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76068_b", "(J)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76081_a", "(III)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 4);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76065_j", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76106_k", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76062_a", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76106_k", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76088_k", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76103_l", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76078_e", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76103_l", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76057_l", "()J", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76095_g", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76061_m", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76102_o", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76069_a", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76102_o", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76071_n", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76114_p", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76090_f", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76114_p", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76059_o", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76104_m", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76084_b", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76104_m", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76083_p", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76101_n", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76080_g", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76101_n", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76077_q", "()Lnet/minecraft/world/WorldSettings$GameType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76089_r", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76060_a", "(Lnet/minecraft/world/WorldSettings$GameType;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76093_s", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76111_s", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76067_t", "()Lnet/minecraft/world/WorldType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76085_a", "(Lnet/minecraft/world/WorldType;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82571_y", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76086_u", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76070_v", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_76091_d", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "field_76109_u", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82574_x", "()Lnet/minecraft/world/GameRules;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82577_x", "Lnet/minecraft/world/GameRules;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_85118_a", "(Lnet/minecraft/crash/CrashReportCategory;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level seed");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$1");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$1", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level generator");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$2");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$2", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level generator options");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$3");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$3", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level spawn location");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$4");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$4", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level time");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$5");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$5", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level dimension");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$6");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$6", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level storage version");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$7");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$7", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level weather");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$8");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$8", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level game mode");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$9");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$9", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setAdditionalProperties", "(Ljava/util/Map;)V", "(Ljava/util/Map<Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;>;)V", null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getAdditionalProperty", "(Ljava/lang/String;)Lnet/minecraft/nbt/NBTBase;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/nbt/NBTBase");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ACONST_NULL);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/nbt/NBTBase"});
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$000", "(Lnet/minecraft/world/storage/WorldInfo;)Lnet/minecraft/world/WorldType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76098_b", "Lnet/minecraft/world/WorldType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$100", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76112_r", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$200", "(Lnet/minecraft/world/storage/WorldInfo;)Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82576_c", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$300", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76099_c", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$400", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76096_d", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$500", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76097_e", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$600", "(Lnet/minecraft/world/storage/WorldInfo;)J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_82575_g", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$700", "(Lnet/minecraft/world/storage/WorldInfo;)J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76094_f", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$800", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76105_j", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$900", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76103_l", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1000", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76101_n", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1100", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76104_m", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1200", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76114_p", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1300", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76102_o", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1400", "(Lnet/minecraft/world/storage/WorldInfo;)Lnet/minecraft/world/WorldSettings$GameType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76113_q", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1500", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76111_s", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1600", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "field_76110_t", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
