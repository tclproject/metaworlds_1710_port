package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class C03PacketPlayer$C06PacketPlayerPosLookDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", null, "net/minecraft/network/play/client/C03PacketPlayer", null);

cw.visitInnerClass("net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "net/minecraft/network/play/client/C03PacketPlayer", "C06PacketPlayerPosLook", ACC_PUBLIC + ACC_STATIC);

{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001362");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149480_h", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149481_i", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(DDDDFFZIBZ)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149479_a", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149477_b", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 5);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149475_d", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(DLOAD, 7);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149478_c", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(FLOAD, 9);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149476_e", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(FLOAD, 10);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149473_f", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 11);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149474_g", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 12);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "subWorldBelowFeetID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 13);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "tractionLoss", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 14);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "losingTraction", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149481_i", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149480_h", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 15);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readDouble", "()D", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149479_a", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readDouble", "()D", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149477_b", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readDouble", "()D", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149475_d", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readDouble", "()D", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149478_c", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readFloat", "()F", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149476_e", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readFloat", "()F", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149473_f", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149479_a", "D");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeDouble", "(D)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149477_b", "D");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeDouble", "(D)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149475_d", "D");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeDouble", "(D)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149478_c", "D");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeDouble", "(D)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149476_e", "F");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeFloat", "(F)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook", "field_149473_f", "F");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeFloat", "(F)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148833_a", "(Lnet/minecraft/network/INetHandler;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/network/play/INetHandlerPlayServer");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "func_148833_a", "(Lnet/minecraft/network/play/INetHandlerPlayServer;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
