package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class MinecraftServerDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER + ACC_ABSTRACT, "net/minecraft/server/MinecraftServer", null, "java/lang/Object", new String[] { "net/minecraft/command/ICommandSender", "java/lang/Runnable", "net/minecraft/profiler/IPlayerUsage" });

cw.visitInnerClass("net/minecraft/server/MinecraftServer$6", null, null, ACC_STATIC);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$5", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$4", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$3", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$2", null, null, 0);

cw.visitInnerClass("net/minecraft/server/MinecraftServer$1", null, null, 0);

cw.visitInnerClass("net/minecraft/world/WorldSettings$GameType", "net/minecraft/world/WorldSettings", "GameType", ACC_PUBLIC + ACC_STATIC + ACC_ENUM);

cw.visitInnerClass("cpw/mods/fml/common/StartupQuery$AbortedException", "cpw/mods/fml/common/StartupQuery", "AbortedException", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraftforge/event/world/WorldEvent$Load", "net/minecraftforge/event/world/WorldEvent", "Load", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraftforge/event/world/WorldEvent$Unload", "net/minecraftforge/event/world/WorldEvent", "Unload", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier", "net/minecraft/network/ServerStatusResponse", "MinecraftProtocolVersionIdentifier", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/network/ServerStatusResponse$PlayerCountData", "net/minecraft/network/ServerStatusResponse", "PlayerCountData", ACC_PUBLIC + ACC_STATIC);

{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "field_147145_h", "Lorg/apache/logging/log4j/Logger;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "field_152367_a", "Ljava/io/File;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_STATIC, "field_71309_l", "Lnet/minecraft/server/MinecraftServer;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_71310_m", "Lnet/minecraft/world/storage/ISaveFormat;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_71308_o", "Ljava/io/File;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_71322_p", "Ljava/util/List;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_71321_q", "Lnet/minecraft/command/ICommandManager;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL, "field_71304_b", "Lnet/minecraft/profiler/Profiler;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147144_o", "Lnet/minecraft/network/NetworkSystem;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147146_q", "Ljava/util/Random;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71320_r", "Ljava/lang/String;", null, null);
{
av0 = fv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71319_s", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "field_71305_c", "[Lnet/minecraft/world/WorldServer;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71317_u", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71316_v", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71315_w", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED + ACC_FINAL, "field_110456_c", "Ljava/net/Proxy;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "field_71302_d", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "field_71303_e", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71325_x", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71324_y", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71323_z", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71284_A", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71285_B", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71286_C", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71280_D", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_143008_E", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC + ACC_FINAL, "field_71311_j", "[J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "worldTickTimes", "Ljava/util/Hashtable;", "Ljava/util/Hashtable<Ljava/lang/Integer;[J>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71292_I", "Ljava/security/KeyPair;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71293_J", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71294_K", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71287_L", "Ljava/lang/String;", null, null);
{
av0 = fv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71288_M", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71289_N", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71290_O", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_147141_M", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71296_Q", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71299_R", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71298_S", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_71295_T", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_104057_T", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_147142_T", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001462");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Ljava/io/File;Ljava/net/Proxy;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/PlayerUsageSnooper");
mv.visitInsn(DUP);
mv.visitLdcInsn("server");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/PlayerUsageSnooper", "<init>", "(Ljava/lang/String;Lnet/minecraft/profiler/IPlayerUsage;J)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/ArrayList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71322_p", "Ljava/util/List;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/Profiler");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/Profiler", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Random");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Random", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147146_q", "Ljava/util/Random;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71319_s", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "net/minecraft/world/WorldServer");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71317_u", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitIntInsn(BIPUSH, 100);
mv.visitIntInsn(NEWARRAY, T_LONG);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71311_j", "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Hashtable");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Hashtable", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(LCONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/server/management/PlayerProfileCache");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_152367_a", "Ljava/io/File;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/management/PlayerProfileCache", "<init>", "(Lnet/minecraft/server/MinecraftServer;Ljava/io/File;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/server/MinecraftServer", "field_71309_l", "Lnet/minecraft/server/MinecraftServer;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_110456_c", "Ljava/net/Proxy;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71308_o", "Ljava/io/File;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/network/NetworkSystem");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/NetworkSystem", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147144_o", "Lnet/minecraft/network/NetworkSystem;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/command/ServerCommandManager");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/command/ServerCommandManager", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71321_q", "Lnet/minecraft/command/ICommandManager;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/chunk/storage/AnvilSaveConverter");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/chunk/storage/AnvilSaveConverter", "<init>", "(Ljava/io/File;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71310_m", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "java/util/UUID", "randomUUID", "()Ljava/util/UUID;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/UUID", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService", "<init>", "(Ljava/net/Proxy;Ljava/lang/String;)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitMethodInsn(INVOKEVIRTUAL, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService", "createMinecraftSessionService", "()Lcom/mojang/authlib/minecraft/MinecraftSessionService;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitMethodInsn(INVOKEVIRTUAL, "com/mojang/authlib/yggdrasil/YggdrasilAuthenticationService", "createProfileRepository", "()Lcom/mojang/authlib/GameProfileRepository;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitInsn(RETURN);
mv.visitMaxs(7, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/PlayerUsageSnooper");
mv.visitInsn(DUP);
mv.visitLdcInsn("server");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/PlayerUsageSnooper", "<init>", "(Ljava/lang/String;Lnet/minecraft/profiler/IPlayerUsage;J)V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/ArrayList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71322_p", "Ljava/util/List;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/profiler/Profiler");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/profiler/Profiler", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Random");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Random", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147146_q", "Ljava/util/Random;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_M1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71319_s", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "net/minecraft/world/WorldServer");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71317_u", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitIntInsn(BIPUSH, 100);
mv.visitIntInsn(NEWARRAY, T_LONG);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71311_j", "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/Hashtable");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Hashtable", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(LCONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_110456_c", "Ljava/net/Proxy;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71308_o", "Ljava/io/File;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147144_o", "Lnet/minecraft/network/NetworkSystem;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71321_q", "Lnet/minecraft/command/ICommandManager;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71310_m", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152364_T", "Lcom/mojang/authlib/yggdrasil/YggdrasilAuthenticationService;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitInsn(RETURN);
mv.visitMaxs(7, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED + ACC_ABSTRACT, "func_71197_b", "()Z", null, new String[] { "java/io/IOException" });
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71237_c", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71254_M", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "func_75801_b", "(Ljava/lang/String;)Z", true);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Converting map!");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("menu.convertingLevel");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71192_d", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71254_M", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$1");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$1", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "func_75805_a", "(Ljava/lang/String;Lnet/minecraft/util/IProgressUpdate;)Z", true);
mv.visitInsn(POP);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED + ACC_SYNCHRONIZED, "func_71192_d", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71298_S", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_SYNCHRONIZED, "func_71195_b_", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71298_S", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71247_a", "(Ljava/lang/String;Ljava/lang/String;JLnet/minecraft/world/WorldType;Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71237_c", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("menu.loadingLevel");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71192_d", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71310_m", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "func_75804_a", "(Ljava/lang/String;Z)Lnet/minecraft/world/storage/ISaveHandler;", true);
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveHandler", "func_75757_d", "()Lnet/minecraft/world/storage/WorldInfo;", true);
mv.visitVarInsn(ASTORE, 8);
mv.visitVarInsn(ALOAD, 8);
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldSettings");
mv.visitInsn(DUP);
mv.visitVarInsn(LLOAD, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71265_f", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71225_e", "()Z", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71199_h", "()Z", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldSettings", "<init>", "(JLnet/minecraft/world/WorldSettings$GameType;ZZLnet/minecraft/world/WorldType;)V", false);
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_82750_a", "(Ljava/lang/String;)Lnet/minecraft/world/WorldSettings;", false);
mv.visitInsn(POP);
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/world/storage/ISaveHandler", "net/minecraft/world/storage/WorldInfo"}, 0, null);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldSettings");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldSettings", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitVarInsn(ASTORE, 9);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldSettings"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71289_N", "Z");
Label l2 = new Label();
mv.visitJumpInsn(IFEQ, l2);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_77159_a", "()Lnet/minecraft/world/WorldSettings;", false);
mv.visitInsn(POP);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71242_L", "()Z", false);
Label l3 = new Label();
mv.visitJumpInsn(IFEQ, l3);
mv.visitTypeInsn(NEW, "net/minecraft/world/demo/DemoWorldServer");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 7);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/demo/DemoWorldServer", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/storage/ISaveHandler;Ljava/lang/String;ILnet/minecraft/profiler/Profiler;)V", false);
Label l4 = new Label();
mv.visitJumpInsn(GOTO, l4);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldServer");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 7);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldServer", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/storage/ISaveHandler;Ljava/lang/String;ILnet/minecraft/world/WorldSettings;Lnet/minecraft/profiler/Profiler;)V", false);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/WorldServer"});
mv.visitVarInsn(ASTORE, 10);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getStaticDimensionIDs", "()[Ljava/lang/Integer;", false);
mv.visitVarInsn(ASTORE, 11);
mv.visitVarInsn(ALOAD, 11);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 12);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 13);
Label l5 = new Label();
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_FULL, 13, new Object[] {"net/minecraft/server/MinecraftServer", "java/lang/String", "java/lang/String", Opcodes.LONG, "net/minecraft/world/WorldType", "java/lang/String", "net/minecraft/world/storage/ISaveHandler", "net/minecraft/world/storage/WorldInfo", "net/minecraft/world/WorldSettings", "net/minecraft/world/WorldServer", "[Ljava/lang/Integer;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 13);
mv.visitVarInsn(ILOAD, 12);
Label l6 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l6);
mv.visitVarInsn(ALOAD, 11);
mv.visitVarInsn(ILOAD, 13);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
mv.visitVarInsn(ISTORE, 14);
mv.visitVarInsn(ILOAD, 14);
Label l7 = new Label();
mv.visitJumpInsn(IFNE, l7);
mv.visitVarInsn(ALOAD, 10);
Label l8 = new Label();
mv.visitJumpInsn(GOTO, l8);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldServerMulti");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 7);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 14);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldServerMulti", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/storage/ISaveHandler;Ljava/lang/String;ILnet/minecraft/world/WorldSettings;Lnet/minecraft/world/WorldServer;Lnet/minecraft/profiler/Profiler;)V", false);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/WorldServer"});
mv.visitVarInsn(ASTORE, 15);
mv.visitVarInsn(ALOAD, 15);
mv.visitTypeInsn(NEW, "net/minecraft/world/WorldManager");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 15);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/WorldManager", "<init>", "(Lnet/minecraft/server/MinecraftServer;Lnet/minecraft/world/WorldServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72954_a", "(Lnet/minecraft/world/IWorldAccess;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71264_H", "()Z", false);
Label l9 = new Label();
mv.visitJumpInsn(IFNE, l9);
mv.visitVarInsn(ALOAD, 15);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72912_H", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71265_f", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76060_a", "(Lnet/minecraft/world/WorldSettings$GameType;)V", false);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitFieldInsn(GETSTATIC, "net/minecraftforge/common/MinecraftForge", "EVENT_BUS", "Lcpw/mods/fml/common/eventhandler/EventBus;");
mv.visitTypeInsn(NEW, "net/minecraftforge/event/world/WorldEvent$Load");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 15);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraftforge/event/world/WorldEvent$Load", "<init>", "(Lnet/minecraft/world/World;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/eventhandler/EventBus", "post", "(Lcpw/mods/fml/common/eventhandler/Event;)Z", false);
mv.visitInsn(POP);
mv.visitIincInsn(13, 1);
mv.visitJumpInsn(GOTO, l5);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_FULL, 10, new Object[] {"net/minecraft/server/MinecraftServer", "java/lang/String", "java/lang/String", Opcodes.LONG, "net/minecraft/world/WorldType", "java/lang/String", "net/minecraft/world/storage/ISaveHandler", "net/minecraft/world/storage/WorldInfo", "net/minecraft/world/WorldSettings", "net/minecraft/world/WorldServer"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(ICONST_1);
mv.visitTypeInsn(ANEWARRAY, "net/minecraft/world/WorldServer");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 10);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72364_a", "([Lnet/minecraft/world/WorldServer;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147135_j", "()Lnet/minecraft/world/EnumDifficulty;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147139_a", "(Lnet/minecraft/world/EnumDifficulty;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71222_d", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(9, 16);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71222_d", "()V", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 1);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 2);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 3);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 4);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("menu.generatingTerrain");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71192_d", "(Ljava/lang/String;)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 6);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("Preparing start region for level ");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 6);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 6);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72861_E", "()Lnet/minecraft/util/ChunkCoordinates;", false);
mv.visitVarInsn(ASTORE, 8);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitVarInsn(LSTORE, 9);
mv.visitIntInsn(SIPUSH, -192);
mv.visitVarInsn(ISTORE, 11);
Label l0 = new Label();
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_FULL, 11, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "net/minecraft/world/WorldServer", "net/minecraft/util/ChunkCoordinates", Opcodes.LONG, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 11);
mv.visitIntInsn(SIPUSH, 192);
Label l1 = new Label();
mv.visitJumpInsn(IF_ICMPGT, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71278_l", "()Z", false);
mv.visitJumpInsn(IFEQ, l1);
mv.visitIntInsn(SIPUSH, -192);
mv.visitVarInsn(ISTORE, 12);
Label l2 = new Label();
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 12);
mv.visitIntInsn(SIPUSH, 192);
Label l3 = new Label();
mv.visitJumpInsn(IF_ICMPGT, l3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71278_l", "()Z", false);
mv.visitJumpInsn(IFEQ, l3);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitVarInsn(LSTORE, 13);
mv.visitVarInsn(LLOAD, 13);
mv.visitVarInsn(LLOAD, 9);
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(1000L));
mv.visitInsn(LCMP);
Label l4 = new Label();
mv.visitJumpInsn(IFLE, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("Preparing spawn area");
mv.visitVarInsn(ILOAD, 5);
mv.visitIntInsn(BIPUSH, 100);
mv.visitInsn(IMUL);
mv.visitIntInsn(SIPUSH, 625);
mv.visitInsn(IDIV);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71216_a_", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(LLOAD, 13);
mv.visitVarInsn(LSTORE, 9);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.LONG}, 0, null);
mv.visitIincInsn(5, 1);
mv.visitVarInsn(ALOAD, 7);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "field_73059_b", "Lnet/minecraft/world/gen/ChunkProviderServer;");
mv.visitVarInsn(ALOAD, 8);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/ChunkCoordinates", "field_71574_a", "I");
mv.visitVarInsn(ILOAD, 11);
mv.visitInsn(IADD);
mv.visitInsn(ICONST_4);
mv.visitInsn(ISHR);
mv.visitVarInsn(ALOAD, 8);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/ChunkCoordinates", "field_71573_c", "I");
mv.visitVarInsn(ILOAD, 12);
mv.visitInsn(IADD);
mv.visitInsn(ICONST_4);
mv.visitInsn(ISHR);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/gen/ChunkProviderServer", "func_73158_c", "(II)Lnet/minecraft/world/chunk/Chunk;", false);
mv.visitInsn(POP);
mv.visitIincInsn(12, 16);
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitIincInsn(11, 16);
mv.visitJumpInsn(GOTO, l0);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71243_i", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 15);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_71225_e", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_71265_f", "()Lnet/minecraft/world/WorldSettings$GameType;", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_147135_j", "()Lnet/minecraft/world/EnumDifficulty;", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_71199_h", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_110455_j", "()I", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_152363_m", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71216_a_", "(Ljava/lang/String;I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71302_d", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71303_e", "I");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(": ");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("%");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71243_i", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71302_d", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71303_e", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71267_a", "(Z)V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "net/minecraft/world/MinecraftException");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71290_O", "Z");
Label l3 = new Label();
mv.visitJumpInsn(IFNE, l3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l4 = new Label();
mv.visitJumpInsn(IFNONNULL, l4);
mv.visitInsn(RETURN);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"[Lnet/minecraft/world/WorldServer;"}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 3);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 4);
Label l5 = new Label();
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ILOAD, 3);
mv.visitJumpInsn(IF_ICMPGE, l3);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 4);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 5);
Label l6 = new Label();
mv.visitJumpInsn(IFNULL, l6);
mv.visitVarInsn(ILOAD, 1);
mv.visitJumpInsn(IFNE, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("Saving chunks for level '");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72912_H", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76065_j", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("'/");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "field_73011_w", "Lnet/minecraft/world/WorldProvider;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldProvider", "func_80007_l", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitVarInsn(ALOAD, 5);
mv.visitInsn(ICONST_1);
mv.visitInsn(ACONST_NULL);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/IProgressUpdate");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_73044_a", "(ZLnet/minecraft/util/IProgressUpdate;)V", false);
mv.visitLabel(l1);
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/MinecraftException"});
mv.visitVarInsn(ASTORE, 6);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/MinecraftException", "getMessage", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;)V", true);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitIincInsn(4, 1);
mv.visitJumpInsn(GOTO, l5);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71260_j", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71290_O", "Z");
Label l0 = new Label();
mv.visitJumpInsn(IFNE, l0);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/Loader", "instance", "()Lcpw/mods/fml/common/Loader;", false);
mv.visitFieldInsn(GETSTATIC, "cpw/mods/fml/common/LoaderState", "SERVER_STARTED", "Lcpw/mods/fml/common/LoaderState;");
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/Loader", "hasReachedState", "(Lcpw/mods/fml/common/LoaderState;)Z", false);
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitJumpInsn(IFNE, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Stopping server");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", false);
Label l1 = new Label();
mv.visitJumpInsn(IFNULL, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/NetworkSystem", "func_151268_b", "()V", false);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
Label l2 = new Label();
mv.visitJumpInsn(IFNULL, l2);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Saving players");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72389_g", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72392_r", "()V", false);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
Label l3 = new Label();
mv.visitJumpInsn(IFNULL, l3);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Saving worlds");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71267_a", "(Z)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
Label l4 = new Label();
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
Label l5 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 1);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 2);
mv.visitFieldInsn(GETSTATIC, "net/minecraftforge/common/MinecraftForge", "EVENT_BUS", "Lcpw/mods/fml/common/eventhandler/EventBus;");
mv.visitTypeInsn(NEW, "net/minecraftforge/event/world/WorldEvent$Unload");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraftforge/event/world/WorldEvent$Unload", "<init>", "(Lnet/minecraft/world/World;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/eventhandler/EventBus", "post", "(Lcpw/mods/fml/common/eventhandler/Event;)Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_73041_k", "()V", false);
mv.visitIincInsn(1, 1);
mv.visitJumpInsn(GOTO, l4);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ASTORE, 1);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 3);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 4);
Label l6 = new Label();
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_FULL, 5, new Object[] {"net/minecraft/server/MinecraftServer", "[Lnet/minecraft/world/WorldServer;", "[Lnet/minecraft/world/WorldServer;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ILOAD, 3);
mv.visitJumpInsn(IF_ICMPGE, l3);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 4);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "field_73011_w", "Lnet/minecraft/world/WorldProvider;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldProvider", "field_76574_g", "I");
mv.visitInsn(ACONST_NULL);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "setWorld", "(ILnet/minecraft/world/WorldServer;)V", false);
mv.visitIincInsn(4, 1);
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"net/minecraft/server/MinecraftServer"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_76468_d", "()Z", false);
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_76470_e", "()V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71278_l", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71317_u", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71263_m", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71317_u", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "run", "()V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Throwable");
Label l3 = new Label();
mv.visitTryCatchBlock(l0, l1, l3, null);
Label l4 = new Label();
mv.visitTryCatchBlock(l2, l4, l3, null);
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l5, l3, null);
Label l6 = new Label();
Label l7 = new Label();
mv.visitTryCatchBlock(l6, l0, l7, "cpw/mods/fml/common/StartupQuery$AbortedException");
Label l8 = new Label();
Label l9 = new Label();
Label l10 = new Label();
mv.visitTryCatchBlock(l8, l9, l10, "java/lang/Throwable");
Label l11 = new Label();
mv.visitTryCatchBlock(l8, l9, l11, null);
Label l12 = new Label();
mv.visitTryCatchBlock(l10, l12, l11, null);
Label l13 = new Label();
mv.visitTryCatchBlock(l11, l13, l11, null);
Label l14 = new Label();
mv.visitTryCatchBlock(l6, l0, l14, "java/lang/Throwable");
Label l15 = new Label();
Label l16 = new Label();
Label l17 = new Label();
mv.visitTryCatchBlock(l15, l16, l17, "java/lang/Throwable");
Label l18 = new Label();
mv.visitTryCatchBlock(l15, l16, l18, null);
Label l19 = new Label();
mv.visitTryCatchBlock(l17, l19, l18, null);
Label l20 = new Label();
mv.visitTryCatchBlock(l18, l20, l18, null);
Label l21 = new Label();
mv.visitTryCatchBlock(l6, l0, l21, null);
mv.visitTryCatchBlock(l7, l8, l21, null);
mv.visitTryCatchBlock(l14, l15, l21, null);
Label l22 = new Label();
Label l23 = new Label();
Label l24 = new Label();
mv.visitTryCatchBlock(l22, l23, l24, "java/lang/Throwable");
Label l25 = new Label();
mv.visitTryCatchBlock(l22, l23, l25, null);
Label l26 = new Label();
mv.visitTryCatchBlock(l24, l26, l25, null);
Label l27 = new Label();
mv.visitTryCatchBlock(l25, l27, l25, null);
mv.visitTryCatchBlock(l21, l22, l21, null);
mv.visitLabel(l6);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71197_b", "()Z", false);
Label l28 = new Label();
mv.visitJumpInsn(IFEQ, l28);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStarted", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitVarInsn(LSTORE, 1);
mv.visitInsn(LCONST_0);
mv.visitVarInsn(LSTORE, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitTypeInsn(NEW, "net/minecraft/util/ChatComponentText");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71286_C", "Ljava/lang/String;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ChatComponentText", "<init>", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151315_a", "(Lnet/minecraft/util/IChatComponent;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier");
mv.visitInsn(DUP);
mv.visitLdcInsn("1.7.10");
mv.visitInsn(ICONST_5);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier", "<init>", "(Ljava/lang/String;I)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151321_a", "(Lnet/minecraft/network/ServerStatusResponse$MinecraftProtocolVersionIdentifier;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer", "func_147138_a", "(Lnet/minecraft/network/ServerStatusResponse;)V", false);
Label l29 = new Label();
mv.visitLabel(l29);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.LONG, Opcodes.LONG}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71317_u", "Z");
Label l30 = new Label();
mv.visitJumpInsn(IFEQ, l30);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitVarInsn(LSTORE, 5);
mv.visitVarInsn(LLOAD, 5);
mv.visitVarInsn(LLOAD, 1);
mv.visitInsn(LSUB);
mv.visitVarInsn(LSTORE, 7);
mv.visitVarInsn(LLOAD, 7);
mv.visitLdcInsn(new Long(2000L));
mv.visitInsn(LCMP);
Label l31 = new Label();
mv.visitJumpInsn(IFLE, l31);
mv.visitVarInsn(LLOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71299_R", "J");
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(15000L));
mv.visitInsn(LCMP);
mv.visitJumpInsn(IFLT, l31);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Can't keep up! Did the system time change, or is the server overloaded? Running {}ms behind, skipping {} tick(s)");
mv.visitInsn(ICONST_2);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(LLOAD, 7);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;", false);
mv.visitInsn(AASTORE);
mv.visitInsn(DUP);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(LLOAD, 7);
mv.visitLdcInsn(new Long(50L));
mv.visitInsn(LDIV);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;", false);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;[Ljava/lang/Object;)V", true);
mv.visitLdcInsn(new Long(2000L));
mv.visitVarInsn(LSTORE, 7);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71299_R", "J");
mv.visitLabel(l31);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.LONG, Opcodes.LONG}, 0, null);
mv.visitVarInsn(LLOAD, 7);
mv.visitInsn(LCONST_0);
mv.visitInsn(LCMP);
Label l32 = new Label();
mv.visitJumpInsn(IFGE, l32);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Time ran backwards! Did the system time change?");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;)V", true);
mv.visitInsn(LCONST_0);
mv.visitVarInsn(LSTORE, 7);
mv.visitLabel(l32);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(LLOAD, 3);
mv.visitVarInsn(LLOAD, 7);
mv.visitInsn(LADD);
mv.visitVarInsn(LSTORE, 3);
mv.visitVarInsn(LLOAD, 5);
mv.visitVarInsn(LSTORE, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_73056_e", "()Z", false);
Label l33 = new Label();
mv.visitJumpInsn(IFEQ, l33);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71217_p", "()V", false);
mv.visitInsn(LCONST_0);
mv.visitVarInsn(LSTORE, 3);
Label l34 = new Label();
mv.visitJumpInsn(GOTO, l34);
mv.visitLabel(l33);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(LLOAD, 3);
mv.visitLdcInsn(new Long(50L));
mv.visitInsn(LCMP);
mv.visitJumpInsn(IFLE, l34);
mv.visitVarInsn(LLOAD, 3);
mv.visitLdcInsn(new Long(50L));
mv.visitInsn(LSUB);
mv.visitVarInsn(LSTORE, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71217_p", "()V", false);
mv.visitJumpInsn(GOTO, l33);
mv.visitLabel(l34);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(LCONST_1);
mv.visitLdcInsn(new Long(50L));
mv.visitVarInsn(LLOAD, 3);
mv.visitInsn(LSUB);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "max", "(JJ)J", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "sleep", "(J)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71296_Q", "Z");
mv.visitJumpInsn(GOTO, l29);
mv.visitLabel(l30);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopping", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitJumpInsn(GOTO, l0);
mv.visitLabel(l28);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ACONST_NULL);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/crash/CrashReport");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71228_a", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71260_j", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitLabel(l1);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
Label l35 = new Label();
mv.visitJumpInsn(GOTO, l35);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 1);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l4);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitJumpInsn(GOTO, l35);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 9);
mv.visitLabel(l5);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitInsn(ATHROW);
mv.visitLabel(l35);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
Label l36 = new Label();
mv.visitJumpInsn(GOTO, l36);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"cpw/mods/fml/common/StartupQuery$AbortedException"});
mv.visitVarInsn(ASTORE, 1);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitLabel(l8);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71260_j", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitLabel(l9);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
Label l37 = new Label();
mv.visitJumpInsn(GOTO, l37);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 1);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l12);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitJumpInsn(GOTO, l37);
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 10);
mv.visitLabel(l13);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitVarInsn(ALOAD, 10);
mv.visitInsn(ATHROW);
mv.visitLabel(l37);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitJumpInsn(GOTO, l36);
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 1);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Encountered an unexpected exception");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(INSTANCEOF, "net/minecraft/util/ReportedException");
Label l38 = new Label();
mv.visitJumpInsn(IFEQ, l38);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/ReportedException");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/ReportedException", "func_71575_a", "()Lnet/minecraft/crash/CrashReport;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71230_b", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 2);
Label l39 = new Label();
mv.visitJumpInsn(GOTO, l39);
mv.visitLabel(l38);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"java/lang/Throwable", "net/minecraft/crash/CrashReport"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/crash/CrashReport");
mv.visitInsn(DUP);
mv.visitLdcInsn("Exception in server tick loop");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/crash/CrashReport", "<init>", "(Ljava/lang/String;Ljava/lang/Throwable;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71230_b", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitLabel(l39);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71238_n", "()Ljava/io/File;", false);
mv.visitLdcInsn("crash-reports");
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/io/File;Ljava/lang/String;)V", false);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("crash-");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitTypeInsn(NEW, "java/text/SimpleDateFormat");
mv.visitInsn(DUP);
mv.visitLdcInsn("yyyy-MM-dd_HH.mm.ss");
mv.visitMethodInsn(INVOKESPECIAL, "java/text/SimpleDateFormat", "<init>", "(Ljava/lang/String;)V", false);
mv.visitTypeInsn(NEW, "java/util/Date");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/Date", "<init>", "()V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/text/SimpleDateFormat", "format", "(Ljava/util/Date;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("-server.txt");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/io/File;Ljava/lang/String;)V", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "func_147149_a", "(Ljava/io/File;)Z", false);
Label l40 = new Label();
mv.visitJumpInsn(IFEQ, l40);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("This crash report has been saved to: ");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/File", "getAbsolutePath", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;)V", true);
Label l41 = new Label();
mv.visitJumpInsn(GOTO, l41);
mv.visitLabel(l40);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/io/File"}, 0, null);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("We were unable to save this crash report to disk.");
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;)V", true);
mv.visitLabel(l41);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "expectServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71228_a", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitLabel(l15);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71260_j", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitLabel(l16);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
Label l42 = new Label();
mv.visitJumpInsn(GOTO, l42);
mv.visitLabel(l17);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"net/minecraft/server/MinecraftServer"}, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 1);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l19);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitJumpInsn(GOTO, l42);
mv.visitLabel(l18);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitLabel(l20);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitVarInsn(ALOAD, 11);
mv.visitInsn(ATHROW);
mv.visitLabel(l42);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitJumpInsn(GOTO, l36);
mv.visitLabel(l21);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 12);
mv.visitLabel(l22);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71260_j", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitLabel(l23);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
Label l43 = new Label();
mv.visitJumpInsn(GOTO, l43);
mv.visitLabel(l24);
mv.visitFrame(Opcodes.F_FULL, 13, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, "java/lang/Throwable"}, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 13);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Exception stopping the server");
mv.visitVarInsn(ALOAD, 13);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l26);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitJumpInsn(GOTO, l43);
mv.visitLabel(l25);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 14);
mv.visitLabel(l27);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "handleServerStopped", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71240_o", "()V", false);
mv.visitVarInsn(ALOAD, 14);
mv.visitInsn(ATHROW);
mv.visitLabel(l43);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 12);
mv.visitInsn(ATHROW);
mv.visitLabel(l36);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"net/minecraft/server/MinecraftServer"}, 0, new Object[] {});
mv.visitInsn(RETURN);
mv.visitMaxs(9, 15);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PRIVATE, "func_147138_a", "(Lnet/minecraft/network/ServerStatusResponse;)V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
Label l3 = new Label();
mv.visitTryCatchBlock(l0, l1, l3, null);
Label l4 = new Label();
mv.visitTryCatchBlock(l2, l4, l3, null);
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l5, l3, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("server-icon.png");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71209_f", "(Ljava/lang/String;)Ljava/io/File;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/File", "isFile", "()Z", false);
Label l6 = new Label();
mv.visitJumpInsn(IFEQ, l6);
mv.visitMethodInsn(INVOKESTATIC, "io/netty/buffer/Unpooled", "buffer", "()Lio/netty/buffer/ByteBuf;", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitLabel(l0);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "javax/imageio/ImageIO", "read", "(Ljava/io/File;)Ljava/awt/image/BufferedImage;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/awt/image/BufferedImage", "getWidth", "()I", false);
mv.visitIntInsn(BIPUSH, 64);
Label l7 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l7);
mv.visitInsn(ICONST_1);
Label l8 = new Label();
mv.visitJumpInsn(GOTO, l8);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {"java/io/File", "io/netty/buffer/ByteBuf", "java/awt/image/BufferedImage"}, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitLdcInsn("Must be 64 pixels wide");
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitMethodInsn(INVOKESTATIC, "org/apache/commons/lang3/Validate", "validState", "(ZLjava/lang/String;[Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/awt/image/BufferedImage", "getHeight", "()I", false);
mv.visitIntInsn(BIPUSH, 64);
Label l9 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l9);
mv.visitInsn(ICONST_1);
Label l10 = new Label();
mv.visitJumpInsn(GOTO, l10);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitLdcInsn("Must be 64 pixels high");
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitMethodInsn(INVOKESTATIC, "org/apache/commons/lang3/Validate", "validState", "(ZLjava/lang/String;[Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitLdcInsn("PNG");
mv.visitTypeInsn(NEW, "io/netty/buffer/ByteBufOutputStream");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESPECIAL, "io/netty/buffer/ByteBufOutputStream", "<init>", "(Lio/netty/buffer/ByteBuf;)V", false);
mv.visitMethodInsn(INVOKESTATIC, "javax/imageio/ImageIO", "write", "(Ljava/awt/image/RenderedImage;Ljava/lang/String;Ljava/io/OutputStream;)Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESTATIC, "io/netty/handler/codec/base64/Base64", "encode", "(Lio/netty/buffer/ByteBuf;)Lio/netty/buffer/ByteBuf;", false);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("data:image/png;base64,");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETSTATIC, "com/google/common/base/Charsets", "UTF_8", "Ljava/nio/charset/Charset;");
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "toString", "(Ljava/nio/charset/Charset;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151320_a", "(Ljava/lang/String;)V", false);
mv.visitLabel(l1);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "release", "()Z", false);
mv.visitInsn(POP);
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_FULL, 4, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/network/ServerStatusResponse", "java/io/File", "io/netty/buffer/ByteBuf"}, 1, new Object[] {"java/lang/Exception"});
mv.visitVarInsn(ASTORE, 4);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Couldn't load server icon");
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l4);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "release", "()Z", false);
mv.visitInsn(POP);
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 6);
mv.visitLabel(l5);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "io/netty/buffer/ByteBuf", "release", "()Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 6);
mv.visitInsn(ATHROW);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71238_n", "()Ljava/io/File;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitLdcInsn(".");
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/lang/String;)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71228_a", "(Lnet/minecraft/crash/CrashReport;)V", null, null);
mv.visitCode();
mv.visitInsn(RETURN);
mv.visitMaxs(0, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71240_o", "()V", null, null);
mv.visitCode();
mv.visitInsn(RETURN);
mv.visitMaxs(0, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71217_p", "()V", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LSTORE, 1);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPreServerTick", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(DUP);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71295_T", "Z");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71295_T", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/profiler/Profiler", "field_76327_a", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76317_a", "()V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.LONG}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("root");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71190_q", "()V", false);
mv.visitVarInsn(LLOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(5000000000L));
mv.visitInsn(LCMP);
Label l1 = new Label();
mv.visitJumpInsn(IFLT, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitTypeInsn(NEW, "net/minecraft/network/ServerStatusResponse$PlayerCountData");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71275_y", "()I", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71233_x", "()I", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/ServerStatusResponse$PlayerCountData", "<init>", "(II)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151319_a", "(Lnet/minecraft/network/ServerStatusResponse$PlayerCountData;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71233_x", "()I", false);
mv.visitIntInsn(BIPUSH, 12);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "min", "(II)I", false);
mv.visitTypeInsn(ANEWARRAY, "com/mojang/authlib/GameProfile");
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147146_q", "Ljava/util/Random;");
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71233_x", "()I", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARRAYLENGTH);
mv.visitInsn(ISUB);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/MathHelper", "func_76136_a", "(Ljava/util/Random;II)I", false);
mv.visitVarInsn(ISTORE, 4);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 5);
Label l2 = new Label();
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {"[Lcom/mojang/authlib/GameProfile;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 5);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARRAYLENGTH);
Label l3 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l3);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ILOAD, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/management/ServerConfigurationManager", "field_72404_b", "Ljava/util/List;");
mv.visitVarInsn(ILOAD, 4);
mv.visitVarInsn(ILOAD, 5);
mv.visitInsn(IADD);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/entity/player/EntityPlayerMP");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/player/EntityPlayerMP", "func_146103_bH", "()Lcom/mojang/authlib/GameProfile;", false);
mv.visitInsn(AASTORE);
mv.visitIincInsn(5, 1);
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESTATIC, "java/util/Arrays", "asList", "([Ljava/lang/Object;)Ljava/util/List;", false);
mv.visitMethodInsn(INVOKESTATIC, "java/util/Collections", "shuffle", "(Ljava/util/List;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse", "func_151318_b", "()Lnet/minecraft/network/ServerStatusResponse$PlayerCountData;", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/ServerStatusResponse$PlayerCountData", "func_151330_a", "([Lcom/mojang/authlib/GameProfile;)V", false);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitIntInsn(SIPUSH, 900);
mv.visitInsn(IREM);
Label l4 = new Label();
mv.visitJumpInsn(IFNE, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("save");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72389_g", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71267_a", "(Z)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tallying");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71311_j", "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitIntInsn(BIPUSH, 100);
mv.visitInsn(IREM);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LLOAD, 1);
mv.visitInsn(LSUB);
mv.visitInsn(LASTORE);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("snooper");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_76468_d", "()Z", false);
Label l5 = new Label();
mv.visitJumpInsn(IFNE, l5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitIntInsn(BIPUSH, 100);
mv.visitJumpInsn(IF_ICMPLE, l5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_76463_a", "()V", false);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitIntInsn(SIPUSH, 6000);
mv.visitInsn(IREM);
Label l6 = new Label();
mv.visitJumpInsn(IFNE, l6);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_76471_b", "()V", false);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPostServerTick", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(6, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71190_q", "()V", null, null);
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Throwable");
Label l3 = new Label();
Label l4 = new Label();
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l4, l5, "java/lang/Throwable");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("levels");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/chunkio/ChunkIOExecutor", "tick", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitIntInsn(SIPUSH, 200);
mv.visitInsn(IREM);
Label l6 = new Label();
mv.visitJumpInsn(IFNE, l6);
mv.visitInsn(ICONST_1);
Label l7 = new Label();
mv.visitJumpInsn(GOTO, l7);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getIDs", "(Z)[Ljava/lang/Integer;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l8 = new Label();
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {Opcodes.TOP, "[Ljava/lang/Integer;", Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARRAYLENGTH);
Label l9 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l9);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
mv.visitVarInsn(ISTORE, 4);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LSTORE, 5);
mv.visitVarInsn(ILOAD, 4);
Label l10 = new Label();
mv.visitJumpInsn(IFEQ, l10);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71255_r", "()Z", false);
Label l11 = new Label();
mv.visitJumpInsn(IFEQ, l11);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.LONG}, 0, null);
mv.visitVarInsn(ILOAD, 4);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorld", "(I)Lnet/minecraft/world/WorldServer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getWorlds", "()Ljava/util/Collection;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Collection", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 7);
Label l12 = new Label();
mv.visitLabel(l12);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/util/Iterator"}, 0, null);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
mv.visitJumpInsn(IFEQ, l11);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/world/World");
mv.visitVarInsn(ASTORE, 8);
mv.visitVarInsn(ALOAD, 8);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/world/WorldServer");
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72912_H", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76065_j", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("pools");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitIntInsn(BIPUSH, 20);
mv.visitInsn(IREM);
Label l13 = new Label();
mv.visitJumpInsn(IFNE, l13);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("timeSync");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitTypeInsn(NEW, "net/minecraft/network/play/server/S03PacketTimeUpdate");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_82737_E", "()J", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72820_D", "()J", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_82736_K", "()Lnet/minecraft/world/GameRules;", false);
mv.visitLdcInsn("doDaylightCycle");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/GameRules", "func_82766_b", "(Ljava/lang/String;)Z", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S03PacketTimeUpdate", "<init>", "(JJZ)V", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "field_73011_w", "Lnet/minecraft/world/WorldProvider;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldProvider", "field_76574_g", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_148537_a", "(Lnet/minecraft/network/Packet;I)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitLabel(l13);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/world/World", "net/minecraft/world/WorldServer"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tick");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPreWorldTick", "(Lnet/minecraft/world/World;)V", false);
mv.visitLabel(l0);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72835_b", "()V", false);
mv.visitLabel(l1);
mv.visitJumpInsn(GOTO, l3);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitVarInsn(ALOAD, 11);
mv.visitLdcInsn("Exception ticking world");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/crash/CrashReport", "func_85055_a", "(Ljava/lang/Throwable;Ljava/lang/String;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 10);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72914_a", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitInsn(POP);
mv.visitTypeInsn(NEW, "net/minecraft/util/ReportedException");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ReportedException", "<init>", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitInsn(ATHROW);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72939_s", "()V", false);
mv.visitLabel(l4);
Label l14 = new Label();
mv.visitJumpInsn(GOTO, l14);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Throwable"});
mv.visitVarInsn(ASTORE, 11);
mv.visitVarInsn(ALOAD, 11);
mv.visitLdcInsn("Exception ticking world entities");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/crash/CrashReport", "func_85055_a", "(Ljava/lang/Throwable;Ljava/lang/String;)Lnet/minecraft/crash/CrashReport;", false);
mv.visitVarInsn(ASTORE, 10);
mv.visitVarInsn(ALOAD, 9);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72914_a", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitInsn(POP);
mv.visitTypeInsn(NEW, "net/minecraft/util/ReportedException");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ReportedException", "<init>", "(Lnet/minecraft/crash/CrashReport;)V", false);
mv.visitInsn(ATHROW);
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "onPostWorldTick", "(Lnet/minecraft/world/World;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tracker");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76320_a", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_73039_n", "()Lnet/minecraft/entity/EntityTracker;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/EntityTracker", "func_72788_a", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitVarInsn(ILOAD, 4);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", false);
mv.visitTypeInsn(CHECKCAST, "[J");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitIntInsn(BIPUSH, 100);
mv.visitInsn(IREM);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "nanoTime", "()J", false);
mv.visitVarInsn(LLOAD, 5);
mv.visitInsn(LSUB);
mv.visitInsn(LASTORE);
mv.visitIincInsn(3, 1);
mv.visitJumpInsn(GOTO, l8);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("dim_unloading");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76318_c", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "worldTickTimes", "Ljava/util/Hashtable;");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "unloadWorlds", "(Ljava/util/Hashtable;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("connection");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76318_c", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/NetworkSystem", "func_151269_c", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("players");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76318_c", "(Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72374_b", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitLdcInsn("tickables");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76318_c", "(Ljava/lang/String;)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
Label l15 = new Label();
mv.visitLabel(l15);
mv.visitFrame(Opcodes.F_FULL, 3, new Object[] {"net/minecraft/server/MinecraftServer", Opcodes.INTEGER, "[Ljava/lang/Integer;"}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71322_p", "Ljava/util/List;");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
Label l16 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l16);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71322_p", "Ljava/util/List;");
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/server/gui/IUpdatePlayerListBox");
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/server/gui/IUpdatePlayerListBox", "func_73660_a", "()V", true);
mv.visitIincInsn(1, 1);
mv.visitJumpInsn(GOTO, l15);
mv.visitLabel(l16);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71304_b", "Lnet/minecraft/profiler/Profiler;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "func_76319_b", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(9, 12);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71255_r", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71256_s", "()V", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/StartupQuery", "reset", "()V", false);
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$2");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("Server thread");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$2", "<init>", "(Lnet/minecraft/server/MinecraftServer;Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer$2", "start", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71209_f", "(Ljava/lang/String;)Ljava/io/File;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71238_n", "()Ljava/io/File;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/io/File;Ljava/lang/String;)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(4, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71236_h", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "warn", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71218_a", "(I)Lnet/minecraft/world/WorldServer;", null, null);
mv.visitCode();
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorld", "(I)Lnet/minecraft/world/WorldServer;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "initDimension", "(I)V", false);
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorld", "(I)Lnet/minecraft/world/WorldServer;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71249_w", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitLdcInsn("1.7.10");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71233_x", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72394_k", "()I", false);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71275_y", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72352_l", "()I", false);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71213_z", "()[Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72369_d", "()[Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152357_F", "()[Lcom/mojang/authlib/GameProfile;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_152600_g", "()[Lcom/mojang/authlib/GameProfile;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getServerModName", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "cpw/mods/fml/common/FMLCommonHandler", "instance", "()Lcpw/mods/fml/common/FMLCommonHandler;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/FMLCommonHandler", "getModName", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71230_b", "(Lnet/minecraft/crash/CrashReport;)Lnet/minecraft/crash/CrashReport;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "func_85056_g", "()Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitLdcInsn("Profiler Position");
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$3");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$3", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
mv.visitJumpInsn(IFLE, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "func_85056_g", "()Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitLdcInsn("Vec3 Pool Size");
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$4");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$4", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
Label l1 = new Label();
mv.visitJumpInsn(IFNULL, l1);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "func_85056_g", "()Lnet/minecraft/crash/CrashReportCategory;", false);
mv.visitLdcInsn("Player Count");
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$5");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$5", "<init>", "(Lnet/minecraft/server/MinecraftServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "func_71500_a", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ARETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71248_a", "(Lnet/minecraft/command/ICommandSender;Ljava/lang/String;)Ljava/util/List;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/util/ArrayList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitLdcInsn("/");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "startsWith", "(Ljava/lang/String;)Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "substring", "(I)Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
mv.visitLdcInsn(" ");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "contains", "(Ljava/lang/CharSequence;)Z", false);
Label l1 = new Label();
mv.visitJumpInsn(IFNE, l1);
mv.visitInsn(ICONST_1);
Label l2 = new Label();
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/util/ArrayList"}, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitVarInsn(ISTORE, 4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71321_q", "Lnet/minecraft/command/ICommandManager;");
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/command/ICommandManager", "func_71558_b", "(Lnet/minecraft/command/ICommandSender;Ljava/lang/String;)Ljava/util/List;", true);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 5);
Label l3 = new Label();
mv.visitJumpInsn(IFNULL, l3);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 6);
Label l4 = new Label();
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_APPEND,3, new Object[] {Opcodes.INTEGER, "java/util/List", "java/util/Iterator"}, 0, null);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
mv.visitJumpInsn(IFEQ, l3);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "java/lang/String");
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ILOAD, 4);
Label l5 = new Label();
mv.visitJumpInsn(IFEQ, l5);
mv.visitVarInsn(ALOAD, 3);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("/");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/ArrayList", "add", "(Ljava/lang/Object;)Z", false);
mv.visitInsn(POP);
Label l6 = new Label();
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/lang/String"}, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/ArrayList", "add", "(Ljava/lang/Object;)Z", false);
mv.visitInsn(POP);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitJumpInsn(GOTO, l4);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARETURN);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitLdcInsn(" ");
mv.visitInsn(ICONST_M1);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "split", "(Ljava/lang/String;I)[Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitInsn(ARRAYLENGTH);
mv.visitInsn(ICONST_1);
mv.visitInsn(ISUB);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72369_d", "()[Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 6);
mv.visitVarInsn(ALOAD, 6);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 7);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 8);
Label l7 = new Label();
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_FULL, 9, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/command/ICommandSender", "java/lang/String", "java/util/ArrayList", "[Ljava/lang/String;", "java/lang/String", "[Ljava/lang/String;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 8);
mv.visitVarInsn(ILOAD, 7);
Label l8 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l8);
mv.visitVarInsn(ALOAD, 6);
mv.visitVarInsn(ILOAD, 8);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ALOAD, 5);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/command/CommandBase", "func_71523_a", "(Ljava/lang/String;Ljava/lang/String;)Z", false);
Label l9 = new Label();
mv.visitJumpInsn(IFEQ, l9);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/ArrayList", "add", "(Ljava/lang/Object;)Z", false);
mv.visitInsn(POP);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitIincInsn(8, 1);
mv.visitJumpInsn(GOTO, l7);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 10);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "func_71276_C", "()Lnet/minecraft/server/MinecraftServer;", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_71309_l", "Lnet/minecraft/server/MinecraftServer;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_70005_c_", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitLdcInsn("Server");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_145747_a", "(Lnet/minecraft/util/IChatComponent;)V", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/util/IChatComponent", "func_150260_c", "()Ljava/lang/String;", true);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_70003_b", "(ILjava/lang/String;)Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71187_D", "()Lnet/minecraft/command/ICommandManager;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71321_q", "Lnet/minecraft/command/ICommandManager;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71250_E", "()Ljava/security/KeyPair;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71292_I", "Ljava/security/KeyPair;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71214_G", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71293_J", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71224_l", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71293_J", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71264_H", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71293_J", "Ljava/lang/String;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitInsn(ICONST_1);
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {Opcodes.INTEGER});
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71270_I", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71294_K", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71261_m", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71294_K", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71246_n", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71287_L", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71221_J", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71287_L", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71253_a", "(Ljava/security/KeyPair;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71292_I", "Ljava/security/KeyPair;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147139_a", "(Lnet/minecraft/world/EnumDifficulty;)V", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 2);
Label l0 = new Label();
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
Label l1 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 2);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 3);
Label l2 = new Label();
mv.visitJumpInsn(IFNULL, l2);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72912_H", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76093_s", "()Z", false);
Label l3 = new Label();
mv.visitJumpInsn(IFEQ, l3);
mv.visitVarInsn(ALOAD, 3);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/EnumDifficulty", "HARD", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/WorldServer", "field_73013_u", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitVarInsn(ALOAD, 3);
mv.visitInsn(ICONST_1);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72891_a", "(ZZ)V", false);
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/world/WorldServer"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71264_H", "()Z", false);
Label l4 = new Label();
mv.visitJumpInsn(IFEQ, l4);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/WorldServer", "field_73013_u", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 3);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "field_73013_u", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/EnumDifficulty", "PEACEFUL", "Lnet/minecraft/world/EnumDifficulty;");
Label l5 = new Label();
mv.visitJumpInsn(IF_ACMPEQ, l5);
mv.visitInsn(ICONST_1);
Label l6 = new Label();
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/WorldServer"});
mv.visitInsn(ICONST_0);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_FULL, 4, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/world/EnumDifficulty", Opcodes.INTEGER, "net/minecraft/world/WorldServer"}, 2, new Object[] {"net/minecraft/world/WorldServer", Opcodes.INTEGER});
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72891_a", "(ZZ)V", false);
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/WorldServer", "field_73013_u", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71193_K", "()Z", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71324_y", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72891_a", "(ZZ)V", false);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitIincInsn(2, 1);
mv.visitJumpInsn(GOTO, l0);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 4);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "func_71193_K", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71242_L", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71288_M", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71204_b", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71288_M", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71194_c", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71289_N", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71254_M", "()Lnet/minecraft/world/storage/ISaveFormat;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71310_m", "Lnet/minecraft/world/storage/ISaveFormat;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71272_O", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71290_O", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71254_M", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "func_75800_d", "()V", true);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
Label l0 = new Label();
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
Label l1 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 1);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 2);
Label l2 = new Label();
mv.visitJumpInsn(IFNULL, l2);
mv.visitFieldInsn(GETSTATIC, "net/minecraftforge/common/MinecraftForge", "EVENT_BUS", "Lcpw/mods/fml/common/eventhandler/EventBus;");
mv.visitTypeInsn(NEW, "net/minecraftforge/event/world/WorldEvent$Unload");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraftforge/event/world/WorldEvent$Unload", "<init>", "(Lnet/minecraft/world/World;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "cpw/mods/fml/common/eventhandler/EventBus", "post", "(Lcpw/mods/fml/common/eventhandler/Event;)Z", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_73041_k", "()V", false);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitIincInsn(1, 1);
mv.visitJumpInsn(GOTO, l0);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71254_M", "()Lnet/minecraft/world/storage/ISaveFormat;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72860_G", "()Lnet/minecraft/world/storage/ISaveHandler;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveHandler", "func_75760_g", "()Ljava/lang/String;", true);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/storage/ISaveFormat", "func_75802_e", "(Ljava/lang/String;)Z", true);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71263_m", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147133_T", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_70000_a", "(Lnet/minecraft/profiler/PlayerUsageSnooper;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("whitelist_enabled");
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("whitelist_count");
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("players_current");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71233_x", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("players_max");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71275_y", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("players_seen");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/management/ServerConfigurationManager", "func_72373_m", "()[Ljava/lang/String;", false);
mv.visitInsn(ARRAYLENGTH);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("uses_auth");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71325_x", "Z");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("gui_state");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71279_ae", "()Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitLdcInsn("enabled");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 2, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String"});
mv.visitLdcInsn("disabled");
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 3, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String", "java/lang/Object"});
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("run_time");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_130071_aq", "()J", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_130105_g", "()J", false);
mv.visitInsn(LSUB);
mv.visitLdcInsn(new Long(60L));
mv.visitInsn(LDIV);
mv.visitLdcInsn(new Long(1000L));
mv.visitInsn(LMUL);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("avg_tick_ms");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71311_j", "[J");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/MathHelper", "func_76127_a", "([J)D", false);
mv.visitLdcInsn(new Double("1.0E-6"));
mv.visitInsn(DMUL);
mv.visitInsn(D2I);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l2 = new Label();
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
Label l3 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l3);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 3);
mv.visitInsn(AALOAD);
Label l4 = new Label();
mv.visitJumpInsn(IFNULL, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 3);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72912_H", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][dimension]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "field_73011_w", "Lnet/minecraft/world/WorldProvider;");
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldProvider", "field_76574_g", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][mode]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76077_q", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][difficulty]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/WorldServer", "field_73013_u", "Lnet/minecraft/world/EnumDifficulty;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][hardcore]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76093_s", "()Z", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][generator_name]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76067_t", "()Lnet/minecraft/world/WorldType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "func_77127_a", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][generator_version]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76067_t", "()Lnet/minecraft/world/WorldType;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "func_77131_c", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][height]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71280_D", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("world[");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn("][chunks_loaded]");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72863_F", "()Lnet/minecraft/world/chunk/IChunkProvider;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/world/chunk/IChunkProvider", "func_73152_e", "()I", true);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitIincInsn(2, 1);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitIincInsn(3, 1);
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("worlds");
mv.visitVarInsn(ILOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152768_a", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(6, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_70001_b", "(Lnet/minecraft/profiler/PlayerUsageSnooper;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("singleplayer");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71264_H", "()Z", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("server_brand");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "getServerModName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("gui_supported");
mv.visitMethodInsn(INVOKESTATIC, "java/awt/GraphicsEnvironment", "isHeadless", "()Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitLdcInsn("headless");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 2, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String"});
mv.visitLdcInsn("supported");
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/server/MinecraftServer", "net/minecraft/profiler/PlayerUsageSnooper"}, 3, new Object[] {"net/minecraft/profiler/PlayerUsageSnooper", "java/lang/String", "java/lang/Object"});
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("dedicated");
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71262_S", "()Z", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/PlayerUsageSnooper", "func_152767_b", "(Ljava/lang/String;Ljava/lang/Object;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_70002_Q", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_71262_S", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71266_T", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71325_x", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71229_d", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71325_x", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71268_U", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71324_y", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71251_e", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71324_y", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71220_V", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71323_z", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71257_f", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71323_z", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71219_W", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71284_A", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71188_g", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71284_A", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71231_X", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71285_B", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71245_h", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71285_B", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_82356_Z", "()Z", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71273_Y", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71286_C", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71205_p", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71286_C", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71207_Z", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71280_D", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71191_d", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71280_D", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71203_ab", "()Lnet/minecraft/server/management/ServerConfigurationManager;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152361_a", "(Lnet/minecraft/server/management/ServerConfigurationManager;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71235_a", "(Lnet/minecraft/world/WorldSettings$GameType;)V", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 2);
Label l0 = new Label();
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ARRAYLENGTH);
Label l1 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l1);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "func_71276_C", "()Lnet/minecraft/server/MinecraftServer;", false);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitVarInsn(ILOAD, 2);
mv.visitInsn(AALOAD);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "func_72912_H", "()Lnet/minecraft/world/storage/WorldInfo;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/storage/WorldInfo", "func_76060_a", "(Lnet/minecraft/world/WorldSettings$GameType;)V", false);
mv.visitIincInsn(2, 1);
mv.visitJumpInsn(GOTO, l0);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147137_ag", "()Lnet/minecraft/network/NetworkSystem;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147144_o", "Lnet/minecraft/network/NetworkSystem;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71200_ad", "()Z", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71296_Q", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71279_ae", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "func_71206_a", "(Lnet/minecraft/world/WorldSettings$GameType;Z)Ljava/lang/String;", null, null);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71259_af", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71315_w", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71223_ag", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71295_T", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_80003_ah", "()Lnet/minecraft/profiler/PlayerUsageSnooper;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71307_n", "Lnet/minecraft/profiler/PlayerUsageSnooper;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82114_b", "()Lnet/minecraft/util/ChunkCoordinates;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/util/ChunkCoordinates");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitInsn(ICONST_0);
mv.visitInsn(ICONST_0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ChunkCoordinates", "<init>", "(III)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(5, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_130014_f_", "()Lnet/minecraft/world/World;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71305_c", "[Lnet/minecraft/world/WorldServer;");
mv.visitInsn(ICONST_0);
mv.visitInsn(AALOAD);
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82357_ak", "()I", null, null);
mv.visitCode();
mv.visitIntInsn(BIPUSH, 16);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_96290_a", "(Lnet/minecraft/world/World;IIILnet/minecraft/entity/player/EntityPlayer;)Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 6);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_104056_am", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_104057_T", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_110454_ao", "()Ljava/net/Proxy;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_110456_c", "Ljava/net/Proxy;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "func_130071_aq", "()J", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_143007_ar", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_143006_e", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_143008_E", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_145748_c_", "()Lnet/minecraft/util/IChatComponent;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/util/ChatComponentText");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_70005_c_", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ChatComponentText", "<init>", "(Ljava/lang/String;)V", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147136_ar", "()Z", null, null);
mv.visitCode();
mv.visitInsn(ICONST_1);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147130_as", "()Lcom/mojang/authlib/minecraft/MinecraftSessionService;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147143_S", "Lcom/mojang/authlib/minecraft/MinecraftSessionService;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152359_aw", "()Lcom/mojang/authlib/GameProfileRepository;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152365_W", "Lcom/mojang/authlib/GameProfileRepository;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_152358_ax", "()Lnet/minecraft/server/management/PlayerProfileCache;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_152366_X", "Lnet/minecraft/server/management/PlayerProfileCache;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147134_at", "()Lnet/minecraft/network/ServerStatusResponse;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_147147_p", "Lnet/minecraft/network/ServerStatusResponse;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_147132_au", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(LCONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147142_T", "J");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71211_k", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71320_r", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71189_e", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71320_r", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_82010_a", "(Lnet/minecraft/server/gui/IUpdatePlayerListBox;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71322_p", "Ljava/util/List;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "add", "(Ljava/lang/Object;)Z", true);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
Label l0 = new Label();
Label l1 = new Label();
Label l2 = new Label();
mv.visitTryCatchBlock(l0, l1, l2, "java/lang/NumberFormatException");
Label l3 = new Label();
Label l4 = new Label();
Label l5 = new Label();
mv.visitTryCatchBlock(l3, l4, l5, "java/lang/Exception");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/init/Bootstrap", "func_151354_b", "()V", false);
mv.visitLabel(l3);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 1);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 2);
mv.visitLdcInsn(".");
mv.visitVarInsn(ASTORE, 3);
mv.visitInsn(ACONST_NULL);
mv.visitVarInsn(ASTORE, 4);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 5);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 6);
mv.visitInsn(ICONST_M1);
mv.visitVarInsn(ISTORE, 7);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 8);
Label l6 = new Label();
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_FULL, 9, new Object[] {"[Ljava/lang/String;", Opcodes.INTEGER, "java/lang/String", "java/lang/String", "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 8);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ARRAYLENGTH);
Label l7 = new Label();
mv.visitJumpInsn(IF_ICMPGE, l7);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 8);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 9);
mv.visitVarInsn(ILOAD, 8);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ARRAYLENGTH);
mv.visitInsn(ICONST_1);
mv.visitInsn(ISUB);
Label l8 = new Label();
mv.visitJumpInsn(IF_ICMPNE, l8);
mv.visitInsn(ACONST_NULL);
Label l9 = new Label();
mv.visitJumpInsn(GOTO, l9);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"java/lang/String"}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 8);
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitInsn(AALOAD);
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/String"});
mv.visitVarInsn(ASTORE, 10);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("nogui");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l10 = new Label();
mv.visitJumpInsn(IFNE, l10);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--nogui");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
mv.visitJumpInsn(IFNE, l10);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--port");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l11 = new Label();
mv.visitJumpInsn(IFEQ, l11);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l11);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitLabel(l0);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "parseInt", "(Ljava/lang/String;)I", false);
mv.visitVarInsn(ISTORE, 7);
mv.visitLabel(l1);
Label l12 = new Label();
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_FULL, 12, new Object[] {"[Ljava/lang/String;", Opcodes.INTEGER, "java/lang/String", "java/lang/String", "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER, "java/lang/String", "java/lang/String", Opcodes.INTEGER}, 1, new Object[] {"java/lang/NumberFormatException"});
mv.visitVarInsn(ASTORE, 12);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--singleplayer");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l13 = new Label();
mv.visitJumpInsn(IFEQ, l13);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l13);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ASTORE, 2);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l13);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--universe");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l14 = new Label();
mv.visitJumpInsn(IFEQ, l14);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l14);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ASTORE, 3);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--world");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l15 = new Label();
mv.visitJumpInsn(IFEQ, l15);
mv.visitVarInsn(ALOAD, 10);
mv.visitJumpInsn(IFNULL, l15);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 11);
mv.visitVarInsn(ALOAD, 10);
mv.visitVarInsn(ASTORE, 4);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l15);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--demo");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
Label l16 = new Label();
mv.visitJumpInsn(IFEQ, l16);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 5);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l16);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 9);
mv.visitLdcInsn("--bonusChest");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
mv.visitJumpInsn(IFEQ, l12);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ISTORE, 6);
mv.visitJumpInsn(GOTO, l12);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 1);
mv.visitLabel(l12);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 11);
Label l17 = new Label();
mv.visitJumpInsn(IFEQ, l17);
mv.visitIincInsn(8, 1);
mv.visitLabel(l17);
mv.visitFrame(Opcodes.F_CHOP,3, null, 0, null);
mv.visitIincInsn(8, 1);
mv.visitJumpInsn(GOTO, l6);
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitTypeInsn(NEW, "net/minecraft/server/dedicated/DedicatedServer");
mv.visitInsn(DUP);
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/lang/String;)V", false);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/dedicated/DedicatedServer", "<init>", "(Ljava/io/File;)V", false);
mv.visitVarInsn(ASTORE, 8);
mv.visitVarInsn(ALOAD, 2);
Label l18 = new Label();
mv.visitJumpInsn(IFNULL, l18);
mv.visitVarInsn(ALOAD, 8);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "func_71224_l", "(Ljava/lang/String;)V", false);
mv.visitLabel(l18);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {"net/minecraft/server/dedicated/DedicatedServer"}, 0, null);
mv.visitVarInsn(ALOAD, 4);
Label l19 = new Label();
mv.visitJumpInsn(IFNULL, l19);
mv.visitVarInsn(ALOAD, 8);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "func_71261_m", "(Ljava/lang/String;)V", false);
mv.visitLabel(l19);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 7);
Label l20 = new Label();
mv.visitJumpInsn(IFLT, l20);
mv.visitVarInsn(ALOAD, 8);
mv.visitVarInsn(ILOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "func_71208_b", "(I)V", false);
mv.visitLabel(l20);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 5);
Label l21 = new Label();
mv.visitJumpInsn(IFEQ, l21);
mv.visitVarInsn(ALOAD, 8);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "func_71204_b", "(Z)V", false);
mv.visitLabel(l21);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 6);
Label l22 = new Label();
mv.visitJumpInsn(IFEQ, l22);
mv.visitVarInsn(ALOAD, 8);
mv.visitInsn(ICONST_1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "func_71194_c", "(Z)V", false);
mv.visitLabel(l22);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 1);
Label l23 = new Label();
mv.visitJumpInsn(IFEQ, l23);
mv.visitMethodInsn(INVOKESTATIC, "java/awt/GraphicsEnvironment", "isHeadless", "()Z", false);
mv.visitJumpInsn(IFNE, l23);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "func_120011_ar", "()V", false);
mv.visitLabel(l23);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/dedicated/DedicatedServer", "func_71256_s", "()V", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Runtime", "getRuntime", "()Ljava/lang/Runtime;", false);
mv.visitTypeInsn(NEW, "net/minecraft/server/MinecraftServer$6");
mv.visitInsn(DUP);
mv.visitLdcInsn("Server Shutdown Thread");
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/server/MinecraftServer$6", "<init>", "(Ljava/lang/String;Lnet/minecraft/server/dedicated/DedicatedServer;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Runtime", "addShutdownHook", "(Ljava/lang/Thread;)V", false);
mv.visitLabel(l4);
Label l24 = new Label();
mv.visitJumpInsn(GOTO, l24);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {"[Ljava/lang/String;"}, 1, new Object[] {"java/lang/Exception"});
mv.visitVarInsn(ASTORE, 1);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitLdcInsn("Failed to start the minecraft server");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "fatal", "(Ljava/lang/String;Ljava/lang/Throwable;)V", true);
mv.visitLabel(l24);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 13);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71244_g", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71277_t", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71320_r", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71234_u", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71319_s", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71274_v", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71286_C", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71258_A", "()Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitLdcInsn("");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71252_i", "(Ljava/lang/String;)Ljava/lang/String;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/network/rcon/RConConsoleSource", "field_70010_a", "Lnet/minecraft/network/rcon/RConConsoleSource;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/rcon/RConConsoleSource", "func_70007_b", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71321_q", "Lnet/minecraft/command/ICommandManager;");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/network/rcon/RConConsoleSource", "field_70010_a", "Lnet/minecraft/network/rcon/RConConsoleSource;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/command/ICommandManager", "func_71556_a", "(Lnet/minecraft/command/ICommandSender;Ljava/lang/String;)I", true);
mv.visitInsn(POP);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/network/rcon/RConConsoleSource", "field_70010_a", "Lnet/minecraft/network/rcon/RConConsoleSource;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/rcon/RConConsoleSource", "func_70008_c", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71239_B", "()Z", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitInsn(ICONST_0);
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71201_j", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "error", "(Ljava/lang/String;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71198_k", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/server/MinecraftServer", "func_71239_B", "()Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "org/apache/logging/log4j/Logger", "info", "(Ljava/lang/String;)V", true);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71215_F", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71319_s", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71208_b", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_71319_s", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_155759_m", "(Ljava/lang/String;)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_147141_M", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_71241_aa", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71316_v", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_104055_i", "(Z)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "SERVER");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/server/MinecraftServer", "field_104057_T", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$000", "()Lorg/apache/logging/log4j/Logger;", null, null);
mv.visitCode();
mv.visitFieldInsn(GETSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 0);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$100", "(Lnet/minecraft/server/MinecraftServer;)Lnet/minecraft/server/management/ServerConfigurationManager;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/server/MinecraftServer", "field_71318_t", "Lnet/minecraft/server/management/ServerConfigurationManager;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
mv.visitCode();
mv.visitMethodInsn(INVOKESTATIC, "org/apache/logging/log4j/LogManager", "getLogger", "()Lorg/apache/logging/log4j/Logger;", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/server/MinecraftServer", "field_147145_h", "Lorg/apache/logging/log4j/Logger;");
mv.visitTypeInsn(NEW, "java/io/File");
mv.visitInsn(DUP);
mv.visitLdcInsn("usercache.json");
mv.visitMethodInsn(INVOKESPECIAL, "java/io/File", "<init>", "(Ljava/lang/String;)V", false);
mv.visitFieldInsn(PUTSTATIC, "net/minecraft/server/MinecraftServer", "field_152367_a", "Ljava/io/File;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 0);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
