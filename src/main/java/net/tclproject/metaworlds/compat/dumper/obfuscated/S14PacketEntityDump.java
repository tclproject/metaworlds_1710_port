package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class S14PacketEntityDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/network/play/server/S14PacketEntity", null, "net/minecraft/network/Packet", null);

cw.visitInnerClass("net/minecraft/network/play/server/S14PacketEntity$S17PacketEntityLookMove", "net/minecraft/network/play/server/S14PacketEntity", "S17PacketEntityLookMove", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "net/minecraft/network/play/server/S14PacketEntity", "S16PacketEntityLook", ACC_PUBLIC + ACC_STATIC);

cw.visitInnerClass("net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "net/minecraft/network/play/server/S14PacketEntity", "S15PacketEntityRelMove", ACC_PUBLIC + ACC_STATIC);

{
fv = cw.visitField(ACC_PROTECTED, "field_149074_a", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "field_149072_b", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "field_149073_c", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "field_149070_d", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "field_149071_e", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "field_149068_f", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "field_149069_g", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001312");
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "subWorldBelowFeetId", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "tractionLoss", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "losingTraction", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "sendSubWorldPosFlag", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "xPosDiffOnSubWorld", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "yPosDiffOnSubWorld", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PROTECTED, "zPosDiffOnSubWorld", "B", null, null);
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "<init>", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIBZ)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149074_a", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "subWorldBelowFeetId", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "tractionLoss", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 4);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "losingTraction", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 5);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149074_a", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "subWorldBelowFeetId", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "tractionLoss", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readBoolean", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity", "losingTraction", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149074_a", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "subWorldBelowFeetId", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "tractionLoss", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "losingTraction", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeBoolean", "(Z)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148833_a", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/network/play/INetHandlerPlayClient", "func_147259_a", "(Lnet/minecraft/network/play/server/S14PacketEntity;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148835_b", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitLdcInsn("id=%d");
mv.visitInsn(ICONST_1);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149074_a", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "format", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(5, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "toString", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitLdcInsn("Entity_");
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "toString", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149065_a", "(Lnet/minecraft/world/World;)Lnet/minecraft/entity/Entity;", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149074_a", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "func_73045_a", "(I)Lnet/minecraft/entity/Entity;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148833_a", "(Lnet/minecraft/network/INetHandler;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/network/play/INetHandlerPlayClient");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148833_a", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149062_c", "()B", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149072_b", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149061_d", "()B", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149073_c", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149064_e", "()B", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149070_d", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149066_f", "()B", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149071_e", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149063_g", "()B", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149068_f", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149060_h", "()Z", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "field_149069_g", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldBelowFeetId", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "subWorldBelowFeetId", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getTractionLoss", "()B", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "tractionLoss", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getLosingTraction", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "losingTraction", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSendSubWorldPosFlag", "()B", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "sendSubWorldPosFlag", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getXPosDiffOnSubWorld", "()B", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "xPosDiffOnSubWorld", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getYPosDiffOnSubWorld", "()B", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "yPosDiffOnSubWorld", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getZPosDiffOnSubWorld", "()B", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity", "zPosDiffOnSubWorld", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
