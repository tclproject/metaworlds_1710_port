package net.tclproject.metaworlds.compat.dumper;
import java.util.*;
import org.objectweb.asm.*;
public class EntitySorterDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/client/renderer/EntitySorter", null, "java/lang/Object", new String[] { "java/util/Comparator" });

{
av0 = cw.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "entityPosX", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "entityPosY", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "entityPosZ", "D", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00000944");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/entity/Entity;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posX", "D");
mv.visitInsn(DNEG);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosX", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posY", "D");
mv.visitInsn(DNEG);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosY", "D");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posZ", "D");
mv.visitInsn(DNEG);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosZ", "D");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "compare", "(Lnet/minecraft/client/renderer/WorldRenderer;Lnet/minecraft/client/renderer/WorldRenderer;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosX", "D");
mv.visitInsn(DNEG);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosY", "D");
mv.visitInsn(DNEG);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosZ", "D");
mv.visitInsn(DNEG);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "transformToLocal", "(DDD)Lnet/minecraft/util/Vec3;", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "worldObj", "Lnet/minecraft/world/World;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosX", "D");
mv.visitInsn(DNEG);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosY", "D");
mv.visitInsn(DNEG);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/EntitySorter", "entityPosZ", "D");
mv.visitInsn(DNEG);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "transformToLocal", "(DDD)Lnet/minecraft/util/Vec3;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "posXPlus", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 3);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "xCoord", "D");
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 5);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "posYPlus", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 3);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "yCoord", "D");
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 7);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "posZPlus", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 3);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "zCoord", "D");
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 9);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "posXPlus", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 4);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "xCoord", "D");
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 11);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "posYPlus", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 4);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "yCoord", "D");
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 13);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(GETFIELD, "net/minecraft/client/renderer/WorldRenderer", "posZPlus", "I");
mv.visitInsn(I2D);
mv.visitVarInsn(ALOAD, 4);
mv.visitFieldInsn(GETFIELD, "net/minecraft/util/Vec3", "zCoord", "D");
mv.visitInsn(DSUB);
mv.visitVarInsn(DSTORE, 15);
mv.visitVarInsn(DLOAD, 5);
mv.visitVarInsn(DLOAD, 5);
mv.visitInsn(DMUL);
mv.visitVarInsn(DLOAD, 7);
mv.visitVarInsn(DLOAD, 7);
mv.visitInsn(DMUL);
mv.visitInsn(DADD);
mv.visitVarInsn(DLOAD, 9);
mv.visitVarInsn(DLOAD, 9);
mv.visitInsn(DMUL);
mv.visitInsn(DADD);
mv.visitVarInsn(DLOAD, 11);
mv.visitVarInsn(DLOAD, 11);
mv.visitInsn(DMUL);
mv.visitVarInsn(DLOAD, 13);
mv.visitVarInsn(DLOAD, 13);
mv.visitInsn(DMUL);
mv.visitInsn(DADD);
mv.visitVarInsn(DLOAD, 15);
mv.visitVarInsn(DLOAD, 15);
mv.visitInsn(DMUL);
mv.visitInsn(DADD);
mv.visitInsn(DSUB);
mv.visitLdcInsn(new Double("1024.0"));
mv.visitInsn(DMUL);
mv.visitInsn(D2I);
mv.visitInsn(IRETURN);
mv.visitMaxs(8, 17);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "compare", "(Ljava/lang/Object;Ljava/lang/Object;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/client/renderer/WorldRenderer");
mv.visitVarInsn(ALOAD, 2);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/client/renderer/WorldRenderer");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/renderer/EntitySorter", "compare", "(Lnet/minecraft/client/renderer/WorldRenderer;Lnet/minecraft/client/renderer/WorldRenderer;)I", false);
mv.visitInsn(IRETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
