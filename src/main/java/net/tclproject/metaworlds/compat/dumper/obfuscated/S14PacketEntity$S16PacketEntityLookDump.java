package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class S14PacketEntity$S16PacketEntityLookDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", null, "net/minecraft/network/play/server/S14PacketEntity", null);

cw.visitInnerClass("net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "net/minecraft/network/play/server/S14PacketEntity", "S16PacketEntityLook", ACC_PUBLIC + ACC_STATIC);

{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001315");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149069_g", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIBZBB)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ILOAD, 4);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "<init>", "(IIBZ)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 5);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149071_e", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 6);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149068_f", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149069_g", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(5, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149071_e", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149068_f", "B");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149071_e", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149068_f", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148835_b", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148835_b", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", yRot=%d, xRot=%d");
mv.visitInsn(ICONST_2);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149071_e", "B");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;", false);
mv.visitInsn(AASTORE);
mv.visitInsn(DUP);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook", "field_149068_f", "B");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;", false);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "format", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(6, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148833_a", "(Lnet/minecraft/network/INetHandler;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/network/play/INetHandlerPlayClient");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148833_a", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
