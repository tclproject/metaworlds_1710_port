package net.tclproject.metaworlds.compat.dumper;
import java.util.*;
import org.objectweb.asm.*;
public class S18PacketEntityTeleportDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/network/play/server/S18PacketEntityTeleport", null, "net/minecraft/network/Packet", null);

{
fv = cw.visitField(ACC_PRIVATE, "field_149458_a", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_149456_b", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_149457_c", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_149454_d", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_149455_e", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "field_149453_f", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001340");
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "subWorldId", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "tractionLoss", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "losingTraction", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "sendSubWorldPosFlag", "B", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "xPosOnSubWorld", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "yPosOnSubWorld", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PUBLIC, "zPosOnSubWorld", "I", null, null);
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "<init>", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/entity/Entity;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityId", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149458_a", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posX", "D");
mv.visitLdcInsn(new Double("32.0"));
mv.visitInsn(DMUL);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/MathHelper", "floor_double", "(D)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149456_b", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posY", "D");
mv.visitLdcInsn(new Double("32.0"));
mv.visitInsn(DMUL);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/MathHelper", "floor_double", "(D)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149457_c", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posZ", "D");
mv.visitLdcInsn(new Double("32.0"));
mv.visitInsn(DMUL);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/MathHelper", "floor_double", "(D)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149454_d", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "rotationYaw", "F");
mv.visitLdcInsn(new Float("256.0"));
mv.visitInsn(FMUL);
mv.visitLdcInsn(new Float("360.0"));
mv.visitInsn(FDIV);
mv.visitInsn(F2I);
mv.visitInsn(I2B);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149455_e", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "rotationPitch", "F");
mv.visitLdcInsn(new Float("256.0"));
mv.visitInsn(FMUL);
mv.visitLdcInsn(new Float("360.0"));
mv.visitInsn(FDIV);
mv.visitInsn(F2I);
mv.visitInsn(I2B);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149453_f", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getWorldBelowFeet", "()Lnet/minecraft/world/World;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getSubWorldID", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "subWorldId", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getTractionLossTicks", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "tractionLoss", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isLosingTraction", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "losingTraction", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "sendSubWorldPosFlag", "B");
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIBZIIIBBBIII)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/Packet", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149458_a", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 5);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149456_b", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 6);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149457_c", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 7);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149454_d", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 8);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149455_e", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 9);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149453_f", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "subWorldId", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "tractionLoss", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 4);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "losingTraction", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 10);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "sendSubWorldPosFlag", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 11);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "xPosOnSubWorld", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 12);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "yPosOnSubWorld", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 13);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "zPosOnSubWorld", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 14);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "readPacketData", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149458_a", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149456_b", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149457_c", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149454_d", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149455_e", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149453_f", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readUnsignedShort", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "subWorldId", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "tractionLoss", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readBoolean", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "losingTraction", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "sendSubWorldPosFlag", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "sendSubWorldPosFlag", "B");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "xPosOnSubWorld", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "yPosOnSubWorld", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readInt", "()I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "zPosOnSubWorld", "I");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "writePacketData", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149458_a", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149456_b", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149457_c", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149454_d", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149455_e", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149453_f", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "subWorldId", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeShort", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "tractionLoss", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "losingTraction", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeBoolean", "(Z)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "sendSubWorldPosFlag", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "sendSubWorldPosFlag", "B");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "xPosOnSubWorld", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "yPosOnSubWorld", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "zPosOnSubWorld", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeInt", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "processPacket", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/network/play/INetHandlerPlayClient", "handleEntityTeleport", "(Lnet/minecraft/network/play/server/S18PacketEntityTeleport;)V", true);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "processPacket", "(Lnet/minecraft/network/INetHandler;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/network/play/INetHandlerPlayClient");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/play/server/S18PacketEntityTeleport", "processPacket", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149451_c", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149458_a", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149449_d", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149456_b", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149448_e", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149457_c", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149446_f", "()I", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149454_d", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149450_g", "()B", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149455_e", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_149447_h", "()B", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "field_149453_f", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldBelowFeetId", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "subWorldId", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSendSubWorldPosFlag", "()B", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "sendSubWorldPosFlag", "B");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getXPosOnSubWorld", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "xPosOnSubWorld", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getYPosOnSubWorld", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "yPosOnSubWorld", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getZPosOnSubWorld", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S18PacketEntityTeleport", "zPosOnSubWorld", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
