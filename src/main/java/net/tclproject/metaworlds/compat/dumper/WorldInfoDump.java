package net.tclproject.metaworlds.compat.dumper;
import java.util.*;
import org.objectweb.asm.*;
public class WorldInfoDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/world/storage/WorldInfo", null, "java/lang/Object", new String[] { "net/tclproject/metaworlds/api/WorldInfoSuperClass" });

cw.visitInnerClass("net/minecraft/world/WorldSettings$GameType", "net/minecraft/world/WorldSettings", "GameType", ACC_PUBLIC + ACC_FINAL + ACC_STATIC + ACC_ENUM);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$1", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$2", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$3", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$4", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$5", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$6", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$7", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$8", null, null, 0);

cw.visitInnerClass("net/minecraft/world/storage/WorldInfo$9", null, null, 0);

{
fv = cw.visitField(ACC_PRIVATE, "randomSeed", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "terrainType", "Lnet/minecraft/world/WorldType;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "generatorOptions", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "spawnX", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "spawnY", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "spawnZ", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "totalTime", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "worldTime", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "lastTimePlayed", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "sizeOnDisk", "J", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "playerTag", "Lnet/minecraft/nbt/NBTTagCompound;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "dimension", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "levelName", "Ljava/lang/String;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "saveVersion", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "raining", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "rainTime", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "thundering", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "thunderTime", "I", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "mapFeaturesEnabled", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "hardcore", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "allowCommands", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "initialized", "Z", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "theGameRules", "Lnet/minecraft/world/GameRules;", null, null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "additionalProperties", "Ljava/util/Map;", "Ljava/util/Map<Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00000587");
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "subWorldIDsByDimension", "Ljava/util/Map;", "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Ljava/lang/Integer;>;>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "subWorldInfoByID", "Ljava/util/Map;", "Ljava/util/Map<Ljava/lang/Integer;Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;>;", null);
fv.visitEnd();
}
{
fv = cw.visitField(ACC_PRIVATE, "nextSubWorldID", "I", null, null);
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PROTECTED, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "DEFAULT", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/nbt/NBTTagCompound;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "DEFAULT", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("RandomSeed");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getLong", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "randomSeed", "J");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorName");
mv.visitIntInsn(BIPUSH, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorName");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getString", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/world/WorldType", "parseWorldType", "(Ljava/lang/String;)Lnet/minecraft/world/WorldType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
Label l1 = new Label();
mv.visitJumpInsn(IFNONNULL, l1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "DEFAULT", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
Label l2 = new Label();
mv.visitJumpInsn(GOTO, l2);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 3, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound", "java/lang/String"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "isVersioned", "()Z", false);
mv.visitJumpInsn(IFEQ, l2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorVersion");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l3 = new Label();
mv.visitJumpInsn(IFEQ, l3);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorVersion");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitVarInsn(ISTORE, 3);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_APPEND,1, new Object[] {Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ILOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "getWorldTypeForGeneratorVersion", "(I)Lnet/minecraft/world/WorldType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorOptions");
mv.visitIntInsn(BIPUSH, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorOptions");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getString", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_CHOP,1, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameType");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/world/WorldSettings$GameType", "getByID", "(I)Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("MapFeatures");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l4 = new Label();
mv.visitJumpInsn(IFEQ, l4);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("MapFeatures");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getBoolean", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
Label l5 = new Label();
mv.visitJumpInsn(GOTO, l5);
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnX");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnY");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnZ");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Time");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getLong", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("DayTime");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l6 = new Label();
mv.visitJumpInsn(IFEQ, l6);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("DayTime");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getLong", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
Label l7 = new Label();
mv.visitJumpInsn(GOTO, l7);
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
mv.visitLabel(l7);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LastPlayed");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getLong", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "lastTimePlayed", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SizeOnDisk");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getLong", "(Ljava/lang/String;)J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "sizeOnDisk", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LevelName");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getString", "(Ljava/lang/String;)Ljava/lang/String;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "levelName", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("version");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "saveVersion", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("rainTime");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "rainTime", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("raining");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getBoolean", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "raining", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thunderTime");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "thunderTime", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thundering");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getBoolean", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "thundering", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("hardcore");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getBoolean", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "hardcore", "Z");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("initialized");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l8 = new Label();
mv.visitJumpInsn(IFEQ, l8);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("initialized");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getBoolean", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
Label l9 = new Label();
mv.visitJumpInsn(GOTO, l9);
mv.visitLabel(l8);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
mv.visitLabel(l9);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("allowCommands");
mv.visitIntInsn(BIPUSH, 99);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l10 = new Label();
mv.visitJumpInsn(IFEQ, l10);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("allowCommands");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getBoolean", "(Ljava/lang/String;)Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
Label l11 = new Label();
mv.visitJumpInsn(GOTO, l11);
mv.visitLabel(l10);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldSettings$GameType", "CREATIVE", "Lnet/minecraft/world/WorldSettings$GameType;");
Label l12 = new Label();
mv.visitJumpInsn(IF_ACMPNE, l12);
mv.visitInsn(ICONST_1);
Label l13 = new Label();
mv.visitJumpInsn(GOTO, l13);
mv.visitLabel(l12);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/world/storage/WorldInfo"});
mv.visitInsn(ICONST_0);
mv.visitLabel(l13);
mv.visitFrame(Opcodes.F_FULL, 2, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound"}, 2, new Object[] {"net/minecraft/world/storage/WorldInfo", Opcodes.INTEGER});
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
mv.visitLabel(l11);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Player");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l14 = new Label();
mv.visitJumpInsn(IFEQ, l14);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Player");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getCompoundTag", "(Ljava/lang/String;)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "playerTag", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "playerTag", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitLdcInsn("Dimension");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "dimension", "I");
mv.visitLabel(l14);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameRules");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;I)Z", false);
Label l15 = new Label();
mv.visitJumpInsn(IFEQ, l15);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameRules");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getCompoundTag", "(Ljava/lang/String;)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/GameRules", "readGameRulesFromNBT", "(Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitLabel(l15);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("NextSubWorldID");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;)Z", false);
Label l16 = new Label();
mv.visitJumpInsn(IFEQ, l16);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("NextSubWorldID");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitLabel(l16);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldIDsByDimension");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;)Z", false);
Label l17 = new Label();
mv.visitJumpInsn(IFEQ, l17);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldIDsByDimension");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getTagList", "(Ljava/lang/String;I)Lnet/minecraft/nbt/NBTTagList;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l18 = new Label();
mv.visitJumpInsn(GOTO, l18);
Label l19 = new Label();
mv.visitLabel(l19);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/nbt/NBTTagList", Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "getCompoundTagAt", "(I)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 4);
mv.visitLdcInsn("DimID");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getInteger", "(Ljava/lang/String;)I", false);
mv.visitVarInsn(ISTORE, 5);
mv.visitVarInsn(ALOAD, 4);
mv.visitLdcInsn("SubWorldIDs");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getIntArray", "(Ljava/lang/String;)[I", false);
mv.visitVarInsn(ASTORE, 6);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ILOAD, 5);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitTypeInsn(NEW, "java/util/HashSet");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKESTATIC, "com/google/common/primitives/Ints", "asList", "([I)Ljava/util/List;", false);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashSet", "<init>", "(Ljava/util/Collection;)V", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitIincInsn(3, 1);
mv.visitLabel(l18);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "tagCount", "()I", false);
mv.visitJumpInsn(IF_ICMPLT, l19);
mv.visitLabel(l17);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldInfos");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "hasKey", "(Ljava/lang/String;)Z", false);
Label l20 = new Label();
mv.visitJumpInsn(IFEQ, l20);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldInfos");
mv.visitIntInsn(BIPUSH, 10);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "getTagList", "(Ljava/lang/String;I)Lnet/minecraft/nbt/NBTTagList;", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 3);
Label l21 = new Label();
mv.visitJumpInsn(GOTO, l21);
Label l22 = new Label();
mv.visitLabel(l22);
mv.visitFrame(Opcodes.F_APPEND,2, new Object[] {"net/minecraft/nbt/NBTTagList", Opcodes.INTEGER}, 0, null);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "getCompoundTagAt", "(I)Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitTypeInsn(NEW, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "<init>", "(Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ASTORE, 5);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 5);
mv.visitFieldInsn(GETFIELD, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "subWorldId", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitVarInsn(ALOAD, 5);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitIincInsn(3, 1);
mv.visitLabel(l21);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "tagCount", "()I", false);
mv.visitJumpInsn(IF_ICMPLT, l22);
mv.visitLabel(l20);
mv.visitFrame(Opcodes.F_CHOP,2, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/world/WorldSettings;Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "DEFAULT", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "getSeed", "()J", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "randomSeed", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "getGameType", "()Lnet/minecraft/world/WorldSettings$GameType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "isMapFeaturesEnabled", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "levelName", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "getHardcoreEnabled", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "hardcore", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "getTerrainType", "()Lnet/minecraft/world/WorldType;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "func_82749_j", "()Ljava/lang/String;", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings", "areCommandsAllowed", "()Z", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_0);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/TreeMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/TreeMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "java/util/HashMap");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETSTATIC, "net/minecraft/world/WorldType", "DEFAULT", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitLdcInsn("");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitTypeInsn(NEW, "net/minecraft/world/GameRules");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/GameRules", "<init>", "()V", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "randomSeed", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "randomSeed", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "lastTimePlayed", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "lastTimePlayed", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "sizeOnDisk", "J");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "sizeOnDisk", "J");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "playerTag", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "playerTag", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "dimension", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "dimension", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "levelName", "Ljava/lang/String;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "levelName", "Ljava/lang/String;");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "saveVersion", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "saveVersion", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "rainTime", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "rainTime", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "raining", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "raining", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thunderTime", "I");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "thunderTime", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thundering", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "thundering", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "hardcore", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "hardcore", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getNBTTagCompound", "()Lnet/minecraft/nbt/NBTTagCompound;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "playerTag", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo", "updateTagCompound", "(Lnet/minecraft/nbt/NBTTagCompound;Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "cloneNBTCompound", "(Lnet/minecraft/nbt/NBTTagCompound;)Lnet/minecraft/nbt/NBTTagCompound;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 2);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 2);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo", "updateTagCompound", "(Lnet/minecraft/nbt/NBTTagCompound;Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ALOAD, 2);
mv.visitInsn(ARETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PRIVATE, "updateTagCompound", "(Lnet/minecraft/nbt/NBTTagCompound;Lnet/minecraft/nbt/NBTTagCompound;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("RandomSeed");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "randomSeed", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setLong", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorName");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "getWorldTypeName", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setString", "(Ljava/lang/String;Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorVersion");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldType", "getGeneratorVersion", "()I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("generatorOptions");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setString", "(Ljava/lang/String;Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameType");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldSettings$GameType", "getID", "()I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("MapFeatures");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setBoolean", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnX");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnY");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SpawnZ");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Time");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setLong", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("DayTime");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setLong", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SizeOnDisk");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "sizeOnDisk", "J");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setLong", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LastPlayed");
mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/server/MinecraftServer", "getSystemTimeMillis", "()J", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setLong", "(Ljava/lang/String;J)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("LevelName");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "levelName", "Ljava/lang/String;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setString", "(Ljava/lang/String;Ljava/lang/String;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("version");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "saveVersion", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("rainTime");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "rainTime", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("raining");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "raining", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setBoolean", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thunderTime");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thunderTime", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("thundering");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thundering", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setBoolean", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("hardcore");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "hardcore", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setBoolean", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("allowCommands");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setBoolean", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("initialized");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setBoolean", "(Ljava/lang/String;Z)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("GameRules");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/GameRules", "writeGameRulesToNBT", "()Lnet/minecraft/nbt/NBTTagCompound;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setTag", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitVarInsn(ALOAD, 2);
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Player");
mv.visitVarInsn(ALOAD, 2);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setTag", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("NextSubWorldID");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagList", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 3);
mv.visitMethodInsn(INVOKESTATIC, "net/minecraftforge/common/DimensionManager", "getWorlds", "()[Lnet/minecraft/world/WorldServer;", false);
mv.visitInsn(DUP);
mv.visitVarInsn(ASTORE, 7);
mv.visitInsn(ARRAYLENGTH);
mv.visitVarInsn(ISTORE, 6);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ISTORE, 5);
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
Label l2 = new Label();
mv.visitLabel(l2);
mv.visitFrame(Opcodes.F_FULL, 8, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagList", Opcodes.TOP, Opcodes.INTEGER, Opcodes.INTEGER, "[Lnet/minecraft/world/WorldServer;"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 7);
mv.visitVarInsn(ILOAD, 5);
mv.visitInsn(AALOAD);
mv.visitVarInsn(ASTORE, 4);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 8);
mv.visitVarInsn(ALOAD, 8);
mv.visitLdcInsn("DimID");
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getDimension", "()I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setInteger", "(Ljava/lang/String;I)V", false);
mv.visitVarInsn(ALOAD, 8);
mv.visitLdcInsn("SubWorldIDs");
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getSubWorldsMap", "()Ljava/util/Map;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "keySet", "()Ljava/util/Set;", true);
mv.visitInsn(ICONST_0);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Integer");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Set", "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "[Ljava/lang/Integer;");
mv.visitMethodInsn(INVOKESTATIC, "org/apache/commons/lang3/ArrayUtils", "toPrimitive", "([Ljava/lang/Integer;)[I", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setIntArray", "(Ljava/lang/String;[I)V", false);
mv.visitVarInsn(ALOAD, 3);
mv.visitVarInsn(ALOAD, 8);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "appendTag", "(Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/WorldServer", "getSubWorlds", "()Ljava/util/Collection;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Collection", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 10);
Label l3 = new Label();
mv.visitJumpInsn(GOTO, l3);
Label l4 = new Label();
mv.visitLabel(l4);
mv.visitFrame(Opcodes.F_FULL, 11, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagList", "net/minecraft/world/WorldServer", Opcodes.INTEGER, Opcodes.INTEGER, "[Lnet/minecraft/world/WorldServer;", "net/minecraft/nbt/NBTTagCompound", Opcodes.TOP, "java/util/Iterator"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/world/World");
mv.visitVarInsn(ASTORE, 9);
mv.visitTypeInsn(NEW, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 9);
mv.visitTypeInsn(CHECKCAST, "net/tclproject/metaworlds/api/SubWorld");
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "<init>", "(Lnet/tclproject/metaworlds/api/SubWorld;)V", false);
mv.visitVarInsn(ASTORE, 11);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 9);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getSubWorldID", "()I", false);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitVarInsn(ALOAD, 11);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitLabel(l3);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 10);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
mv.visitJumpInsn(IFNE, l4);
mv.visitIincInsn(5, 1);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_FULL, 8, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagList", Opcodes.TOP, Opcodes.INTEGER, Opcodes.INTEGER, "[Lnet/minecraft/world/WorldServer;"}, 0, new Object[] {});
mv.visitVarInsn(ILOAD, 5);
mv.visitVarInsn(ILOAD, 6);
mv.visitJumpInsn(IF_ICMPLT, l2);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldIDsByDimension");
mv.visitVarInsn(ALOAD, 3);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setTag", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagList");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagList", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 4);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "values", "()Ljava/util/Collection;", true);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Collection", "iterator", "()Ljava/util/Iterator;", true);
mv.visitVarInsn(ASTORE, 6);
Label l5 = new Label();
mv.visitJumpInsn(GOTO, l5);
Label l6 = new Label();
mv.visitLabel(l6);
mv.visitFrame(Opcodes.F_FULL, 7, new Object[] {"net/minecraft/world/storage/WorldInfo", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagCompound", "net/minecraft/nbt/NBTTagList", "net/minecraft/nbt/NBTTagList", Opcodes.TOP, "java/util/Iterator"}, 0, new Object[] {});
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitVarInsn(ASTORE, 5);
mv.visitTypeInsn(NEW, "net/minecraft/nbt/NBTTagCompound");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/nbt/NBTTagCompound", "<init>", "()V", false);
mv.visitVarInsn(ASTORE, 7);
mv.visitVarInsn(ALOAD, 5);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "writeToNBT", "(Lnet/minecraft/nbt/NBTTagCompound;)V", false);
mv.visitVarInsn(ALOAD, 4);
mv.visitVarInsn(ALOAD, 7);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagList", "appendTag", "(Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitLabel(l5);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitVarInsn(ALOAD, 6);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
mv.visitJumpInsn(IFNE, l6);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("SubWorldInfos");
mv.visitVarInsn(ALOAD, 4);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/nbt/NBTTagCompound", "setTag", "(Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(4, 12);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getNextSubWorldID", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(DUP);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitInsn(DUP_X1);
mv.visitInsn(ICONST_1);
mv.visitInsn(IADD);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "nextSubWorldID", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(4, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldIDs", "(I)Ljava/util/Collection;", "(I)Ljava/util/Collection<Ljava/lang/Integer;>;", null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldIDsByDimension", "Ljava/util/Map;");
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "java/util/Collection");
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "updateSubWorldInfo", "(Lnet/tclproject/metaworlds/api/SubWorld;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "net/tclproject/metaworlds/api/SubWorld", "getSubWorldID", "()I", true);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitTypeInsn(NEW, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "<init>", "(Lnet/tclproject/metaworlds/api/SubWorld;)V", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "updateSubWorldInfo", "(Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(GETFIELD, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder", "subWorldId", "I");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitInsn(POP);
mv.visitInsn(RETURN);
mv.visitMaxs(3, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldInfo", "(I)Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitVarInsn(ILOAD, 1);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/tclproject/metaworlds/patcher/SubWorldInfoHolder");
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSubWorldInfos", "()Ljava/util/Collection;", "()Ljava/util/Collection<Lnet/tclproject/metaworlds/patcher/SubWorldInfoHolder;>;", null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "subWorldInfoByID", "Ljava/util/Map;");
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "values", "()Ljava/util/Collection;", true);
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSeed", "()J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "randomSeed", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSpawnX", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSpawnY", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSpawnZ", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getWorldTotalTime", "()J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getWorldTime", "()J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSizeOnDisk", "()J", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "sizeOnDisk", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getPlayerNBTTagCompound", "()Lnet/minecraft/nbt/NBTTagCompound;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "playerTag", "Lnet/minecraft/nbt/NBTTagCompound;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getVanillaDimension", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "dimension", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setSpawnX", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setSpawnY", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "incrementTotalWorldTime", "(J)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setSpawnZ", "(I)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setWorldTime", "(J)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(LLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
mv.visitInsn(RETURN);
mv.visitMaxs(3, 3);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setSpawnPosition", "(III)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 4);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getWorldName", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "levelName", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setWorldName", "(Ljava/lang/String;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "levelName", "Ljava/lang/String;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getSaveVersion", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "saveVersion", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setSaveVersion", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "saveVersion", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getLastTimePlayed", "()J", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "lastTimePlayed", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isThundering", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thundering", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setThundering", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "thundering", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getThunderTime", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thunderTime", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setThunderTime", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "thunderTime", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isRaining", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "raining", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setRaining", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "raining", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getRainTime", "()I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "rainTime", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setRainTime", "(I)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "rainTime", "I");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getGameType", "()Lnet/minecraft/world/WorldSettings$GameType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isMapFeaturesEnabled", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setGameType", "(Lnet/minecraft/world/WorldSettings$GameType;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isHardcoreModeEnabled", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "hardcore", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getTerrainType", "()Lnet/minecraft/world/WorldType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setTerrainType", "(Lnet/minecraft/world/WorldType;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getGeneratorOptions", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "areCommandsAllowed", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "isInitialized", "()Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setServerInitialized", "(Z)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "initialized", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getGameRulesInstance", "()Lnet/minecraft/world/GameRules;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameRules", "Lnet/minecraft/world/GameRules;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "addToCrashReport", "(Lnet/minecraft/crash/CrashReportCategory;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level seed");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$1");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$1", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level generator");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$2");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$2", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level generator options");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$3");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$3", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level spawn location");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$4");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$4", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level time");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$5");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$5", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level dimension");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$6");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$6", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level storage version");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$7");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$7", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level weather");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$8");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$8", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitLdcInsn("Level game mode");
mv.visitTypeInsn(NEW, "net/minecraft/world/storage/WorldInfo$9");
mv.visitInsn(DUP);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/world/storage/WorldInfo$9", "<init>", "(Lnet/minecraft/world/storage/WorldInfo;)V", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReportCategory", "addCrashSectionCallable", "(Ljava/lang/String;Ljava/util/concurrent/Callable;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(5, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "setAdditionalProperties", "(Ljava/util/Map;)V", "(Ljava/util/Map<Ljava/lang/String;Lnet/minecraft/nbt/NBTBase;>;)V", null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
Label l0 = new Label();
mv.visitJumpInsn(IFNONNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "getAdditionalProperty", "(Ljava/lang/String;)Lnet/minecraft/nbt/NBTBase;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
Label l0 = new Label();
mv.visitJumpInsn(IFNULL, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "additionalProperties", "Ljava/util/Map;");
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/nbt/NBTBase");
Label l1 = new Label();
mv.visitJumpInsn(GOTO, l1);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(ACONST_NULL);
mv.visitLabel(l1);
mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"net/minecraft/nbt/NBTBase"});
mv.visitInsn(ARETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$0", "(Lnet/minecraft/world/storage/WorldInfo;)Lnet/minecraft/world/WorldType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "terrainType", "Lnet/minecraft/world/WorldType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$1", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "mapFeaturesEnabled", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$2", "(Lnet/minecraft/world/storage/WorldInfo;)Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "generatorOptions", "Ljava/lang/String;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$3", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnX", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$4", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnY", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$5", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "spawnZ", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$6", "(Lnet/minecraft/world/storage/WorldInfo;)J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "totalTime", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$7", "(Lnet/minecraft/world/storage/WorldInfo;)J", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "worldTime", "J");
mv.visitInsn(LRETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$8", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "dimension", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$9", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "saveVersion", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$10", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "rainTime", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$11", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "raining", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$12", "(Lnet/minecraft/world/storage/WorldInfo;)I", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thunderTime", "I");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$13", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "thundering", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$14", "(Lnet/minecraft/world/storage/WorldInfo;)Lnet/minecraft/world/WorldSettings$GameType;", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "theGameType", "Lnet/minecraft/world/WorldSettings$GameType;");
mv.visitInsn(ARETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$15", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "hardcore", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_STATIC + ACC_SYNTHETIC, "access$16", "(Lnet/minecraft/world/storage/WorldInfo;)Z", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/world/storage/WorldInfo", "allowCommands", "Z");
mv.visitInsn(IRETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
