package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class C03PacketPlayer$C05PacketPlayerLookDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", null, "net/minecraft/network/play/client/C03PacketPlayer", null);

cw.visitInnerClass("net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "net/minecraft/network/play/client/C03PacketPlayer", "C05PacketPlayerLook", ACC_PUBLIC + ACC_STATIC);

{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001363");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149481_i", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(FFZIBZ)V", null, null);
{
av0 = mv.visitAnnotation("Lcpw/mods/fml/relauncher/SideOnly;", true);
av0.visitEnum("value", "Lcpw/mods/fml/relauncher/Side;", "CLIENT");
av0.visitEnd();
}
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(FLOAD, 1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149476_e", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(FLOAD, 2);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149473_f", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 3);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149474_g", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 4);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "subWorldBelowFeetID", "I");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 5);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "tractionLoss", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 6);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "losingTraction", "Z");
mv.visitVarInsn(ALOAD, 0);
mv.visitInsn(ICONST_1);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149481_i", "Z");
mv.visitInsn(RETURN);
mv.visitMaxs(2, 7);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readFloat", "()F", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149476_e", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readFloat", "()F", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149473_f", "F");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149476_e", "F");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeFloat", "(F)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook", "field_149473_f", "F");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeFloat", "(F)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148833_a", "(Lnet/minecraft/network/INetHandler;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/network/play/INetHandlerPlayServer");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/client/C03PacketPlayer", "func_148833_a", "(Lnet/minecraft/network/play/INetHandlerPlayServer;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
