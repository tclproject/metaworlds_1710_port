package net.tclproject.metaworlds.compat.dumper.obfuscated;
import java.util.*;
import org.objectweb.asm.*;
public class S14PacketEntity$S15PacketEntityRelMoveDump implements Opcodes {

public static byte[] dump () throws Exception {

ClassWriter cw = new ClassWriter(0);
FieldVisitor fv;
MethodVisitor mv;
AnnotationVisitor av0;

cw.visit(52, ACC_PUBLIC + ACC_SUPER, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", null, "net/minecraft/network/play/server/S14PacketEntity", null);

cw.visitInnerClass("net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "net/minecraft/network/play/server/S14PacketEntity", "S15PacketEntityRelMove", ACC_PUBLIC + ACC_STATIC);

{
fv = cw.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "__OBFID", "Ljava/lang/String;", null, "CL_00001313");
fv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "<init>", "()V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(1, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "<init>", "(IIBZBBBBBBB)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 1);
mv.visitVarInsn(ILOAD, 2);
mv.visitVarInsn(ILOAD, 3);
mv.visitVarInsn(ILOAD, 4);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "<init>", "(IIBZ)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 5);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149072_b", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 6);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149073_c", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 7);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149070_d", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 8);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "sendSubWorldPosFlag", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 9);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "xPosDiffOnSubWorld", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 10);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "yPosDiffOnSubWorld", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ILOAD, 11);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "zPosDiffOnSubWorld", "B");
mv.visitInsn(RETURN);
mv.visitMaxs(5, 12);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149072_b", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149073_c", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149070_d", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "sendSubWorldPosFlag", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "sendSubWorldPosFlag", "B");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "xPosDiffOnSubWorld", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "yPosDiffOnSubWorld", "B");
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "readByte", "()B", false);
mv.visitFieldInsn(PUTFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "zPosDiffOnSubWorld", "B");
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", null, new String[] { "java/io/IOException" });
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148840_b", "(Lnet/minecraft/network/PacketBuffer;)V", false);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149072_b", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149073_c", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149070_d", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "sendSubWorldPosFlag", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "sendSubWorldPosFlag", "B");
Label l0 = new Label();
mv.visitJumpInsn(IFEQ, l0);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "xPosDiffOnSubWorld", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "yPosDiffOnSubWorld", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitVarInsn(ALOAD, 1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "zPosDiffOnSubWorld", "B");
mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/network/PacketBuffer", "writeByte", "(I)Lio/netty/buffer/ByteBuf;", false);
mv.visitInsn(POP);
mv.visitLabel(l0);
mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148835_b", "()Ljava/lang/String;", null, null);
mv.visitCode();
mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
mv.visitInsn(DUP);
mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
mv.visitVarInsn(ALOAD, 0);
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148835_b", "()Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitLdcInsn(", xa=%d, ya=%d, za=%d");
mv.visitInsn(ICONST_3);
mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
mv.visitInsn(DUP);
mv.visitInsn(ICONST_0);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149072_b", "B");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;", false);
mv.visitInsn(AASTORE);
mv.visitInsn(DUP);
mv.visitInsn(ICONST_1);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149073_c", "B");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;", false);
mv.visitInsn(AASTORE);
mv.visitInsn(DUP);
mv.visitInsn(ICONST_2);
mv.visitVarInsn(ALOAD, 0);
mv.visitFieldInsn(GETFIELD, "net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove", "field_149070_d", "B");
mv.visitMethodInsn(INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;", false);
mv.visitInsn(AASTORE);
mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "format", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
mv.visitInsn(ARETURN);
mv.visitMaxs(6, 1);
mv.visitEnd();
}
{
mv = cw.visitMethod(ACC_PUBLIC, "func_148833_a", "(Lnet/minecraft/network/INetHandler;)V", null, null);
mv.visitCode();
mv.visitVarInsn(ALOAD, 0);
mv.visitVarInsn(ALOAD, 1);
mv.visitTypeInsn(CHECKCAST, "net/minecraft/network/play/INetHandlerPlayClient");
mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/network/play/server/S14PacketEntity", "func_148833_a", "(Lnet/minecraft/network/play/INetHandlerPlayClient;)V", false);
mv.visitInsn(RETURN);
mv.visitMaxs(2, 2);
mv.visitEnd();
}
cw.visitEnd();

return cw.toByteArray();
}
}
