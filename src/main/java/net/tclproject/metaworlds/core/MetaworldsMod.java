package net.tclproject.metaworlds.core;

import java.io.File;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.objectweb.asm.util.ASMifier;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.client.renderer.DestroyBlockProgress;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.EntitySorter;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.RenderList;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.command.ICommandManager;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityTrackerEntry;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemBucket;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import net.minecraft.network.play.server.S14PacketEntity;
import net.minecraft.network.play.server.S18PacketEntityTeleport;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.WorldInfo;
import net.minecraftforge.common.MinecraftForge;
import net.tclproject.metaworlds.admin.MwAdminGuiHandler;
import net.tclproject.metaworlds.admin.SubWorldImportProgressUpdater;
import net.tclproject.metaworlds.boats.MetaworldsBoatsMod;
import net.tclproject.metaworlds.patcher.ClassPatcher;
import net.tclproject.metaworlds.serverlist.ServerListButtonAdder;
import net.tclproject.mysteriumlib.network.MetaMagicNetwork;

@Mod(modid = "mwcore", version = MetaworldsBoatsMod.VERSION, name = "MetaWorlds Core")
public class MetaworldsMod {
   public static final String CHANNEL = "mwcore";
   @Instance("MetaworldsMod")
   public static MetaworldsMod instance;
   
   public GeneralPacketPipeline networkHandler;
   public static final String CHANNEL2 = "metaworldscontrolscaptainmod";

   @EventHandler
   public void preInit(FMLPreInitializationEvent event) {
      if(event.getSide().isClient()) {
         WorldClient.subWorldFactory = new SubWorldClientFactory();
      }

      WorldServer.subWorldFactory = new SubWorldServerFactory();
      FMLCommonHandler.instance().bus().register(new EventHookContainer());
      MinecraftForge.EVENT_BUS.register(new EventHookContainer());
   }

   @EventHandler
   public void load(FMLInitializationEvent event) {
      FMLCommonHandler.instance().bus().register(new MWCorePlayerTracker());
      FMLCommonHandler.instance().bus().register(new PlayerTickHandler());
      MinecraftForge.EVENT_BUS.register(new MWCorePlayerTracker());
      MinecraftForge.EVENT_BUS.register(new PlayerTickHandler());
      if(event.getSide().isClient()) {
         FMLCommonHandler.instance().bus().register(new SubWorldClientPreTickHandler());
         FMLCommonHandler.instance().bus().register(new ServerListButtonAdder());
         MinecraftForge.EVENT_BUS.register(new SubWorldClientPreTickHandler());
         MinecraftForge.EVENT_BUS.register(new ServerListButtonAdder());
      }

      MetaMagicNetwork.registerPackets();
      
      networkHandler = new GeneralPacketPipeline();
      networkHandler.initialize(CHANNEL2);
      networkHandler.addDiscriminator(251, CSubWorldProxyPacket.class);
      networkHandler.addDiscriminator(250, SSubWorldProxyPacket.class);

      NetworkRegistry.INSTANCE.registerGuiHandler("metaworldsmod", new MwAdminGuiHandler());
      FMLCommonHandler.instance().bus().register(new SubWorldImportProgressUpdater());
      MinecraftForge.EVENT_BUS.register(new SubWorldImportProgressUpdater());
   }

   @EventHandler
   public void postInit(FMLPostInitializationEvent event) {
//	   dumpAllModifications();
//	   dumpAllModificationsObf();
	   if (ClassPatcher.transformerCountDone < ClassPatcher.transformerCount) {
		   FMLLog.log("Metaworlds", Level.ERROR, "     ----------------------------***----------------------------     ", new Object[0]);
		   FMLLog.log("Metaworlds", Level.ERROR, "--------------------------------*****--------------------------------", new Object[0]);
		   FMLLog.log("Metaworlds", Level.ERROR, "Only applied " + ClassPatcher.transformerCountDone + " patches out of " + ClassPatcher.transformerCount + "! The game will start anyway, but issues are likely to occur!", new Object[0]);
		   FMLLog.log("Metaworlds", Level.ERROR, "--------------------------------*****--------------------------------", new Object[0]);
		   FMLLog.log("Metaworlds", Level.ERROR, "     ----------------------------***----------------------------     ", new Object[0]);
	   }
   }
   
   /** highly local thing dependent on my directory structure, do not use this */
   public static void dumpAllModificationsObf() {
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/entity/AbstractClientPlayer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/entity/player/EntityPlayer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/entity/player/EntityPlayerMP.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/multiplayer/PlayerControllerMP.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/multiplayer/WorldClient.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/network/NetHandlerPlayClient.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/particle/EffectRenderer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/renderer/DestroyBlockProgress.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/renderer/EntityRenderer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/renderer/EntitySorter.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/renderer/RenderGlobal.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/renderer/RenderList.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/renderer/WorldRenderer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/client/Minecraft.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/entity/item/EntityItem.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/entity/Entity.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/entity/EntityTrackerEntry.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/item/ItemBucket.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/client/C03PacketPlayer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/client/C03PacketPlayer$C04PacketPlayerPosition.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/client/C03PacketPlayer$C05PacketPlayerLook.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/client/C03PacketPlayer$C06PacketPlayerPosLook.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/server/S08PacketPlayerPosLook.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/server/S14PacketEntity.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/server/S14PacketEntity$S15PacketEntityRelMove.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/server/S14PacketEntity$S16PacketEntityLook.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/server/S14PacketEntity$S17PacketEntityLookMove.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/play/server/S18PacketEntityTeleport.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/network/NetHandlerPlayServer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/server/management/PlayerManager.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/server/MinecraftServer.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/tileentity/TileEntity.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/util/AxisAlignedBB.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/util/MovingObjectPosition.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/world/storage/WorldInfo.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/world/World.class", true);
	   storeASMFromClassName("/home/consoler/Downloads/metaworlds-1.0a/net/minecraft/world/WorldServer.class", true);
   }
   
   public static void dumpAllModifications() {
	   storeASMFromClassName(AbstractClientPlayer.class.getName(), false);
	   storeASMFromClassName(EntityPlayer.class.getName(), false);
	   storeASMFromClassName(EntityPlayerMP.class.getName(), false);
	   storeASMFromClassName(PlayerControllerMP.class.getName(), false);
	   storeASMFromClassName(WorldClient.class.getName(), false);
	   storeASMFromClassName(NetHandlerPlayClient.class.getName(), false);
	   storeASMFromClassName(EffectRenderer.class.getName(), false);
	   storeASMFromClassName(DestroyBlockProgress.class.getName(), false);
	   storeASMFromClassName(EntityRenderer.class.getName(), false);
	   storeASMFromClassName(EntitySorter.class.getName(), false);
	   storeASMFromClassName(RenderGlobal.class.getName(), false);
	   storeASMFromClassName(RenderList.class.getName(), false);
	   storeASMFromClassName(WorldRenderer.class.getName(), false);
	   storeASMFromClassName(Minecraft.class.getName(), false);
	   storeASMFromClassName(EntityItem.class.getName(), false);
	   storeASMFromClassName(Entity.class.getName(), false);
	   storeASMFromClassName(EntityTrackerEntry.class.getName(), false);
	   storeASMFromClassName(ItemBucket.class.getName(), false);
	   storeASMFromClassName(C03PacketPlayer.class.getName(), false);
	   storeASMFromClassName(S08PacketPlayerPosLook.class.getName(), false);
	   storeASMFromClassName(S14PacketEntity.class.getName(), false);
	   storeASMFromClassName(S18PacketEntityTeleport.class.getName(), false);
	   storeASMFromClassName(NetHandlerPlayServer.class.getName(), false);
	   storeASMFromClassName(PlayerManager.class.getName(), false);
	   storeASMFromClassName(MinecraftServer.class.getName(), false);
	   storeASMFromClassName(TileEntity.class.getName(), false);
	   storeASMFromClassName(AxisAlignedBB.class.getName(), false);
	   storeASMFromClassName(MovingObjectPosition.class.getName(), false);
	   storeASMFromClassName(WorldInfo.class.getName(), false);
	   storeASMFromClassName(World.class.getName(), false);
	   storeASMFromClassName(WorldServer.class.getName(), false);
   }
   
   public static void storeASMFromClassName(String name, boolean obf) {
	   FMLLog.log("Metaworlds", Level.ERROR, "Stand back, magic is going to happen! Retrieving ASM from " + name + "!", new Object[0]);
	   try {
		    PrintStream o = new PrintStream(new File(name + ".asm" + (obf ? ".obf" : "")));
	        PrintStream console = System.out;
	        System.setOut(o);
			ASMifier.main(new String[] {name});
	        System.setOut(console);
	        o.close();
	   } catch (Exception e) {
		  throw new RuntimeException(e);
	   }
   }

   @EventHandler
   public void serverStart(FMLServerStartingEvent event) {
      MinecraftServer server = MinecraftServer.getServer();
      ICommandManager command = server.getCommandManager();
      ServerCommandManager manager = (ServerCommandManager)command;
      manager.registerCommand(new CommandTPWorlds());
      manager.registerCommand(new CommandMWAdmin());
   }
}
