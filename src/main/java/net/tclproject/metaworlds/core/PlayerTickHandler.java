package net.tclproject.metaworlds.core;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.tclproject.metaworlds.api.EntitySuperClass;

public class PlayerTickHandler {
   @SubscribeEvent
   public void onPlayerTick(PlayerTickEvent event) {
      if(event.phase.equals(Phase.START) && event.player.isLosingTraction()) {
         byte tractionTicks = event.player.getTractionLossTicks();
         EntityPlayer var10001 = event.player;
         if(tractionTicks >= 20) {
        	 event.player.setWorldBelowFeet((World)null);
         } else if(event.player.isOnLadder()) {
        	 event.player.setTractionTickCount((byte)0);
         } else {
        	 event.player.setTractionTickCount((byte)(tractionTicks + 1));
         }
      }
   }
}
