package net.tclproject.metaworlds.core;

import java.util.Collection;
import java.util.Iterator;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.event.entity.EntityEvent.CanUpdate;
import net.minecraftforge.event.world.WorldEvent.Load;
import net.tclproject.metaworlds.api.EntitySuperClass;
import net.tclproject.metaworlds.api.WorldInfoSuperClass;
import net.tclproject.metaworlds.api.WorldSuperClass;
import net.tclproject.metaworlds.compat.packet.SubWorldCreatePacket;
import net.tclproject.mysteriumlib.network.MetaMagicNetwork;

public class EventHookContainer {
	
   @SubscribeEvent
   public void worldLoaded(Load event) {
      if(!event.world.isRemote) {
         if(((WorldSuperClass)event.world).isSubWorld()) {
            MetaMagicNetwork.dispatcher.sendToDimension(new SubWorldCreatePacket(1, new Integer[]{Integer.valueOf(((WorldSuperClass)event.world).getSubWorldID())}), event.world.provider.dimensionId);
         } else {
            Collection subWorldIDs = ((WorldInfoSuperClass)DimensionManager.getWorld(0).getWorldInfo()).getSubWorldIDs(((WorldServer)event.world).provider.dimensionId);
            if(subWorldIDs != null) {
               Iterator i$ = subWorldIDs.iterator();

               while(i$.hasNext()) {
                  Integer curSubWorldID = (Integer)i$.next();
                  if (event.world.getSubWorldsMap().get(curSubWorldID) == null) ((WorldSuperClass)event.world).CreateSubWorld(curSubWorldID.intValue());
               }
            }
         }
      }
   }

   @SubscribeEvent
   public void canUpdateEntity(CanUpdate event) {
      if(((WorldSuperClass)event.entity.worldObj).isSubWorld()) {
         Vec3 transformedPos = ((EntitySuperClass)event.entity).getGlobalPos();
         int i = MathHelper.floor_double(transformedPos.xCoord);
         int j = MathHelper.floor_double(transformedPos.zCoord);
         boolean isForced = ((WorldSuperClass)event.entity.worldObj).getParentWorld().getPersistentChunks().containsKey(new ChunkCoordIntPair(i >> 4, j >> 4));
         int b0 = isForced?0:32;
         boolean canUpdate = ((WorldSuperClass)event.entity.worldObj).getParentWorld().checkChunksExist(i - b0, 0, j - b0, i + b0, 0, j + b0);
         if(canUpdate) {
            event.canUpdate = true;
         }
      }
   }
}
