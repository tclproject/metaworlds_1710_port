package net.tclproject.metaworlds.api;

import java.util.HashMap;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public interface EntitySuperClass {
   World getWorldBelowFeet();

   void setWorldBelowFeet(World var1);

   Vec3 getGlobalPos();

   Vec3 getLocalPos(World var1);

   double getGlobalRotationYaw();

   boolean getIsJumping();
}
