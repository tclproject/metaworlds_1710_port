package net.tclproject.metaworlds.api;

import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.tclproject.metaworlds.patcher.OrientedBB;

public interface AxisAlignedBBSuperClass {
	public AxisAlignedBB getTransformedToGlobalBoundingBox(World transformerWorld);

	public AxisAlignedBB getTransformedToLocalBoundingBox(World transformerWorld);

	public OrientedBB rotateYaw(double targetYaw);

	public OrientedBB getOrientedBB();
}
