package net.tclproject.metaworlds.api;

public interface TileEntityBaseSubworldsSuperClass {
	public double getDistanceFromGlobal(double par1, double par3, double par5);
}
