package net.tclproject.metaworlds.patcher;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.material.Material;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.profiler.Profiler;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.storage.ISaveHandler;
import net.tclproject.metaworlds.api.AxisAlignedBBSuperClass;
import net.tclproject.metaworlds.api.EntitySuperClass;
import net.tclproject.metaworlds.api.PlayerManagerSuperClass;
import net.tclproject.metaworlds.api.SubWorld;
import net.tclproject.metaworlds.api.WorldSuperClass;

public abstract class WorldIntermediateClass extends World {
   private ArrayList collidingBBCacheIntermediate = new ArrayList();

   public WorldIntermediateClass(ISaveHandler par1iSaveHandler, String par2Str, WorldProvider par3WorldProvider, WorldSettings par4WorldSettings, Profiler par5Profiler) {
      super(par1iSaveHandler, par2Str, par3WorldProvider, par4WorldSettings, par5Profiler);
   }

   public WorldIntermediateClass(ISaveHandler par1ISaveHandler, String par2Str, WorldSettings par3WorldSettings, WorldProvider par4WorldProvider, Profiler par5Profiler) {
      super(par1ISaveHandler, par2Str, par3WorldSettings, par4WorldProvider, par5Profiler);
   }

   public MovingObjectPosition func_147447_a(Vec3 par1Vec3, Vec3 par2Vec3, boolean par3, boolean par4, boolean par5) {
      MovingObjectPosition bestResult = null;
      Vec3 vecSource = ((WorldSuperClass)this).transformToGlobal(par1Vec3);
      Vec3 vecDest = ((WorldSuperClass)this).transformToGlobal(par2Vec3);
      Iterator i$ = ((WorldSuperClass)((WorldSuperClass)this).getParentWorld()).getWorlds().iterator();

      while(i$.hasNext()) {
         World curWorld = (World)i$.next();
         MovingObjectPosition curResult = ((WorldIntermediateClass)curWorld).rayTraceBlocks_do_do_single(((WorldSuperClass)curWorld).transformToLocal(vecSource), ((WorldSuperClass)curWorld).transformToLocal(vecDest), par3, par4, par5);
         if(curResult != null) {
        	// This field is actually inserted on runtime
            curResult.worldObj = curWorld;
            curResult.hitVec = ((WorldSuperClass)curWorld).transformLocalToOther(this, curResult.hitVec);
         }

         if(bestResult == null || bestResult.typeOfHit == MovingObjectPosition.MovingObjectType.MISS || curResult != null && curResult.typeOfHit != MovingObjectPosition.MovingObjectType.MISS && bestResult.hitVec.squareDistanceTo(par1Vec3) > curResult.hitVec.squareDistanceTo(par1Vec3)) {
            bestResult = curResult;
         }
      }

      return bestResult;
   }

   public MovingObjectPosition rayTraceBlocks_do_do_single(Vec3 par1Vec3, Vec3 par2Vec3, boolean par3, boolean par4, boolean par5) {
      return super.func_147447_a(par1Vec3, par2Vec3, par3, par4, par5);
   }

   public List getCollidingBoundingBoxes(Entity par1Entity, AxisAlignedBB par2AxisAlignedBB) {
      this.collidingBBCacheIntermediate.clear();
      this.collidingBBCacheIntermediate = (ArrayList)this.getCollidingBoundingBoxesLocal(par1Entity, par2AxisAlignedBB);
      Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator();

      while(i$.hasNext()) {
         World curSubWorld = (World)i$.next();
         this.collidingBBCacheIntermediate.addAll(((SubWorld)curSubWorld).getCollidingBoundingBoxesGlobal(par1Entity, par2AxisAlignedBB));
      }

      return this.collidingBBCacheIntermediate;
   }

   public List getCollidingBoundingBoxesLocal(Entity par1Entity, AxisAlignedBB par2AxisAlignedBB) {
      return super.getCollidingBoundingBoxes(par1Entity, par2AxisAlignedBB);
   }

   public boolean isMaterialInBB(AxisAlignedBB par1AxisAlignedBB, Material par2Material) {
      if(this.isMaterialInBBLocal(par1AxisAlignedBB, par2Material)) {
         return true;
      } else {
         if(!((WorldSuperClass)this).isSubWorld()) {
            Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator();

            while(i$.hasNext()) {
               World curSubWorld = (World)i$.next();
               if(((SubWorld)curSubWorld).isMaterialInBBGlobal(par1AxisAlignedBB, par2Material)) {
                  return true;
               }
            }
         }

         return false;
      }
   }

   public boolean isMaterialInBBLocal(AxisAlignedBB par1AxisAlignedBB, Material par2Material) {
      return super.isMaterialInBB(par1AxisAlignedBB, par2Material);
   }

   public boolean isAABBInMaterial(AxisAlignedBB par1AxisAlignedBB, Material par2Material) {
      if(super.isAABBInMaterial(par1AxisAlignedBB, par2Material)) {
         return true;
      } else {
         if(!((WorldSuperClass)this).isSubWorld()) {
            Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator();

            while(i$.hasNext()) {
               World curSubWorld = (World)i$.next();
               if(((SubWorld)curSubWorld).isAABBInMaterialGlobal(par1AxisAlignedBB, par2Material)) {
                  return true;
               }
            }
         }

         return false;
      }
   }

   public List getEntitiesWithinAABBExcludingEntity(Entity par1Entity, AxisAlignedBB par2AxisAlignedBB, IEntitySelector par3IEntitySelector) {
      ArrayList arraylist = new ArrayList();
      arraylist.addAll(this.getEntitiesWithinAABBExcludingEntityLocal(par1Entity, par2AxisAlignedBB, par3IEntitySelector));
      Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator();

      while(i$.hasNext()) {
         World curSubWorld = (World)i$.next();
         arraylist.addAll(((WorldSuperClass)curSubWorld).getEntitiesWithinAABBExcludingEntityLocal(par1Entity, ((AxisAlignedBBSuperClass)par2AxisAlignedBB).getTransformedToLocalBoundingBox(curSubWorld), par3IEntitySelector));
      }

      return arraylist;
   }

   public List getEntitiesWithinAABBExcludingEntityLocal(Entity par1Entity, AxisAlignedBB par2AxisAlignedBB) {
      return this.getEntitiesWithinAABBExcludingEntityLocal(par1Entity, par2AxisAlignedBB, (IEntitySelector)null);
   }

   public List getEntitiesWithinAABBExcludingEntityLocal(Entity par1Entity, AxisAlignedBB par2AxisAlignedBB, IEntitySelector par3IEntitySelector) {
      if(par1Entity instanceof EntityPlayer) {
         par1Entity = ((EntityPlayer)par1Entity).getProxyPlayer(this);
      }

      return super.getEntitiesWithinAABBExcludingEntity((Entity)par1Entity, par2AxisAlignedBB, par3IEntitySelector);
   }

   public List selectEntitiesWithinAABB(Class par1Class, AxisAlignedBB par2AxisAlignedBB, IEntitySelector par3IEntitySelector) {
      ArrayList arraylist = new ArrayList();
      arraylist.addAll(this.selectEntitiesWithinAABBLocal(par1Class, par2AxisAlignedBB, par3IEntitySelector));
      Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator();

      while(i$.hasNext()) {
         World curSubWorld = (World)i$.next();
         arraylist.addAll(((WorldIntermediateClass)curSubWorld).selectEntitiesWithinAABBLocal(par1Class, ((AxisAlignedBBSuperClass)par2AxisAlignedBB).getTransformedToLocalBoundingBox(curSubWorld), par3IEntitySelector));
      }

      return arraylist;
   }

   public List selectEntitiesWithinAABBLocal(Class par1Class, AxisAlignedBB par2AxisAlignedBB, IEntitySelector par3IEntitySelector) {
      return super.selectEntitiesWithinAABB(par1Class, par2AxisAlignedBB, par3IEntitySelector);
   }

   public boolean spawnEntityInWorld(Entity par1Entity) {
      boolean result = super.spawnEntityInWorld(par1Entity);
      World curSubWorld;
      Object proxyPlayer;
      if(!this.isRemote && !((WorldSuperClass)this).isSubWorld() && par1Entity instanceof EntityPlayer) {
         for(Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator(); i$.hasNext(); curSubWorld.spawnEntityInWorld((Entity)proxyPlayer)) {
            curSubWorld = (World)i$.next();
            proxyPlayer = ((EntityPlayer)par1Entity).getProxyPlayer(curSubWorld);
            if(proxyPlayer == null) {
               proxyPlayer = new EntityPlayerMPSubWorldProxy((EntityPlayerMP)par1Entity, curSubWorld);
               ((EntityPlayerMP)proxyPlayer).theItemInWorldManager.setWorld((WorldServer)curSubWorld);
            }
         }
      }

      return result;
   }

   public void removeEntity(Entity par1Entity) {
      super.removeEntity(par1Entity);
      if(!this.isRemote && !((WorldSuperClass)this).isSubWorld() && par1Entity instanceof EntityPlayer) {
         Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator();

         while(i$.hasNext()) {
            World curSubWorld = (World)i$.next();
            EntityPlayer proxyPlayer = ((EntityPlayer)par1Entity).getProxyPlayer(curSubWorld);
            if(proxyPlayer != null) {
               curSubWorld.removeEntity(proxyPlayer);
               if(!((PlayerManagerSuperClass)((WorldServer)curSubWorld).getPlayerManager()).getPlayers().contains(proxyPlayer)) {
            	   ((EntityPlayer)par1Entity).getPlayerProxyMap().remove(Integer.valueOf(((WorldSuperClass)curSubWorld).getSubWorldID()));
               }
            }
         }
      }
   }

   public void removePlayerEntityDangerously(Entity par1Entity) {
      super.removePlayerEntityDangerously(par1Entity);
      if(!this.isRemote && !((WorldSuperClass)this).isSubWorld() && par1Entity instanceof EntityPlayer) {
         Iterator i$ = ((WorldSuperClass)this).getSubWorlds().iterator();

         while(i$.hasNext()) {
            World curSubWorld = (World)i$.next();
            EntityPlayer proxyPlayer = ((EntityPlayer)par1Entity).getProxyPlayer(curSubWorld);
            curSubWorld.removeEntity(proxyPlayer);
            if(!((PlayerManagerSuperClass)((WorldServer)curSubWorld).getPlayerManager()).getPlayers().contains(proxyPlayer)) {
               ((EntityPlayer)par1Entity).getPlayerProxyMap().remove(Integer.valueOf(((WorldSuperClass)curSubWorld).getSubWorldID()));
            }
         }
      }
   }
}
