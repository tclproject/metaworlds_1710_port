package net.tclproject.metaworlds.patcher;

import java.io.ByteArrayInputStream;

import org.apache.logging.log4j.Level;

import cpw.mods.fml.common.FMLLog;
import javassist.ClassPool;
import javassist.CtClass;
import net.minecraft.launchwrapper.IClassTransformer;

import net.tclproject.metaworlds.compat.dumper.*;
import net.tclproject.mysteriumlib.asm.common.CustomLoadingPlugin;

public class ClassPatcher implements IClassTransformer {
	
	public static int transformerCount = 17;
	public static int transformerCountDone = 0;

   public byte[] transform(String name, String arg1, byte[] data) {
      return this.applyPatch(name, arg1, data);
   }

   public byte[] applyPatch(String name, String mappedName, byte[] inputData) {
	   
	   // We're going to be replacing the class anyway so no need for superclass patch?
//       if ("net.minecraft.client.multiplayer.WorldClient".equals(mappedName)) {
//    	   return setSuperclass("net.minecraft.client.multiplayer.WorldClient", "net.tclproject.metaworlds.patcher.WorldIntermediateClass", inputData);
//       } else 
//       if ("net.minecraft.util.AxisAlignedBB".equals(mappedName)) { // Does this even get applied?
//    	   return setSuperclass("net.minecraft.util.AxisAlignedBB", "net.tclproject.metaworlds.patcher.AxisAlignedBB_BaseSubWorld", inputData);
//       if ("net.minecraft.tileentity.TileEntity".equals(mappedName)) {
//    	   return setSuperclass("net.minecraft.tileentity.TileEntity", "net.tclproject.metaworlds.patcher.TileEntityBaseSubWorlds", inputData);
//       } else if ("net.minecraft.world.World".equals(mappedName)) {
//    	   return setSuperclass("net.minecraft.world.World", "net.tclproject.metaworlds.patcher.WorldTransformable", inputData);
//       } else if ("net.minecraft.world.WorldServer".equals(mappedName)) {
//    	   return setSuperclass("net.minecraft.world.WorldServer", "net.tclproject.metaworlds.patcher.WorldIntermediateClass", inputData);
//       } else if ("net.minecraft.entity.Entity".equals(mappedName)) {
//    	   return setSuperclass("net.minecraft.entity.Entity", "net.tclproject.metaworlds.patcher.EntityDraggableBySubWorld", inputData);
       
       if (!CustomLoadingPlugin.isObfuscated()) { // development environment
	       try {
	           if ("net.minecraft.entity.player.EntityPlayer".equals(mappedName)) {
	        	   return setSuperclass("net.minecraft.entity.player.EntityPlayer", "net.tclproject.metaworlds.patcher.EntityIntermediateClass", inputData);
	           } else if ("net.minecraft.entity.player.EntityPlayerMP".equals(mappedName)) {
	        	   return setSuperclass("net.minecraft.entity.player.EntityPlayerMP", "net.tclproject.metaworlds.patcher.EntityPlayerIntermediateClass", inputData);
	           } else if ("net.minecraft.client.entity.AbstractClientPlayer".equals(mappedName)) {
	        	   return setSuperclass("net.minecraft.client.entity.AbstractClientPlayer", "net.tclproject.metaworlds.patcher.EntityPlayerIntermediateClass", inputData);
	           }
	           
		       if ("net.minecraft.util.AxisAlignedBB".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.util.AxisAlignedBB", AxisAlignedBBDump.dump());
		       } else if ("net.minecraft.network.play.client.C03PacketPlayer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.client.C03PacketPlayer", C03PacketPlayerDump.dump());
		       } else if ("net.minecraft.client.renderer.DestroyBlockProgress".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.DestroyBlockProgress", DestroyBlockProgressDump.dump());
		       } else if ("net.minecraft.client.particle.EffectRenderer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.particle.EffectRenderer", EffectRendererDump.dump());
		       } else if ("net.minecraft.entity.Entity".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.entity.Entity", EntityDump.dump());
		       } else if ("net.minecraft.entity.item.EntityItem".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.entity.item.EntityItem", EntityItemDump.dump());
		       } else if ("net.minecraft.client.renderer.EntityRenderer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.EntityRenderer", EntityRendererDump.dump());
		       } else if ("net.minecraft.client.renderer.EntitySorter".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.EntitySorter", EntitySorterDump.dump());
		       } else if ("net.minecraft.entity.EntityTrackerEntry".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.entity.EntityTrackerEntry", EntityTrackerEntryDump.dump());
		       } else if ("net.minecraft.item.ItemBucket".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.item.ItemBucket", ItemBucketDump.dump());
		       } else if ("net.minecraft.client.Minecraft".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.Minecraft", MinecraftDump.dump());
		       } else if ("net.minecraft.server.MinecraftServer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.server.MinecraftServer", MinecraftServerDump.dump());
		       } else if ("net.minecraft.util.MovingObjectPosition".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.util.MovingObjectPosition", MovingObjectPositionDump.dump());
		       } else if ("net.minecraft.client.network.NetHandlerPlayClient".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.network.NetHandlerPlayClient", NetHandlerPlayClientDump.dump());
		       } else if ("net.minecraft.network.NetHandlerPlayServer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.NetHandlerPlayServer", NetHandlerPlayServerDump.dump());
		       } else if ("net.minecraft.client.multiplayer.PlayerControllerMP".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.multiplayer.PlayerControllerMP", PlayerControllerMPDump.dump());
		       } else if ("net.minecraft.server.management.PlayerManager".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.server.management.PlayerManager", PlayerManagerDump.dump());
		       } else if ("net.minecraft.client.renderer.RenderGlobal".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.RenderGlobal", RenderGlobalDump.dump());
		       } else if ("net.minecraft.client.renderer.RenderList".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.RenderList", RenderListDump.dump());
		       } else if ("net.minecraft.network.play.server.S08PacketPlayerPosLook".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S08PacketPlayerPosLook", S08PacketPlayerPosLookDump.dump());
		       } else if ("net.minecraft.network.play.server.S14PacketEntity".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S14PacketEntity", S14PacketEntityDump.dump());
		       } else if ("net.minecraft.network.play.server.S18PacketEntityTeleport".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S18PacketEntityTeleport", S18PacketEntityTeleportDump.dump());
		       } else if ("net.minecraft.tileentity.TileEntity".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.tileentity.TileEntity", TileEntityDump.dump());
		       } else if ("net.minecraft.client.multiplayer.WorldClient".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.multiplayer.WorldClient", WorldClientDump.dump());
		       } else if ("net.minecraft.world.World".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.world.World", WorldDump.dump());
		       } else if ("net.minecraft.world.storage.WorldInfo".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.world.storage.WorldInfo", WorldInfoDump.dump());
		       } else if ("net.minecraft.client.renderer.WorldRenderer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.WorldRenderer", WorldRendererDump.dump());
		       } else if ("net.minecraft.world.WorldServer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.world.WorldServer", WorldServerDump.dump());
		       }
	       } catch (Exception e) {
	    	   throw new RuntimeException(e);
	       }
       } else { // actual minecraft
    	   try {
    		   
    		   //  no superclass setting because it doesn't work outside of dev env
    		   if ("net.minecraft.entity.player.EntityPlayer".equals(mappedName)) {
	        	   return overwriteClass("net.minecraft.entity.player.EntityPlayer", net.tclproject.metaworlds.compat.dumper.obfuscated.EntityPlayerDump.dump());
	           } else if ("net.minecraft.entity.player.EntityPlayerMP".equals(mappedName)) {
	        	   return overwriteClass("net.minecraft.entity.player.EntityPlayerMP", net.tclproject.metaworlds.compat.dumper.obfuscated.EntityPlayerMPDump.dump());
	           } else if ("net.minecraft.client.entity.AbstractClientPlayer".equals(mappedName)) {
	        	   return overwriteClass("net.minecraft.client.entity.AbstractClientPlayer", net.tclproject.metaworlds.compat.dumper.obfuscated.AbstractClientPlayerDump.dump());
	           }
    		   
		       if ("net.minecraft.util.AxisAlignedBB".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.util.AxisAlignedBB", net.tclproject.metaworlds.compat.dumper.obfuscated.AxisAlignedBBDump.dump());
		       } else if ("net.minecraft.network.play.client.C03PacketPlayer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.client.C03PacketPlayer", net.tclproject.metaworlds.compat.dumper.obfuscated.C03PacketPlayerDump.dump());
		       } else if ("net.minecraft.network.play.client.C03PacketPlayer$C04PacketPlayerPosition".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.client.C03PacketPlayer$C04PacketPlayerPosition", net.tclproject.metaworlds.compat.dumper.obfuscated.C03PacketPlayer$C04PacketPlayerPositionDump.dump());
		       } else if ("net.minecraft.network.play.client.C03PacketPlayer$C05PacketPlayerLook".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.client.C03PacketPlayer$C05PacketPlayerLook", net.tclproject.metaworlds.compat.dumper.obfuscated.C03PacketPlayer$C05PacketPlayerLookDump.dump());
		       } else if ("net.minecraft.network.play.client.C03PacketPlayer$C06PacketPlayerPosLook".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.client.C03PacketPlayer$C06PacketPlayerPosLook", net.tclproject.metaworlds.compat.dumper.obfuscated.C03PacketPlayer$C06PacketPlayerPosLookDump.dump());
		       } else if ("net.minecraft.client.renderer.DestroyBlockProgress".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.DestroyBlockProgress", net.tclproject.metaworlds.compat.dumper.obfuscated.DestroyBlockProgressDump.dump());
		       } else if ("net.minecraft.client.particle.EffectRenderer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.particle.EffectRenderer", net.tclproject.metaworlds.compat.dumper.obfuscated.EffectRendererDump.dump());
		       } else if ("net.minecraft.entity.Entity".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.entity.Entity", net.tclproject.metaworlds.compat.dumper.obfuscated.EntityDump.dump());
		       } else if ("net.minecraft.entity.item.EntityItem".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.entity.item.EntityItem", net.tclproject.metaworlds.compat.dumper.obfuscated.EntityItemDump.dump());
		       } else if ("net.minecraft.client.renderer.EntityRenderer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.EntityRenderer", net.tclproject.metaworlds.compat.dumper.obfuscated.EntityRendererDump.dump());
		       } else if ("net.minecraft.client.renderer.EntitySorter".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.EntitySorter", net.tclproject.metaworlds.compat.dumper.obfuscated.EntitySorterDump.dump());
		       } else if ("net.minecraft.entity.EntityTrackerEntry".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.entity.EntityTrackerEntry", net.tclproject.metaworlds.compat.dumper.obfuscated.EntityTrackerEntryDump.dump());
		       } else if ("net.minecraft.item.ItemBucket".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.item.ItemBucket", net.tclproject.metaworlds.compat.dumper.obfuscated.ItemBucketDump.dump());
		       } else if ("net.minecraft.client.Minecraft".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.Minecraft", net.tclproject.metaworlds.compat.dumper.obfuscated.MinecraftDump.dump());
		       } else if ("net.minecraft.server.MinecraftServer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.server.MinecraftServer", net.tclproject.metaworlds.compat.dumper.obfuscated.MinecraftServerDump.dump());
		       } else if ("net.minecraft.util.MovingObjectPosition".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.util.MovingObjectPosition", net.tclproject.metaworlds.compat.dumper.obfuscated.MovingObjectPositionDump.dump());
		       } else if ("net.minecraft.client.network.NetHandlerPlayClient".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.network.NetHandlerPlayClient", net.tclproject.metaworlds.compat.dumper.obfuscated.NetHandlerPlayClientDump.dump());
		       } else if ("net.minecraft.network.NetHandlerPlayServer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.NetHandlerPlayServer", net.tclproject.metaworlds.compat.dumper.obfuscated.NetHandlerPlayServerDump.dump());
		       } else if ("net.minecraft.client.multiplayer.PlayerControllerMP".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.multiplayer.PlayerControllerMP", net.tclproject.metaworlds.compat.dumper.obfuscated.PlayerControllerMPDump.dump());
		       } else if ("net.minecraft.server.management.PlayerManager".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.server.management.PlayerManager", net.tclproject.metaworlds.compat.dumper.obfuscated.PlayerManagerDump.dump());
		       } else if ("net.minecraft.client.renderer.RenderGlobal".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.RenderGlobal", net.tclproject.metaworlds.compat.dumper.obfuscated.RenderGlobalDump.dump());
		       } else if ("net.minecraft.client.renderer.RenderList".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.RenderList", net.tclproject.metaworlds.compat.dumper.obfuscated.RenderListDump.dump());
		       } else if ("net.minecraft.network.play.server.S08PacketPlayerPosLook".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S08PacketPlayerPosLook", net.tclproject.metaworlds.compat.dumper.obfuscated.S08PacketPlayerPosLookDump.dump());
		       } else if ("net.minecraft.network.play.server.S14PacketEntity".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S14PacketEntity", net.tclproject.metaworlds.compat.dumper.obfuscated.S14PacketEntityDump.dump());
		       } else if ("net.minecraft.network.play.server.S14PacketEntity$S15PacketEntityRelMove".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S14PacketEntity$S15PacketEntityRelMove", net.tclproject.metaworlds.compat.dumper.obfuscated.S14PacketEntity$S15PacketEntityRelMoveDump.dump());
		       } else if ("net.minecraft.network.play.server.S14PacketEntity$S16PacketEntityLook".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S14PacketEntity$S16PacketEntityLook", net.tclproject.metaworlds.compat.dumper.obfuscated.S14PacketEntity$S16PacketEntityLookDump.dump());
		       } else if ("net.minecraft.network.play.server.S14PacketEntity$S17PacketEntityLookMove".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S14PacketEntity$S17PacketEntityLookMove", net.tclproject.metaworlds.compat.dumper.obfuscated.S14PacketEntity$S17PacketEntityLookMoveDump.dump());
		       } else if ("net.minecraft.network.play.server.S18PacketEntityTeleport".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.network.play.server.S18PacketEntityTeleport", net.tclproject.metaworlds.compat.dumper.obfuscated.S18PacketEntityTeleportDump.dump());
		       } else if ("net.minecraft.tileentity.TileEntity".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.tileentity.TileEntity", net.tclproject.metaworlds.compat.dumper.obfuscated.TileEntityDump.dump());
		       } else if ("net.minecraft.client.multiplayer.WorldClient".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.multiplayer.WorldClient", net.tclproject.metaworlds.compat.dumper.obfuscated.WorldClientDump.dump());
		       } else if ("net.minecraft.world.World".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.world.World", net.tclproject.metaworlds.compat.dumper.obfuscated.WorldDump.dump());
		       } else if ("net.minecraft.world.storage.WorldInfo".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.world.storage.WorldInfo", net.tclproject.metaworlds.compat.dumper.obfuscated.WorldInfoDump.dump());
		       } else if ("net.minecraft.client.renderer.WorldRenderer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.client.renderer.WorldRenderer", net.tclproject.metaworlds.compat.dumper.obfuscated.WorldRendererDump.dump());
		       } else if ("net.minecraft.world.WorldServer".equals(mappedName)) {
		    	   return overwriteClass("net.minecraft.world.WorldServer", net.tclproject.metaworlds.compat.dumper.obfuscated.WorldServerDump.dump());
		       }
	       } catch (Exception e) {
	    	   throw new RuntimeException(e);
	       }
       }

       return inputData;
   }
   
   private byte[] setSuperclass(String cls, String supercls, byte[] data) {
	   FMLLog.log("Metaworlds", Level.INFO, "Found: " + cls + "!", new Object[0]);
	   try {
           ClassPool cp = ClassPool.getDefault();
           CtClass cc = cp.makeClass(new ByteArrayInputStream(data));
           cc.setSuperclass(cp.get(supercls));
           byte[] byteCode = cc.toBytecode();
           cc.detach();
           FMLLog.log("Metaworlds", Level.INFO, "Successfully set superclass of " + cls + " to " + supercls + "!", new Object[0]);
           transformerCountDone++;
           return byteCode;
       } catch (Exception ex) {
           throw new RuntimeException(ex);
       }
   }
   
   private byte[] overwriteClass(String overwrittenCls, byte[] clsToOverwriteWith) {
	   FMLLog.log("Metaworlds", Level.INFO, "Found: " + overwrittenCls + "!", new Object[0]);
	   try {
           ClassPool cp = ClassPool.getDefault();
           CtClass cc = cp.makeClass(new ByteArrayInputStream(clsToOverwriteWith));
           byte[] byteCode = cc.toBytecode();
           cc.detach();
           FMLLog.log("Metaworlds", Level.INFO, "Successfully overwritten " + overwrittenCls + " with " + clsToOverwriteWith + "!", new Object[0]);
           transformerCountDone++;
           return byteCode;
       } catch (Exception ex) {
           throw new RuntimeException(ex);
       }
   }
}
