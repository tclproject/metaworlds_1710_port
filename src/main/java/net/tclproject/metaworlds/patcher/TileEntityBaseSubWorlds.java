package net.tclproject.metaworlds.patcher;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Vec3;
import net.tclproject.metaworlds.api.SubWorld;
import net.tclproject.metaworlds.api.TileEntityBaseSubworldsSuperClass;
import net.tclproject.metaworlds.api.WorldSuperClass;

public abstract class TileEntityBaseSubWorlds implements TileEntityBaseSubworldsSuperClass {
   public double getDistanceFromGlobal(double par1, double par3, double par5) {
	  // This may show up an error in an IDE, but in reality we do indeed make it extend the right thing so there is no error here
      TileEntity tThis = (TileEntity)this;
      if(tThis.hasWorldObj() && tThis.getWorldObj() instanceof SubWorld) {
         Vec3 transformedPos = ((WorldSuperClass)tThis.getWorldObj()).transformToGlobal((double)tThis.xCoord + 0.5D, (double)tThis.yCoord + 0.5D, (double)tThis.zCoord + 0.5D);
         return transformedPos.squareDistanceTo(par1, par3, par5);
      } else {
         return tThis.getDistanceFrom(par1, par3, par5);
      }
   }
}
