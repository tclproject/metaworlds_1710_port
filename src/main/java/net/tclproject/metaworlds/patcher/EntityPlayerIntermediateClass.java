package net.tclproject.metaworlds.patcher;

import java.util.Iterator;

import com.mojang.authlib.GameProfile;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.tclproject.metaworlds.api.EntitySuperClass;
import net.tclproject.metaworlds.api.WorldSuperClass;

public abstract class EntityPlayerIntermediateClass extends EntityPlayer {
   private final boolean isProxyPlayer = this instanceof EntityPlayerProxy;

   public EntityPlayerIntermediateClass(World p_i45324_1_, GameProfile p_i45324_2_) {
      super(p_i45324_1_, p_i45324_2_);
   }
   
   // does nothing except be used to trigger init of class
   public static void init() {};

   public boolean isInBed() {
      if(super.isInBed()) {
         return true;
      } else {
         if(!((WorldSuperClass)this.worldObj).isSubWorld()) {
            Iterator i$ = (this).getPlayerProxyMap().values().iterator();

            while(i$.hasNext()) {
               EntityPlayerProxy curProxy = (EntityPlayerProxy)i$.next();
               if(((EntityPlayer)curProxy).isPlayerSleeping() && ((EntityPlayer)curProxy).isInBed()) {
                  return true;
               }
            }
         }

         return false;
      }
   }

   public void setSleeping(boolean newState) {
      this.sleeping = newState;
   }

   public void wakeUpPlayer(boolean par1, boolean par2, boolean par3) {
      super.wakeUpPlayer(par1, par2, par3);
      if(!this.isProxyPlayer) {
         Iterator i$ = (this).getPlayerProxyMap().values().iterator();

         while(i$.hasNext()) {
            EntityPlayerProxy curPlayerProxy = (EntityPlayerProxy)i$.next();
            ((EntityPlayer)curPlayerProxy).wakeUpPlayer(par1, par2, par3);
         }
      }
   }

   public boolean isOnLadder() {
      if(this.isProxyPlayer) {
         return ((EntityPlayerProxy)this).getRealPlayer().isOnLadder();
      } else if(super.isOnLadder()) {
         return true;
      } else {
         Iterator i$ = (this).getPlayerProxyMap().values().iterator();

         EntityPlayerProxy curPlayerProxy;
         do {
            if(!i$.hasNext()) {
               return false;
            }

            curPlayerProxy = (EntityPlayerProxy)i$.next();
         } while(!((EntityPlayerIntermediateClass)curPlayerProxy).isOnLadderLocal());

         return true;
      }
   }

   public boolean isOnLadderLocal() {
      return super.isOnLadder();
   }

   public boolean shouldRenderInPass(int pass) {
      return ((WorldSuperClass)this.worldObj).isSubWorld()?false:super.shouldRenderInPass(pass);
   }
}
