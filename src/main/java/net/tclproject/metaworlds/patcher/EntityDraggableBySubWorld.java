package net.tclproject.metaworlds.patcher;

import java.util.HashMap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.tclproject.metaworlds.api.EntitySuperClass;
import net.tclproject.metaworlds.api.SubWorld;
import net.tclproject.metaworlds.api.WorldSuperClass;

public abstract class EntityDraggableBySubWorld implements Comparable, EntitySuperClass {
   public World worldBelowFeet;
   protected byte tractionLoss;
   public static final byte tractionLossThreshold = 20;
   protected boolean losingTraction = false;
   public int serverPosXOnSubWorld;
   public int serverPosYOnSubWorld;
   public int serverPosZOnSubWorld;
   public HashMap<Integer, EntityPlayerProxy> playerProxyMap = this instanceof EntityPlayer?new HashMap():null;

   public double getTractionFactor() {
      return 1.0D - (double)this.tractionLoss * (double)this.tractionLoss / 400.0D;
   }

   public byte getTractionLossTicks() {
      return this.tractionLoss;
   }
   
   public HashMap getPlayerProxyMap() {
	   return playerProxyMap;
   }

   public void setTractionTickCount(byte newTickCount) {
      this.tractionLoss = newTickCount;
   }

   public boolean isLosingTraction() {
      return this.losingTraction;
   }

   public void slowlyRemoveWorldBelowFeet() {
      this.losingTraction = true;
   }

   public void setWorldBelowFeet(World newWorldBelowFeet) {
      this.losingTraction = false;
      this.tractionLoss = 0;
      if(newWorldBelowFeet != this.worldBelowFeet) {
         if(this.worldBelowFeet != null && ((WorldSuperClass)this.worldBelowFeet).isSubWorld()) {
            ((SubWorld)this.worldBelowFeet).unregisterEntityToDrag(this);
         }

         this.worldBelowFeet = newWorldBelowFeet;
         if(this.worldBelowFeet != null && ((WorldSuperClass)this.worldBelowFeet).isSubWorld()) {
            ((SubWorld)this.worldBelowFeet).registerEntityToDrag(this);
         }

         if(this.worldBelowFeet != ((Entity)this).worldObj && ((WorldSuperClass)((Entity)this).worldObj).isSubWorld()) {
            ((SubWorld)((Entity)this).worldObj).registerDetachedEntity(this);
         } else if(this.worldBelowFeet == ((Entity)this).worldObj && ((WorldSuperClass)((Entity)this).worldObj).isSubWorld()) {
            ((SubWorld)((Entity)this).worldObj).unregisterDetachedEntity(this);
         }
      }
   }

   public World getWorldBelowFeet() {
      return this.worldBelowFeet == null?((Entity)this).worldObj:this.worldBelowFeet;
   }

   public int compareTo(Object par1Obj) {
      return this instanceof Entity && par1Obj instanceof Entity?((Entity)par1Obj).getEntityId() - ((Entity)this).getEntityId():0;
   }

   public Vec3 getGlobalPos() {
      return ((WorldSuperClass)((Entity)this).worldObj).transformToGlobal((Entity)this);
   }

   public Vec3 getLocalPos(World referenceWorld) {
      Entity eThis = (Entity)this;
      return referenceWorld == null && eThis.worldObj == null?Vec3.createVectorHelper(eThis.posX, eThis.posY, eThis.posZ):(referenceWorld != eThis.worldObj && referenceWorld != null?((WorldSuperClass)referenceWorld).transformToLocal(this.getGlobalPos()):Vec3.createVectorHelper(eThis.posX, eThis.posY, eThis.posZ));
   }

   public double getGlobalRotationYaw() {
      return (double)((Entity)this).rotationYaw - ((WorldSuperClass)((Entity)this).worldObj).getRotationYaw();
   }

   public double getDistanceSq(double par1, double par3, double par5, World targetWorld) {
      Vec3 thisVecGlobal = this.getGlobalPos();
      Vec3 targetVecGlobal = ((WorldSuperClass)targetWorld).transformToGlobal(par1, par3, par5);
      return targetVecGlobal.squareDistanceTo(thisVecGlobal);
   }

   public boolean getIsJumping() {
      return this instanceof EntityLivingBase?((EntityLivingBase)this).isJumping:false;
   }

   public EntityPlayer getProxyPlayer(World subWorld) {
      return this.getProxyPlayer(((WorldSuperClass)subWorld).getSubWorldID());
   }

   public EntityPlayer getProxyPlayer(int subWorldID) {
      return subWorldID == 0?(EntityPlayer)this:(EntityPlayer)this.playerProxyMap.get(Integer.valueOf(subWorldID));
   }
}
