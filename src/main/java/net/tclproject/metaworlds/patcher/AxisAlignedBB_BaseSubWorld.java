package net.tclproject.metaworlds.patcher;

import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.tclproject.metaworlds.api.AxisAlignedBBSuperClass;
import net.tclproject.metaworlds.api.SubWorld;

public abstract class AxisAlignedBB_BaseSubWorld implements AxisAlignedBBSuperClass {
   public AxisAlignedBB getTransformedToGlobalBoundingBox(World transformerWorld) {
      return (AxisAlignedBB)(transformerWorld instanceof SubWorld?OBBPool.createOBB((AxisAlignedBB)this).transformBoundingBoxToGlobal(transformerWorld):(AxisAlignedBB)this);
   }

   public AxisAlignedBB getTransformedToLocalBoundingBox(World transformerWorld) {
      return (AxisAlignedBB)(transformerWorld instanceof SubWorld?OBBPool.createOBB((AxisAlignedBB)this).transformBoundingBoxToLocal(transformerWorld):(AxisAlignedBB)this);
   }

   public OrientedBB rotateYaw(double targetYaw) {
      return OBBPool.createOBB((AxisAlignedBB)this).rotateYaw(targetYaw);
   }

   public OrientedBB getOrientedBB() {
      return OBBPool.createOBB((AxisAlignedBB)this);
   }
}
