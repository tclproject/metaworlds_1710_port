package net.tclproject.metaworlds.patcher;

import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.tclproject.metaworlds.api.WorldSuperClass;

public class NetHandlerPlayClientSubWorldProxy extends NetHandlerPlayClient {
   public EntityClientPlayerMPSubWorldProxy proxyPlayer;

   public NetHandlerPlayClientSubWorldProxy(MinecraftSubWorldProxy minecraftProxy, NetHandlerPlayClient parentNetHandler, WorldClient targetSubWorld) {
	  super(minecraftProxy, new NetworkManagerSubWorldProxy(parentNetHandler.getNetworkManager(), ((WorldSuperClass)targetSubWorld).getSubWorldID(), true), targetSubWorld);
      this.currentServerMaxPlayers = parentNetHandler.currentServerMaxPlayers;
   }
}
