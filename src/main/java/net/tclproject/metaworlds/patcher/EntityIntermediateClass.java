package net.tclproject.metaworlds.patcher;

import java.util.Iterator;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.tclproject.metaworlds.api.EntityIntermediateClassSuperClass;
import net.tclproject.metaworlds.api.EntitySuperClass;
import net.tclproject.metaworlds.api.WorldSuperClass;

public abstract class EntityIntermediateClass extends EntityLivingBase implements EntityIntermediateClassSuperClass {
   private boolean isPlayerProxy = this instanceof EntityPlayerProxy;
   private static boolean isTransformingClient = false;
   private static boolean isTransformingServer = false;

   public EntityIntermediateClass(World par1World) {
      super(par1World);
   }
   
   // does nothing except be used to trigger init of class
   public static void init() {};

   public void setPosition(double par1, double par3, double par5) {
      super.setPosition(par1, par3, par5);
      if(this.tryLockTransformations()) {
         EntityPlayer curProxyPlayer;
         if(this.isPlayerProxy) {
            EntityPlayerProxy i$ = (EntityPlayerProxy)this;
            Vec3 curProxy = ((EntitySuperClass)this).getGlobalPos();
            curProxyPlayer = i$.getRealPlayer();
            if(curProxyPlayer == null) {
               this.releaseTransformationLock();
               return;
            }

            ((EntityIntermediateClassSuperClass)curProxyPlayer).setPositionLocal(curProxy.xCoord, curProxy.yCoord, curProxy.zCoord);
            Iterator transformedToLocalPos = curProxyPlayer.getPlayerProxyMap().values().iterator();

            while(transformedToLocalPos.hasNext()) {
               EntityPlayerProxy curProxy1 = (EntityPlayerProxy)transformedToLocalPos.next();
               if(curProxy1 != this) {
                  EntityPlayer curProxyPlayer1 = (EntityPlayer)curProxy1;
                  Vec3 transformedToLocalPos1 = ((WorldSuperClass)curProxyPlayer1.worldObj).transformToLocal(curProxy);
                  ((EntityIntermediateClassSuperClass)curProxyPlayer1).setPositionLocal(transformedToLocalPos1.xCoord, transformedToLocalPos1.yCoord, transformedToLocalPos1.zCoord);
               }
            }
         } else if(this.isPlayer()) {
            Iterator i$1 = (this).getPlayerProxyMap().values().iterator();

            while(i$1.hasNext()) {
               EntityPlayerProxy curProxy2 = (EntityPlayerProxy)i$1.next();
               curProxyPlayer = (EntityPlayer)curProxy2;
               Vec3 transformedToLocalPos2 = ((WorldSuperClass)curProxyPlayer.worldObj).transformToLocal(par1, par3, par5);
               ((EntityIntermediateClassSuperClass)curProxyPlayer).setPositionLocal(transformedToLocalPos2.xCoord, transformedToLocalPos2.yCoord, transformedToLocalPos2.zCoord);
            }
         }

         this.releaseTransformationLock();
      }
   }

   public void setPositionLocal(double par1, double par3, double par5) {
      super.setPosition(par1, par3, par5);
   }

   public void setRotation(float par1, float par2) {
      super.setRotation(par1, par2);
      if(this.tryLockTransformations()) {
         if(this.isPlayerProxy) {
            EntityPlayerProxy i$ = (EntityPlayerProxy)this;
            EntityPlayer curProxy = i$.getRealPlayer();
            if(curProxy == null) {
               this.releaseTransformationLock();
               return;
            }

            curProxy.setRotation(transformYawToGlobal(par1, this), par2);
            Iterator curProxyPlayer = (curProxy).getPlayerProxyMap().values().iterator();

            while(curProxyPlayer.hasNext()) {
               EntityPlayerProxy curProxy1 = (EntityPlayerProxy)curProxyPlayer.next();
               if(curProxy1 != this) {
                  EntityPlayer curProxyPlayer1 = (EntityPlayer)curProxy1;
                  curProxyPlayer1.setRotation(transformYawToLocal(curProxy.rotationYaw, curProxyPlayer1), par2);
               }
            }
         } else if(this.isPlayer()) {
            Iterator i$1 = (this).getPlayerProxyMap().values().iterator();

            while(i$1.hasNext()) {
               EntityPlayerProxy curProxy2 = (EntityPlayerProxy)i$1.next();
               EntityPlayer curProxyPlayer2 = (EntityPlayer)curProxy2;
               curProxyPlayer2.setRotation(transformYawToLocal(par1, curProxyPlayer2), par2);
            }
         }

         this.releaseTransformationLock();
      }
   }

   public void setRotationLocal(float par1, float par2) {
      super.setRotation(par1, par2);
   }

   public void moveEntity(double par1, double par3, double par5) {
      super.moveEntity(par1, par3, par5);
      if(this.isPlayerProxy || this.isPlayer()) {
         this.setPosition(this.posX, this.posY, this.posZ);
         EntityPlayerProxy curProxyPlayer;
         if(!this.isPlayerProxy) {
            for(Iterator i$ = (this).getPlayerProxyMap().values().iterator(); i$.hasNext(); ((EntityPlayer)curProxyPlayer).onGround = this.onGround) {
               curProxyPlayer = (EntityPlayerProxy)i$.next();
            }
         }
      }
   }

   public void setPositionAndRotation(double par1, double par3, double par5, float par7, float par8) {
      super.setPositionAndRotation(par1, par3, par5, par7, par8);
      if(this.tryLockTransformations()) {
         EntityPlayer curProxyPlayer;
         if(this.isPlayerProxy) {
            EntityPlayerProxy i$ = (EntityPlayerProxy)this;
            Vec3 curProxy = ((EntitySuperClass)this).getGlobalPos();
            curProxyPlayer = i$.getRealPlayer();
            if(curProxyPlayer == null) {
               this.releaseTransformationLock();
               return;
            }

            ((EntityIntermediateClassSuperClass)curProxyPlayer).setPositionAndRotationLocal(curProxy.xCoord, curProxy.yCoord, curProxy.zCoord, transformYawToGlobal(par7, this), par8);
            Iterator transformedToLocalPos = (curProxyPlayer).getPlayerProxyMap().values().iterator();

            while(transformedToLocalPos.hasNext()) {
               EntityPlayerProxy curProxy1 = (EntityPlayerProxy)transformedToLocalPos.next();
               if(curProxy1 != this) {
                  EntityPlayer curProxyPlayer1 = (EntityPlayer)curProxy1;
                  Vec3 transformedToLocalPos1 = ((WorldSuperClass)curProxyPlayer1.worldObj).transformToLocal(curProxy);
                  ((EntityIntermediateClassSuperClass)curProxyPlayer).setPositionAndRotationLocal(transformedToLocalPos1.xCoord, transformedToLocalPos1.yCoord, transformedToLocalPos1.zCoord, transformYawToLocal(curProxyPlayer.rotationYaw, curProxyPlayer1), par8);
               }
            }
         } else if(this.isPlayer()) {
            Iterator i$1 = (this).getPlayerProxyMap().values().iterator();

            while(i$1.hasNext()) {
               EntityPlayerProxy curProxy2 = (EntityPlayerProxy)i$1.next();
               curProxyPlayer = (EntityPlayer)curProxy2;
               Vec3 transformedToLocalPos2 = ((WorldSuperClass)curProxyPlayer.worldObj).transformToLocal(par1, par3, par5);
               ((EntityIntermediateClassSuperClass)curProxyPlayer).setPositionAndRotationLocal(transformedToLocalPos2.xCoord, transformedToLocalPos2.yCoord, transformedToLocalPos2.zCoord, transformYawToLocal(par7, curProxyPlayer), par8);
            }
         }

         this.releaseTransformationLock();
      }
   }

   public void setPositionAndRotationLocal(double par1, double par3, double par5, float par7, float par8) {
      super.setPositionAndRotation(par1, par3, par5, par7, par8);
   }

   @SideOnly(Side.CLIENT)
   public void setAngles(float par1, float par2) {
      super.setAngles(par1, par2);
      if(this.tryLockTransformations()) {
         if(this.isPlayerProxy) {
            EntityPlayerProxy i$ = (EntityPlayerProxy)this;
            EntityPlayer curProxy = i$.getRealPlayer();
            if(curProxy == null) {
               this.releaseTransformationLock();
               return;
            }

            curProxy.setAngles(par1, par2);
            Iterator curProxyPlayer = (curProxy).getPlayerProxyMap().values().iterator();

            while(curProxyPlayer.hasNext()) {
               EntityPlayerProxy curProxy1 = (EntityPlayerProxy)curProxyPlayer.next();
               if(curProxy1 != this) {
                  EntityPlayer curProxyPlayer1 = (EntityPlayer)curProxy1;
                  curProxyPlayer1.setAngles(par1, par2);
               }
            }
         } else if(this.isPlayer()) {
            Iterator i$1 = (this).getPlayerProxyMap().values().iterator();

            while(i$1.hasNext()) {
               EntityPlayerProxy curProxy2 = (EntityPlayerProxy)i$1.next();
               EntityPlayer curProxyPlayer2 = (EntityPlayer)curProxy2;
               curProxyPlayer2.setAngles(par1, par2);
            }
         }

         this.releaseTransformationLock();
      }
   }

   public void setAnglesLocal(float par1, float par2) {
      super.setAngles(par1, par2);
   }

   public void setLocationAndAngles(double par1, double par3, double par5, float par7, float par8) {
      super.setLocationAndAngles(par1, par3, par5, par7, par8);
      if(this.tryLockTransformations()) {
         EntityPlayer curProxyPlayer;
         if(this.isPlayerProxy) {
            EntityPlayerProxy i$ = (EntityPlayerProxy)this;
            Vec3 curProxy = ((EntitySuperClass)this).getGlobalPos();
            curProxyPlayer = i$.getRealPlayer();
            if(curProxyPlayer == null) {
               this.releaseTransformationLock();
               return;
            }

            ((EntityIntermediateClassSuperClass)curProxyPlayer).setLocationAndAnglesLocal(curProxy.xCoord, curProxy.yCoord, curProxy.zCoord, transformYawToGlobal(par7, this), par8);
            Iterator transformedToLocalPos = (curProxyPlayer).getPlayerProxyMap().values().iterator();

            while(transformedToLocalPos.hasNext()) {
               EntityPlayerProxy curProxy1 = (EntityPlayerProxy)transformedToLocalPos.next();
               if(curProxy1 != this) {
                  EntityPlayer curProxyPlayer1 = (EntityPlayer)curProxy1;
                  Vec3 transformedToLocalPos1 = ((WorldSuperClass)curProxyPlayer1.worldObj).transformToLocal(curProxy);
                  ((EntityIntermediateClassSuperClass)curProxyPlayer1).setLocationAndAnglesLocal(transformedToLocalPos1.xCoord, transformedToLocalPos1.yCoord, transformedToLocalPos1.zCoord, transformYawToLocal(curProxyPlayer.rotationYaw, curProxyPlayer1), par8);
               }
            }
         } else if(this.isPlayer()) {
            Iterator i$1 = (this).getPlayerProxyMap().values().iterator();

            while(i$1.hasNext()) {
               EntityPlayerProxy curProxy2 = (EntityPlayerProxy)i$1.next();
               curProxyPlayer = (EntityPlayer)curProxy2;
               Vec3 transformedToLocalPos2 = ((WorldSuperClass)curProxyPlayer.worldObj).transformToLocal(par1, par3, par5);
               ((EntityIntermediateClassSuperClass)curProxyPlayer).setLocationAndAnglesLocal(transformedToLocalPos2.xCoord, transformedToLocalPos2.yCoord, transformedToLocalPos2.zCoord, transformYawToLocal(par7, curProxyPlayer), par8);
            }
         }

         this.releaseTransformationLock();
      }
   }

   public void setLocationAndAnglesLocal(double par1, double par3, double par5, float par7, float par8) {
      super.setLocationAndAngles(par1, par3, par5, par7, par8);
   }

   private boolean tryLockTransformations() {
      if((!this.worldObj.isRemote || !isTransformingClient) && (this.worldObj.isRemote || !isTransformingServer)) {
         if(this.worldObj.isRemote) {
            isTransformingClient = true;
         } else {
            isTransformingServer = true;
         }

         return true;
      } else {
         return false;
      }
   }

   private void releaseTransformationLock() {
      if(this.worldObj.isRemote) {
         isTransformingClient = false;
      } else {
         isTransformingServer = false;
      }
   }

   private static float transformYawToGlobal(float parYaw, Entity transformingEntity) {
      return (float)((double)parYaw - ((WorldSuperClass)transformingEntity.worldObj).getRotationYaw());
   }

   private static float transformYawToLocal(float parYaw, Entity transformingEntity) {
      return (float)((double)parYaw + ((WorldSuperClass)transformingEntity.worldObj).getRotationYaw());
   }
}
