package net.tclproject.mysteriumlib.asm.fixes;

import org.apache.logging.log4j.Level;
import org.lwjgl.opengl.GL11;

import com.mojang.authlib.GameProfile;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.eventhandler.Event;
import cpw.mods.fml.relauncher.FMLRelaunchLog;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.network.play.client.C0CPacketInput;
import net.minecraft.network.play.server.S23PacketBlockChange;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.ItemInWorldManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.CombatTracker;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings.GameType;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.NibbleArray;
import net.minecraft.world.chunk.storage.AnvilChunkLoader;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.BlockEvent;
import net.tclproject.metaworlds.api.EntitySuperClass;
import net.tclproject.metaworlds.api.SubWorld;
import net.tclproject.metaworlds.api.TileEntityBaseSubworldsSuperClass;
import net.tclproject.metaworlds.api.WorldSuperClass;
import net.tclproject.metaworlds.compat.CompatUtil;
import net.tclproject.metaworlds.compat.packet.UpdateServerHealthPacket;
import net.tclproject.metaworlds.controls.captain.EntitySubWorldController;
import net.tclproject.mysteriumlib.asm.annotations.EnumReturnSetting;
import net.tclproject.mysteriumlib.asm.annotations.Fix;
import net.tclproject.mysteriumlib.asm.annotations.ReturnedValue;
import net.tclproject.mysteriumlib.network.MetaMagicNetwork;

public class MysteriumPatchesFixesMeta {
	
	@Fix
	public static void log(FMLRelaunchLog log, Level level, String format, Object... data) {
		if (format.startsWith("Detected ongoing potential memory lea")) {
			FMLLog.severe("Let me interject for a moment. There is no actual memory leak, but forge is unfortunately complaining due to the way some packets are handled. You can safely ignore this.", new Object[] {0});
		}
	}
	
	@Fix
	public static void severe(FMLLog log, String format, Object... data)
    {
		if (format.startsWith("Detected ongoing potential memory lea")) {
			FMLLog.severe("Let me interject for a moment. There is no actual memory leak, but forge is unfortunately complaining due to the way some packets are handled. You can safely ignore this.", new Object[] {0});
		}
    }
	
	@Fix(returnSetting=EnumReturnSetting.ON_TRUE)
	@SideOnly(Side.CLIENT)
	public static boolean renderItemInFirstPerson(ItemRenderer i, float p_78440_1_) 
	{
		if (Minecraft.getMinecraft().thePlayer.ridingEntity instanceof EntitySubWorldController) {
			return true;
		}
		return false;
    }
	
	@Fix
	@SideOnly(Side.CLIENT)
    public static void handleHealthUpdate(EntityLivingBase b, byte p_70103_1_)
    {
		if (b instanceof EntityPlayer && p_70103_1_ == 2)
        {
        	
        }
    }
	
	@Fix
	public static void attackEntityFrom(EntityLivingBase b, DamageSource p_70097_1_, float p_70097_2_)
    {
		if (b instanceof EntityPlayer) {
			if (b.hurtResistantTime <= 0) {
				MetaMagicNetwork.dispatcher.sendToServer(new UpdateServerHealthPacket(b.getHealth() - p_70097_2_));
//				b.setHealth(b.getHealth() - p_70097_2_);
			}
		}
    }
	
	@Fix
	public static void func_151521_b(CombatTracker ct)
    {
		Minecraft.getMinecraft().thePlayer.setHealth(0);
		MetaMagicNetwork.dispatcher.sendToServer(new UpdateServerHealthPacket(0));
    }
	
	private static boolean inOnUpdate = false;
	
	@SideOnly(Side.CLIENT)
	@Fix(returnSetting = EnumReturnSetting.ON_TRUE)
	public static boolean onUpdate(EntityClientPlayerMP clp)
    {
		if (inOnUpdate) {
			return true;
		} else {
	        if (clp.worldObj.blockExists(MathHelper.floor_double(clp.posX), 0, MathHelper.floor_double(clp.posZ)))
	        {
	        	inOnUpdate = true;
	        	clp.onUpdate();
	        	inOnUpdate = false;
	        	
	            if (clp.isRiding())
	            {
	            	clp.sendQueue.addToSendQueue(new C03PacketPlayer.C05PacketPlayerLook(clp.rotationYaw, clp.rotationPitch, clp.onGround, ((WorldSuperClass)((EntitySuperClass)clp).getWorldBelowFeet()).getSubWorldID(), (clp).getTractionLossTicks(), (clp).isLosingTraction()));
	                clp.sendQueue.addToSendQueue(new C0CPacketInput(clp.moveStrafing, clp.moveForward, clp.movementInput.jump, clp.movementInput.sneak));
	                return true;
	            }
	        }
		}
        return false;
    }
	
	@SideOnly(Side.CLIENT)
	@Fix(returnSetting = EnumReturnSetting.ALWAYS)
	public static void sendMotionUpdates(EntityClientPlayerMP clp)
    {
		// if it's inside an onUpdate, we don't actually want it to do anything
		if (inOnUpdate) return;
		// if not, we overwrite the whole thing to work with our custom packet constructors
		boolean flag = clp.isSprinting();

        if (flag != clp.wasSprinting)
        {
            if (flag)
            {
                clp.sendQueue.addToSendQueue(new C0BPacketEntityAction(clp, 4));
            }
            else
            {
                clp.sendQueue.addToSendQueue(new C0BPacketEntityAction(clp, 5));
            }

            clp.wasSprinting = flag;
        }

        boolean flag1 = clp.isSneaking();

        if (flag1 != clp.wasSneaking)
        {
            if (flag1)
            {
                clp.sendQueue.addToSendQueue(new C0BPacketEntityAction(clp, 1));
            }
            else
            {
                clp.sendQueue.addToSendQueue(new C0BPacketEntityAction(clp, 2));
            }

            clp.wasSneaking = flag1;
        }

        double d0 = clp.posX - clp.oldPosX;
        double d1 = clp.boundingBox.minY - clp.oldMinY;
        double d2 = clp.posZ - clp.oldPosZ;
        double d3 = (double)(clp.rotationYaw - clp.oldRotationYaw);
        double d4 = (double)(clp.rotationPitch - clp.oldRotationPitch);
        boolean flag2 = d0 * d0 + d1 * d1 + d2 * d2 > 9.0E-4D || clp.ticksSinceMovePacket >= 20;
        boolean flag3 = d3 != 0.0D || d4 != 0.0D;

        if (clp.ridingEntity != null)
        {
        	clp.sendQueue.addToSendQueue(new C03PacketPlayer.C06PacketPlayerPosLook(clp.motionX, -999.0D, -999.0D, clp.motionZ, clp.rotationYaw, clp.rotationPitch, clp.onGround, ((WorldSuperClass)((EntitySuperClass)clp).getWorldBelowFeet()).getSubWorldID(), (clp).getTractionLossTicks(), (clp).isLosingTraction()));
            flag2 = false;
        }
        else if (flag2 && flag3)
        {
        	double stance = clp.boundingBox.minY;
        	Vec3 transformedPos = ((EntitySuperClass)clp).getLocalPos(((EntitySuperClass)clp).getWorldBelowFeet());
        	if (((EntitySuperClass)clp).getWorldBelowFeet() != clp.worldObj)
        		stance -= clp.posY;        
        	clp.sendQueue.addToSendQueue(new C03PacketPlayer.C06PacketPlayerPosLook(transformedPos.xCoord, stance, transformedPos.yCoord, transformedPos.zCoord, clp.rotationYaw, clp.rotationPitch, clp.onGround, ((WorldSuperClass)((EntitySuperClass)clp).getWorldBelowFeet()).getSubWorldID(), (clp).getTractionLossTicks(), (clp).isLosingTraction()));
        }
        else if (flag2)
        {
        	double stance = clp.boundingBox.minY;
        	Vec3 transformedPos = ((EntitySuperClass)clp).getLocalPos(((EntitySuperClass)clp).getWorldBelowFeet());
        	if (((EntitySuperClass)clp).getWorldBelowFeet() != clp.worldObj)
        		stance -= clp.posY;        
        	clp.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(transformedPos.xCoord, stance, transformedPos.yCoord, transformedPos.zCoord, clp.onGround, ((WorldSuperClass)((EntitySuperClass)clp).getWorldBelowFeet()).getSubWorldID(), (clp).getTractionLossTicks(), (clp).isLosingTraction()));
        }
        else if (flag3)
        {
            clp.sendQueue.addToSendQueue(new C03PacketPlayer.C05PacketPlayerLook(clp.rotationYaw, clp.rotationPitch, clp.onGround, ((WorldSuperClass)((EntitySuperClass)clp).getWorldBelowFeet()).getSubWorldID(), (clp).getTractionLossTicks(), (clp).isLosingTraction()));
        }
        else
        {
            clp.sendQueue.addToSendQueue(new C03PacketPlayer(clp.onGround, ((WorldSuperClass)((EntitySuperClass)clp).getWorldBelowFeet()).getSubWorldID(), (clp).getTractionLossTicks(), (clp).isLosingTraction()));
        }

        ++clp.ticksSinceMovePacket;
        clp.wasOnGround = clp.onGround;

        if (flag2)
        {
            clp.oldPosX = clp.posX;
            clp.oldMinY = clp.boundingBox.minY;
            clp.oldPosY = clp.posY;
            clp.oldPosZ = clp.posZ;
            clp.ticksSinceMovePacket = 0;
        }

        if (flag3)
        {
            clp.oldRotationYaw = clp.rotationYaw;
            clp.oldRotationPitch = clp.rotationPitch;
        }
    }
	
	// If it's inside an update of the player and we don't want it to send all the packets again, we return false so it won't. If it's not inside an update, it'll just continue on as usual.
	@Fix(returnSetting = EnumReturnSetting.ON_TRUE, booleanAlwaysReturned=false)
	public static boolean isRiding(Entity ent) {
		return inOnUpdate && ent instanceof EntityClientPlayerMP;
	}
	
	@Fix(insertOnExit = true, returnSetting = EnumReturnSetting.ALWAYS)
	public static MovingObjectPosition collisionRayTrace(Block bl, World world, int p_149731_2_, int p_149731_3_, int p_149731_4_, Vec3 p_149731_5_, Vec3 p_149731_6_, @ReturnedValue MovingObjectPosition mopReturned)
    {
		if (mopReturned == null) return null;
		return new MovingObjectPosition(mopReturned, world);
    }
	
	@SideOnly(Side.CLIENT)
	@Fix(returnSetting = EnumReturnSetting.ALWAYS)
	public static boolean renderEntityStatic(RenderManager m, Entity p_147936_1_, float p_147936_2_, boolean p_147936_3_)
    {
        if (p_147936_1_.ticksExisted == 0)
        {
            p_147936_1_.lastTickPosX = p_147936_1_.posX;
            p_147936_1_.lastTickPosY = p_147936_1_.posY;
            p_147936_1_.lastTickPosZ = p_147936_1_.posZ;
        }

        double d0 = p_147936_1_.lastTickPosX + (p_147936_1_.posX - p_147936_1_.lastTickPosX) * (double)p_147936_2_;
        double d1 = p_147936_1_.lastTickPosY + (p_147936_1_.posY - p_147936_1_.lastTickPosY) * (double)p_147936_2_;
        double d2 = p_147936_1_.lastTickPosZ + (p_147936_1_.posZ - p_147936_1_.lastTickPosZ) * (double)p_147936_2_;
        float f1 = p_147936_1_.prevRotationYaw + (p_147936_1_.rotationYaw - p_147936_1_.prevRotationYaw) * p_147936_2_;
        int i = p_147936_1_.getBrightnessForRender(p_147936_2_);

        if (p_147936_1_.isBurning())
        {
            i = 15728880;
        }

        int j = i % 65536;
        int k = i / 65536;
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float)j / 1.0F, (float)k / 1.0F);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        if (p_147936_1_.worldObj.isSubWorld())
        {
            GL11.glPushMatrix();
            GL11.glTranslated(-m.renderPosX, -m.renderPosY, -m.renderPosZ);
            SubWorld parentSubWorld = (SubWorld)p_147936_1_.worldObj;
            GL11.glMultMatrix(parentSubWorld.getTransformToGlobalMatrixDirectBuffer());
            GL11.glTranslated(m.renderPosX, m.renderPosY, m.renderPosZ);
        }
        boolean result = m.func_147939_a(p_147936_1_, d0 - m.renderPosX, d1 - m.renderPosY, d2 - m.renderPosZ, f1, p_147936_2_, p_147936_3_);
        if (p_147936_1_.worldObj.isSubWorld())
            GL11.glPopMatrix();
        return result;
    }
	
	@Fix(returnSetting = EnumReturnSetting.ALWAYS)
	public static boolean canEntityBeSeen(EntityLivingBase l, Entity par1Entity) {
        return par1Entity.worldObj.rayTraceBlocks(par1Entity.worldObj.transformToLocal(l.posX, l.posY + (double)l.getEyeHeight(), l.posZ), Vec3.createVectorHelper(par1Entity.posX, par1Entity.posY + (double)par1Entity.getEyeHeight(), par1Entity.posZ)) == null;
        //return this.worldObj.rayTraceBlocks(this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX, this.posY + (double)this.getEyeHeight(), this.posZ), this.worldObj.getWorldVec3Pool().getVecFromPool(par1Entity.posX, par1Entity.posY + (double)par1Entity.getEyeHeight(), par1Entity.posZ)) == null;
	}
	
	@Fix(targetMethod = "<init>")
	public static void EntityPlayerMP(EntityPlayerMP mp, MinecraftServer p_i45285_1_, WorldServer p_i45285_2_, GameProfile p_i45285_3_, ItemInWorldManager p_i45285_4_)
    {
		p_i45285_4_.thisPlayerMP = mp;
    }
	
	@Fix(returnSetting = EnumReturnSetting.ALWAYS)
	public static void onBlockClicked(ItemInWorldManager m, int p_73074_1_, int p_73074_2_, int p_73074_3_, int p_73074_4_)
    {
        if (!m.gameType.isAdventure() || CompatUtil.isCurrentToolAdventureModeExempt(m.thisPlayerMP, p_73074_1_, p_73074_2_, p_73074_3_, m.theWorld))
        {
            PlayerInteractEvent event = ForgeEventFactory.onPlayerInteract(m.thisPlayerMP, Action.LEFT_CLICK_BLOCK, p_73074_1_, p_73074_2_, p_73074_3_, p_73074_4_, m.theWorld);
            if (event.isCanceled())
            {
            	m.thisPlayerMP.playerNetServerHandler.sendPacket(new S23PacketBlockChange(p_73074_1_, p_73074_2_, p_73074_3_, m.theWorld));
                return;
            }

            if (m.isCreative())
            {
                if (!m.theWorld.extinguishFire((EntityPlayer)null, p_73074_1_, p_73074_2_, p_73074_3_, p_73074_4_))
                {
                    m.tryHarvestBlock(p_73074_1_, p_73074_2_, p_73074_3_);
                }
            }
            else
            {
                m.initialDamage = m.curblockDamage;
                float f = 1.0F;
                Block block = m.theWorld.getBlock(p_73074_1_, p_73074_2_, p_73074_3_);


                if (!block.isAir(m.theWorld, p_73074_1_, p_73074_2_, p_73074_3_))
                {
                    if (event.useBlock != Event.Result.DENY)
                    {
                        block.onBlockClicked(m.theWorld, p_73074_1_, p_73074_2_, p_73074_3_, m.thisPlayerMP);
                        m.theWorld.extinguishFire(null, p_73074_1_, p_73074_2_, p_73074_3_, p_73074_4_);
                    }
                    else
                    {
                    	m.thisPlayerMP.playerNetServerHandler.sendPacket(new S23PacketBlockChange(p_73074_1_, p_73074_2_, p_73074_3_, m.theWorld));
                    }
                    f = block.getPlayerRelativeBlockHardness(m.thisPlayerMP, m.thisPlayerMP.worldObj, p_73074_1_, p_73074_2_, p_73074_3_);
                }

                if (event.useItem == Event.Result.DENY)
                {
                    if (f >= 1.0f)
                    {
                    	m.thisPlayerMP.playerNetServerHandler.sendPacket(new S23PacketBlockChange(p_73074_1_, p_73074_2_, p_73074_3_, m.theWorld));
                    }
                    return;
                }

                if (!block.isAir(m.theWorld, p_73074_1_, p_73074_2_, p_73074_3_) && f >= 1.0F)
                {
                    m.tryHarvestBlock(p_73074_1_, p_73074_2_, p_73074_3_);
                }
                else
                {
                    m.isDestroyingBlock = true;
                    m.partiallyDestroyedBlockX = p_73074_1_;
                    m.partiallyDestroyedBlockY = p_73074_2_;
                    m.partiallyDestroyedBlockZ = p_73074_3_;
                    int i1 = (int)(f * 10.0F);
                    m.theWorld.destroyBlockInWorldPartially(m.thisPlayerMP.getEntityId(), p_73074_1_, p_73074_2_, p_73074_3_, i1);
                    m.durabilityRemainingOnBlock = i1;
                }
            }
        }
    }
	
	@Fix(returnSetting = EnumReturnSetting.ALWAYS)
	public static Chunk readChunkFromNBT(AnvilChunkLoader acl, World p_75823_1_, NBTTagCompound p_75823_2_)
    {
        int i = p_75823_2_.getInteger("xPos");
        int j = p_75823_2_.getInteger("zPos");
        
        // this is the line we actually change from Chunk chunk = new Chunk(p_75823_1_, i, j);
        Chunk chunk = p_75823_1_.createNewChunk(i, j);
        
        chunk.heightMap = p_75823_2_.getIntArray("HeightMap");
        chunk.isTerrainPopulated = p_75823_2_.getBoolean("TerrainPopulated");
        chunk.isLightPopulated = p_75823_2_.getBoolean("LightPopulated");
        chunk.inhabitedTime = p_75823_2_.getLong("InhabitedTime");
        NBTTagList nbttaglist = p_75823_2_.getTagList("Sections", 10);
        byte b0 = 16;
        ExtendedBlockStorage[] aextendedblockstorage = new ExtendedBlockStorage[b0];
        boolean flag = !p_75823_1_.provider.hasNoSky;

        for (int k = 0; k < nbttaglist.tagCount(); ++k)
        {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(k);
            byte b1 = nbttagcompound1.getByte("Y");
            ExtendedBlockStorage extendedblockstorage = new ExtendedBlockStorage(b1 << 4, flag);
            extendedblockstorage.setBlockLSBArray(nbttagcompound1.getByteArray("Blocks"));

            if (nbttagcompound1.hasKey("Add", 7))
            {
                extendedblockstorage.setBlockMSBArray(new NibbleArray(nbttagcompound1.getByteArray("Add"), 4));
            }

            extendedblockstorage.setBlockMetadataArray(new NibbleArray(nbttagcompound1.getByteArray("Data"), 4));
            extendedblockstorage.setBlocklightArray(new NibbleArray(nbttagcompound1.getByteArray("BlockLight"), 4));

            if (flag)
            {
                extendedblockstorage.setSkylightArray(new NibbleArray(nbttagcompound1.getByteArray("SkyLight"), 4));
            }

            extendedblockstorage.removeInvalidBlocks();
            aextendedblockstorage[b1] = extendedblockstorage;
        }

        chunk.setStorageArrays(aextendedblockstorage);

        if (p_75823_2_.hasKey("Biomes", 7))
        {
            chunk.setBiomeArray(p_75823_2_.getByteArray("Biomes"));
        }

        // End this method here and split off entity loading to another method
        return chunk;
    }
	
	@SideOnly(Side.CLIENT)
	@Fix(returnSetting = EnumReturnSetting.ALWAYS)
	public static void renderTileEntity(TileEntityRendererDispatcher ter, TileEntity p_147544_1_, float p_147544_2_)
    {
		// There may show up another error inside an IDE here, but we do indeed cast tileentity to the right thing so there is no error here
		if (((TileEntityBaseSubworldsSuperClass)p_147544_1_).getDistanceFromGlobal(ter.field_147560_j, ter.field_147561_k, ter.field_147558_l) < p_147544_1_.getMaxRenderDistanceSquared())
		{
			int i = p_147544_1_.getWorldObj().getLightBrightnessForSkyBlocks(p_147544_1_.xCoord, p_147544_1_.yCoord, p_147544_1_.zCoord, 0);
			int j = i % 65536;
            int k = i / 65536;
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float)j / 1.0F, (float)k / 1.0F);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glPushMatrix();
            if (p_147544_1_.hasWorldObj() && p_147544_1_.getWorldObj().isSubWorld())
            {
                GL11.glTranslated(-ter.staticPlayerX, -ter.staticPlayerY, -ter.staticPlayerZ);
                SubWorld parentSubWorld = (SubWorld)p_147544_1_.getWorldObj();
                GL11.glMultMatrix(parentSubWorld.getTransformToGlobalMatrixDirectBuffer());
                GL11.glTranslated(ter.staticPlayerX, ter.staticPlayerY, ter.staticPlayerZ);
            }
            ter.renderTileEntityAt(p_147544_1_, (double)p_147544_1_.xCoord - ter.staticPlayerX, (double)p_147544_1_.yCoord - ter.staticPlayerY, (double)p_147544_1_.zCoord - ter.staticPlayerZ, p_147544_2_);
            GL11.glPopMatrix();
		}
    }
	
	@Fix(returnSetting=EnumReturnSetting.ALWAYS)
	public static BlockEvent.BreakEvent onBlockBreakEvent(ForgeHooks f, World world, GameType gameType, EntityPlayerMP entityPlayer, int x, int y, int z)
    {
        // Logic from tryHarvestBlock for pre-canceling the event
        boolean preCancelEvent = false;
        
        // The following is the only line we are actually changing but the other two options I have is to either
        // deal with raw ASM or overwrite the whole class like the original metaworlds did, neither of which I want to do
        if (gameType.isAdventure() && !CompatUtil.isCurrentToolAdventureModeExempt(entityPlayer, x, y, z, world))
        {
            preCancelEvent = true;
        }
        else if (gameType.isCreative() && entityPlayer.getHeldItem() != null && entityPlayer.getHeldItem().getItem() instanceof ItemSword)
        {
            preCancelEvent = true;
        }

        // Tell client the block is gone immediately then process events
        if (world.getTileEntity(x, y, z) == null)
        {
            S23PacketBlockChange packet = new S23PacketBlockChange(x, y, z, world);
            packet.field_148883_d = Blocks.air;
            packet.field_148884_e = 0;
            entityPlayer.playerNetServerHandler.sendPacket(packet);
        }

        // Post the block break event
        Block block = world.getBlock(x, y, z);
        int blockMetadata = world.getBlockMetadata(x, y, z);
        BlockEvent.BreakEvent event = new BlockEvent.BreakEvent(x, y, z, world, block, blockMetadata, entityPlayer);
        event.setCanceled(preCancelEvent);
        MinecraftForge.EVENT_BUS.post(event);

        // Handle if the event is canceled
        if (event.isCanceled())
        {
            // Let the client know the block still exists
            entityPlayer.playerNetServerHandler.sendPacket(new S23PacketBlockChange(x, y, z, world));

            // Update any tile entity data for this block
            TileEntity tileentity = world.getTileEntity(x, y, z);
            if (tileentity != null)
            {
                Packet pkt = tileentity.getDescriptionPacket();
                if (pkt != null)
                {
                    entityPlayer.playerNetServerHandler.sendPacket(pkt);
                }
            }
        }
        return event;
    }
// EXAMPLE: (more examples in the official wiki)
	
      /**
       * Target: every time the window is resized, print the new size
       */
//      @Fix
//      @SideOnly(Side.CLIENT)
//      public static static void resize(Minecraft mc, int x, int y) {
//          System.out.println("Resize, x=" + x + ", y=" + y);
//     }

}
