package net.tclproject.mysteriumlib.asm.fixes;

import net.tclproject.metaworlds.patcher.ClassPatcher;
import net.tclproject.mysteriumlib.asm.common.CustomClassTransformer;
import net.tclproject.mysteriumlib.asm.common.CustomLoadingPlugin;
import net.tclproject.mysteriumlib.asm.common.FirstClassTransformer;

public class MysteriumPatchesFixLoaderMeta extends CustomLoadingPlugin {

    // Turns on MysteriumASM Lib. You can do this in only one of your Fix Loaders.

    @Override
    public String[] getASMTransformerClass() {
        return new String[]{FirstClassTransformer.class.getName(), ClassPatcher.class.getName()};
    }

    @Override
    public void registerFixes() {
//    	CustomClassTransformer.registerPostTransformer(new ClassPatcher());
    	// Doesn't do all the work: superclasses are set by ClassPatcher and some classes are overwritten by ClassOverPatcher
	    registerClassWithFixes("net.tclproject.mysteriumlib.asm.fixes.MysteriumPatchesFixesMeta");
    }
}
