package net.minecraft.client.renderer;

import java.util.Comparator;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.util.Vec3;

@SideOnly(Side.CLIENT)
public class EntitySorter implements Comparator
{
    /** Entity position X */
    private double entityPosX;
    /** Entity position Y */
    private double entityPosY;
    /** Entity position Z */
    private double entityPosZ;
    private static final String __OBFID = "CL_00000944";

    public EntitySorter(Entity p_i1242_1_)
    {
        this.entityPosX = -p_i1242_1_.posX;
        this.entityPosY = -p_i1242_1_.posY;
        this.entityPosZ = -p_i1242_1_.posZ;
    }

    public int compare(WorldRenderer p_compare_1_, WorldRenderer p_compare_2_)
    {
    	Vec3 transformedPos1 = p_compare_1_.worldObj.transformToLocal(-this.entityPosX, -this.entityPosY, -this.entityPosZ);
    	Vec3 transformedPos2 = p_compare_2_.worldObj.transformToLocal(-this.entityPosX, -this.entityPosY, -this.entityPosZ);
    	
    	double d0 = (double)p_compare_1_.posXPlus - transformedPos1.xCoord;
    	double d1 = (double)p_compare_1_.posYPlus - transformedPos1.yCoord;
    	double d2 = (double)p_compare_1_.posZPlus - transformedPos1.zCoord;
    	double d3 = (double)p_compare_2_.posXPlus - transformedPos2.xCoord;
    	double d4 = (double)p_compare_2_.posYPlus - transformedPos2.yCoord;
    	double d5 = (double)p_compare_2_.posZPlus - transformedPos2.zCoord;
        return (int)((d0 * d0 + d1 * d1 + d2 * d2 - (d3 * d3 + d4 * d4 + d5 * d5)) * 1024.0D);
    }

    public int compare(Object p_compare_1_, Object p_compare_2_)
    {
        return this.compare((WorldRenderer)p_compare_1_, (WorldRenderer)p_compare_2_);
    }
}