# Metaworlds Unofficial

Metaworlds Unofficial is a minecraft mod continued from the [Metaworlds](https://bitbucket.org/MMM_MasterM/metaworldsgradle/src/master/) mod for Minecraft Forge 1.7.2 that ports the mod
to use Minecraft Forge 1.7.10 instead, as well as improving and adding some features on top of it.

## Improvements

- Ported to 1.7.10
- Fixed damage taken on subworlds not being shown
- Fixed not being able to die while on a subworld (even in survival)
- Added a really primitive collision system with the overworld (that I am not proud of and hope someone can step in and improve)

## Installation to Game

1. Install Minecraft 1.7.10 with the **latest** Forge version.
2. Download and move the latest jar file from this mod's releases into the mods folder.
3. You might (but not always) have to install extra dependencies as specified [in this link](https://github.com/jblas-project/jblas/wiki/Missing-Libraries)

## Setting up a Development Environment

You can set up a devevelopment environment by Gradle, with IntelliJIdea: `gradlew setupDecompWorkspace idea genIntellijRuns` or Eclipse: `gradlew setupDecompWorkspace eclipse` then import the project. Complete the running configurations with the following VM option: `-Dfml.coreMods.load=net.tclproject.mysteriumlib.asm.fixes.MysteriumPatchesFixLoaderMeta`

## TODO

- Proper collisions with main world (And I'd love some help on this)
- Collisions with other metaworlds
- Performance improvements

## Contributing
Pull requests are welcome.

## License
This mod is currently licensed under the MIT license.
